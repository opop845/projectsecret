﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Secret
{
    public class BattleCamController : MonoBehaviour
    {
        public Animator animator;

        public void SetBattleCam(string trigger)
        {
            animator.SetTrigger(trigger);
            foreach (AnimatorControllerParameter parameter in animator.parameters)
            {
                if (parameter.name != trigger) animator.ResetTrigger(parameter.name);
            }            
            //Debug.Log("BattleCam Switch to " + trigger);
        }

        public void ReSetBattleCam()
        {
            foreach (AnimatorControllerParameter parameter in animator.parameters)
            {
                animator.ResetTrigger(parameter.name);
            }           
        }
    }
}


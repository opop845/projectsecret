﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Secret
{
    public class LobbyCamController : MonoBehaviour
    {
        public Animator animator;

        public void SetLobbyCam(string trigger)
        {
            animator.SetTrigger(trigger);
            foreach (AnimatorControllerParameter parameter in animator.parameters)
            {
                if (parameter.name != trigger) animator.ResetTrigger(parameter.name);
            }
            Debug.Log("LobbyCam Switch to " + trigger);
        }

        public void ResetLobbyCam(string trigger)
        {
            foreach (AnimatorControllerParameter parameter in animator.parameters)
            {
                animator.ResetTrigger(parameter.name);
            }
        }
    }
}


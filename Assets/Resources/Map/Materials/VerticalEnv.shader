// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "VerticalEnv"
{
	Properties
	{
		_distance("distance", Float) = -0.06
		_Bottom_color("Bottom_color", Color) = (0.9339623,0.04846031,0.04846031,0)
		_Top_Color("Top_Color", Color) = (0.9528302,0.8573204,0.382031,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityPBSLighting.cginc"
		#pragma target 3.0
		#pragma surface surf StandardCustomLighting keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nometa noforwardadd 
		struct Input
		{
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float4 _Bottom_color;
		uniform float4 _Top_Color;
		uniform float _distance;

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			float3 ase_worldPos = i.worldPos;
			float clampResult10 = clamp( (0.4 + (( ase_worldPos.y + _distance ) - 0.0) * (0.55 - 0.4) / (1.0 - 0.0)) , 0.0 , 1.0 );
			float4 lerpResult6 = lerp( _Bottom_color , _Top_Color , clampResult10);
			c.rgb = lerpResult6.rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			float3 ase_worldPos = i.worldPos;
			float clampResult10 = clamp( (0.4 + (( ase_worldPos.y + _distance ) - 0.0) * (0.55 - 0.4) / (1.0 - 0.0)) , 0.0 , 1.0 );
			float4 lerpResult6 = lerp( _Bottom_color , _Top_Color , clampResult10);
			o.Emission = lerpResult6.rgb;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18500
-1682;145;1368;862;1209.063;386.0247;1.186365;True;False
Node;AmplifyShaderEditor.WorldPosInputsNode;1;-1269.177,1.389076;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;9;-1362.898,225.6122;Inherit;False;Property;_distance;distance;0;0;Create;True;0;0;False;0;False;-0.06;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;2;-1188.504,197.1395;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;14;-942.1312,336.4718;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0.4;False;4;FLOAT;0.55;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;7;-703.2794,-28.27009;Inherit;False;Property;_Bottom_color;Bottom_color;1;0;Create;True;0;0;False;0;False;0.9339623,0.04846031,0.04846031,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;8;-562.1014,-224.0204;Inherit;False;Property;_Top_Color;Top_Color;2;0;Create;True;0;0;False;0;False;0.9528302,0.8573204,0.382031,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;10;-660.5695,311.0304;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;6;-331.9469,131.8893;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-26,-7;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;VerticalEnv;False;False;False;False;True;True;True;True;True;False;True;True;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;False;0;True;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;0;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;2;0;1;2
WireConnection;2;1;9;0
WireConnection;14;0;2;0
WireConnection;10;0;14;0
WireConnection;6;0;7;0
WireConnection;6;1;8;0
WireConnection;6;2;10;0
WireConnection;0;2;6;0
WireConnection;0;13;6;0
ASEEND*/
//CHKSM=D6438930A214824205A82C1716D08990F803A253
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


namespace Secret
{
    public class RespawnNoti : MonoBehaviour
    {    
        public TextMeshProUGUI TextRespawnTimeLeft; 

        void Start()
        {
            StartCoroutine(RespawnTimeCountdown());
        }

        IEnumerator RespawnTimeCountdown()
        {  
            for (float i = GameManager.Instance.PlayerObject.RespawnTime; i > 0; i-- )
            {        
                TextRespawnTimeLeft.text = i.ToString();
                yield return new WaitForSeconds(1.0f);
            }
            Destroy(gameObject);
        }
    }
}


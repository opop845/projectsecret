﻿using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;


namespace Secret
{
    public class SkillCutScene : MonoBehaviour
    {
        float originalTimeScale = 1.0f;
        [SerializeField] TextMeshProUGUI TextSkill;

        public void SetSkillCut(Skill skill)
        {
            gameObject.SetActive(true);
            TextSkill.SetText(skill.Name);
            //originalTimeScale = Time.timeScale;
            StartCoroutine(SkillCutShow());
        }

        IEnumerator SkillCutShow()
        {
            Time.timeScale = 0.10f;
            yield return new WaitForSecondsRealtime(0.55f);
            Time.timeScale = originalTimeScale;
            gameObject.SetActive(false);
        }
    }
}

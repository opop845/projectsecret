﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Secret
{
    public class TempPopupOpen : MonoBehaviour
    {
        public GameObject PopupTrophyReward;
        public GameObject PopupSpellDetail;

        public void OpenPopupTrophyReward()
        {
            PopupTrophyReward.SetActive(true);
        }

        public void OpenPopupSpellDetail()
        {
            PopupSpellDetail.SetActive(true);
        }

    }
}

﻿using Secret;
using Secret.Server;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class EnemyPlayInfo : MonoBehaviour
{
    public TextMeshProUGUI InfoText;

    void Start()
    {
        InfoText.text = GameManager.Instance.EnemyInfoTextString;        
    }
}

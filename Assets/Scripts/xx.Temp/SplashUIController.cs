﻿using BehaviorDesigner.Runtime.Tasks.Unity.UnityGameObject;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using DG.Tweening;

public class SplashUIController : MonoBehaviour
{
    private float disappearTime = 1.0f;
    private float timer = 0f;
    private bool isForward = false;
    public DOTweenAnimation tween;

    private void Awake()
    {
        tween.hasOnRewind = true;
        tween.onRewind = new UnityEngine.Events.UnityEvent();
        tween.onRewind.AddListener(() => gameObject.SetActive(false));
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        tween.DOPlayForward();
        isForward = true;
        timer = 0f;
    }

    private void Update()
    {
        if (isForward)
        {
            timer += Time.deltaTime;
            if (timer >= disappearTime)
            {
                isForward = false;
                tween.DOPlayBackwards();
            }
        }
    }
}

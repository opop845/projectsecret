﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


namespace Secret
{
    public class TimeNoti : MonoBehaviour
    {      
        public GameObject TimeLeftNoti;
        public GameObject SoulNoti;
        public GameObject SuddenDeathNoti;
        public GameObject CountdownNoti;
        public GameObject Frame;

        public TextMeshProUGUI TextTimeLeft;
        public TextMeshProUGUI TextCountdown;

        string caseName;

        void Start()
        {
            // Init
            Frame.SetActive(true);
            TimeLeftNoti.SetActive(false);
            SoulNoti.SetActive(false);
            SuddenDeathNoti.SetActive(false);
            CountdownNoti.SetActive(false);
            caseName = gameObject.name;
            
            // case에 따른 처리
            switch (caseName)
            {
                case "TimeNoti120":
                    TextTimeLeft.text = "60";
                    TimeLeftNoti.SetActive(true);
                    SoulNoti.SetActive(true);
                    Destroy(gameObject, 1.5f);                    
                    break;
                case "TimeNoti30":
                    TextTimeLeft.text = "30";
                    TimeLeftNoti.SetActive(true);                 
                    Destroy(gameObject, 1.5f);
                    break;
                case "TimeNoti10":
                    StartCoroutine(TimeCountdown());
                    break;
                case "TimeNoti999":                 
                    SuddenDeathNoti.SetActive(true);
                    Destroy(gameObject, 1.5f);
                    break;
            }
        }

        IEnumerator TimeCountdown()
        {
            CountdownNoti.SetActive(true);
            Frame.SetActive(false);
            
            for (int i = 10; i > 0; i-- )
            {
                TextCountdown.text = i.ToString();
                yield return new WaitForSeconds(1.0f);
            }
            CountdownNoti.SetActive(false);
            Destroy(gameObject, 1.5f);
        }
    }
}


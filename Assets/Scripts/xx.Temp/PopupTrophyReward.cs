﻿using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Secret.UI
{
    public class PopupTrophyReward : BaseUI<PopupTrophyReward>
    {
        [SerializeField] TextMeshProUGUI textHighTrophy;
        [SerializeField] TextMeshProUGUI textTorphy;
        [SerializeField] TextMeshProUGUI textRewardType;
        [SerializeField] TextMeshProUGUI textReward;
        [SerializeField] TextMeshProUGUI textCharacter;
        [SerializeField] PrefabTrophySpell[] prefabSpells;
        [SerializeField] Button btnkLock;
        [SerializeField] Button btnReward;
        [SerializeField] TextMeshProUGUI textButtonReward;

        private RewardTrophy RewardTrophy;

        public GameObject GoldReward; // Sanghun
        public GameObject CharacterUnlock; // Sanghun

        protected override void Awake()
        {
            base.Awake();
            GameEventManager.Instance.EventRewardTrophy += Display;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            GameEventManager.Instance.EventRewardTrophy -= Display;
        }
        public override void Open()
        {
            base.Open();
            RewardTrophy = GameDataManager.Instance.User.NextRewardTrophy;
            textHighTrophy.SetText($"Best Trophy : { GameDataManager.Instance.User.HighTrophy}");
            Display();
        }

        void Display()
        {
            if (RewardTrophy == null)
                Close();
            else
            {
                textTorphy.SetText(RewardTrophy.HighTrophy);
                textHighTrophy.SetText($"Best Trophy : {GameDataManager.Instance.User.HighTrophy}");

                foreach (var spell in prefabSpells)
                {
                    spell.SetActive(false);
                }

                switch (RewardTrophy.RewardType)
                {
                    case RewardTrophyType.Spell:
                        for (int i = 0; i < RewardTrophy.Spell.Count; i++)
                            prefabSpells[i].Initialize(RewardTrophy.Spell[i]);
                        textReward.SetText("");
                        textCharacter.SetText("");
                        textRewardType.SetText("Spell Unlock");
                        GoldReward.SetActive(false);
                        CharacterUnlock.SetActive(false);
                        break;
                    case RewardTrophyType.Character:
                        textReward.SetText("");
                        textCharacter.SetText("You can use New Character!\n" + RewardTrophy.Character[0].Name);
                        textRewardType.SetText("Character Unlock");
                        GoldReward.SetActive(false);
                        CharacterUnlock.SetActive(true);
                        break;
                    case RewardTrophyType.Gold:
                        textReward.SetText("x" + RewardTrophy.Gold);
                        textCharacter.SetText("");
                        textRewardType.SetText("Reward");
                        GoldReward.SetActive(true);
                        CharacterUnlock.SetActive(false);
                        break;
                }

                //textRewardType.SetText(RewardTrophy.RewardType.ToString());
                if (RewardTrophy.Index == GameDataManager.Instance.User.TrophyRewardIndex + 1
                    && RewardTrophy.HighTrophy <= GameDataManager.Instance.User.HighTrophy)
                {
                    btnkLock.SetActive(false);
                    btnReward.SetActive(true);
                    textButtonReward.SetText("Claim");
                }
                else if (RewardTrophy.Index > GameDataManager.Instance.User.TrophyRewardIndex + 1
                  || RewardTrophy.HighTrophy > GameDataManager.Instance.User.HighTrophy)
                {
                    btnkLock.SetActive(true);
                    btnReward.SetActive(false);
                }
                else
                {
                    btnkLock.SetActive(false);
                    btnReward.SetActive(true);
                    textButtonReward.SetText("Claimed");
                }
            }
        }

        #region Event

        public void OnClickNext()
        {
            if (RewardTrophy != null)
            {
                if (GameDataManager.Instance.RewardTrophies.TryGetValue(RewardTrophy.Index + 1, out var value))
                {
                    RewardTrophy = value;
                    Display();
                }
            }
        }

        public void OnClickPrevious()
        {
            if (RewardTrophy != null)
            {
                if (GameDataManager.Instance.RewardTrophies.TryGetValue(RewardTrophy.Index - 1, out var value))
                {
                    RewardTrophy = value;
                    Display();
                }
            }
        }

        public void OnClickReward()
        {
            if (RewardTrophy != null)
                ServerManager.Instance.RewardTrophy(RewardTrophy);
        }

        public void OnClickTemp()
        {
        }

        #endregion
    }
}

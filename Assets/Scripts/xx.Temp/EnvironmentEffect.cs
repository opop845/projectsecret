﻿using Photon.Pun.Demo.PunBasics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret
{
    public class EnvironmentEffect : MonoBehaviour
    {
        public GameObject Group;
        Vector3 GroupPos;


        // Start is called before the first frame update
        void Start()
        {
            //Debug.Log(RainEffect.transform.position);
            GroupPos = Group.transform.position;
        }

        // Update is called once per frame
        void Update()
        {
            //Debug.Log(GameManager.Instance.PlayerObject.gameObject.transform.position);
            GroupPos.z = GameManager.Instance.PlayerObject.gameObject.transform.position.z;
            Group.transform.position = GroupPos;
        }
    }
}



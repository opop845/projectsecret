﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


namespace Secret
{
    public class UserCharacterInfo : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        private bool IsLongPress = false;
        public GameObject PanelStatePlayer; // Sanghun
        public GameObject PanelStateEnemy; // Sanghun


        public void OnPointerEnter(PointerEventData eventData) { }

        public void OnPointerExit(PointerEventData eventData) { }

        public void OnPointerClick(PointerEventData eventData) { }

        IEnumerator LongPress()
        {
            yield return new WaitForSecondsRealtime(0.1f);
            IsLongPress = true;
            GameManager.Instance.bothInfo.SetActive(true); // Sanghun
            PanelStatePlayer.SetActive(true); // Sanghun
            PanelStateEnemy.SetActive(true); // Sanghun
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            StartCoroutine(LongPress());
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            StopAllCoroutines();

            if (IsLongPress)
            {
                IsLongPress = false;
                GameManager.Instance.bothInfo.SetActive(false); // Sanghun
                PanelStatePlayer.SetActive(false); // Sanghun
                PanelStateEnemy.SetActive(false); // Sanghun
            }                        
        }
    }
}

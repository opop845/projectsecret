﻿using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using I2.Loc;
using UnityEngine.UI;

namespace Secret
{
    public class PopupSpellDetail : BaseUI<PopupSpellDetail>
    {
        [SerializeField] TextMeshProUGUI textSpellName;
        [SerializeField] TextMeshProUGUI textSpellSoul;
        [SerializeField] TextMeshProUGUI textSpellLevel;
        [SerializeField] TextMeshProUGUI textSpellCount;
        [SerializeField] TextMeshProUGUI textSpellCharacter;
        [SerializeField] TextMeshProUGUI textSpellDesc;
        [SerializeField] TextMeshProUGUI textSpellLevelUpGold;

        [SerializeField] Image imageItemIcon;
        [SerializeField] Image imageSkillIcon;
        [SerializeField] Image imageSkillIndicator;
        [SerializeField] Slider sliderCount;

        private UserSpell userSpell;
        private SpellLevel nextSpellLevel;
        private Spell spell;

        protected override void OnEnable()
        {
            base.OnEnable();
            GameEventManager.Instance.EventDeckSpellChanged += OnEventDeckSpellChanged;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            GameEventManager.Instance.EventDeckSpellChanged -= OnEventDeckSpellChanged;
        }

        public void OpenSpellInfo(UserSpell target)
        {
            userSpell = target;
            if (userSpell != null)
            {
                nextSpellLevel = GameDataManager.Instance.GetSpellLevel(userSpell.Level + 1);
                if (nextSpellLevel != null)
                {
                    sliderCount.maxValue = nextSpellLevel.RequiredSpellCardAmount;
                    sliderCount.value = userSpell.Count;
                    textSpellCount.SetText($"{userSpell.Count}/{nextSpellLevel.RequiredSpellCardAmount}");
                    textSpellLevelUpGold.SetText(nextSpellLevel.RequiredGold);
                }
                else
                {
                    sliderCount.value = sliderCount.maxValue = userSpell.Count;
                    textSpellCount.SetText($"{userSpell.Count}/0");
                    textSpellLevelUpGold.SetText(0);
                }

                textSpellName.SetText(userSpell.Spell.Name);
                textSpellSoul.SetText(userSpell.Spell.Soul);
                textSpellLevel.SetText($"{LocalizationManager.GetTranslation("common_level")} {userSpell.Level}");
                textSpellDesc.SetText(userSpell.Spell.Desc);
                textSpellCharacter.SetActive(userSpell.Spell.SpellType == SpellType.Skill);
                imageItemIcon.SetSprite(userSpell.Spell.IconSprite);
                imageSkillIcon.SetSprite(userSpell.Spell.IconSprite);
                imageItemIcon.SetActive(userSpell.Spell.SpellType == SpellType.Item);
                imageSkillIcon.SetActive(userSpell.Spell.SpellType == SpellType.Skill);
                imageSkillIndicator.SetActive(userSpell.Spell.SpellType == SpellType.Skill);
                Open();
            }
        }

        #region Event

        public void OnClickSpellLevelUp()
        {
            ServerManager.Instance.LevelUpSpell(userSpell);
        }

        #endregion

        #region EventHandler

        private void OnEventDeckSpellChanged()
        {
            if (userSpell != null)
            {
                nextSpellLevel = GameDataManager.Instance.GetSpellLevel(userSpell.Level + 1);
                if (nextSpellLevel != null)
                {
                    sliderCount.maxValue = nextSpellLevel.RequiredSpellCardAmount;
                    sliderCount.value = userSpell.Count;
                    textSpellCount.SetText($"{userSpell.Count}/{nextSpellLevel.RequiredSpellCardAmount}");
                    textSpellLevelUpGold.SetText(nextSpellLevel.RequiredGold);
                }
                else
                {
                    sliderCount.value = sliderCount.maxValue = userSpell.Count;
                    textSpellLevelUpGold.SetText(0);
                    textSpellCount.SetText($"{userSpell.Count}/0");
                }

                textSpellName.SetText(userSpell.Spell.Name);
                textSpellSoul.SetText(userSpell.Spell.Soul);
                textSpellLevel.SetText($"{LocalizationManager.GetTranslation("level")} {userSpell.Level}");
                textSpellDesc.SetText(userSpell.Spell.Desc);
                textSpellCharacter.SetActive(userSpell.Spell.SpellType == SpellType.Skill);
                imageItemIcon.SetSprite(userSpell.Spell.IconSprite);
                imageSkillIcon.SetSprite(userSpell.Spell.IconSprite);
                imageItemIcon.SetActive(userSpell.Spell.SpellType == SpellType.Item);
                imageSkillIcon.SetActive(userSpell.Spell.SpellType == SpellType.Skill);
                imageSkillIndicator.SetActive(userSpell.Spell.SpellType == SpellType.Skill);
            }
        }

        #endregion

    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


namespace Secret
{
    public class InGameComment : MonoBehaviour
    {      
        public GameObject KillIcon;
        public GameObject TowerIcon;
        public TextMeshProUGUI Comment;

        void Start()
        {
            Destroy(gameObject, 1.0f);
        }
    }
}


﻿using Secret.Data;
using System.Collections;
using UnityEngine;

namespace Secret
{
    public class BattleBoxObject : MonoBehaviour
    {
        [SerializeField] GameObject model;
        private BattleBox battleBoxData;
        private BoxCollider boxCollider;
        private GameObject miniMapObject;

        public void Init(BattleBox target, Transform pos, GameObject obj)
        {
            transform.position = pos.position;
            transform.rotation = pos.rotation;
            battleBoxData = target;
            boxCollider = gameObject.GetComponent<BoxCollider>();
            miniMapObject = obj;
            miniMapObject.SetActive(true);
        }

        IEnumerator SpawnBox()
        {
            miniMapObject.SetActive(false);
            boxCollider.enabled = false;
            model?.SetActive(false);
            yield return YieldInstructionCache.WaitForSeconds(45f);
            boxCollider.enabled = true;
            model?.SetActive(true);
            miniMapObject.SetActive(true);
        }

        private void OnTriggerEnter(Collider other)
        {
            var character = other.GetComponent<CharacterObject>();
            if (character != null)
            {
                character.PurchaseItem(battleBoxData.Spells.RandomValue().Item);
                GameManager.Instance.DisplayGetBuffItemEffect(character);
                StartCoroutine(SpawnBox());
            }
        }
    }
}
﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret
{
    public class AttackState : State
    {
        public AttackState(DefaultObject obj) : base(obj)
        {
            type = StateType.Attack;
        }

        public override IEnumerator OnEnter(params object[] objs)
        {
            while (true)
            {
                float attackspeed = defaultObject.AttackSpeed;
                float attackCoolTime = 1f / attackspeed;
                if (defaultObject.target != null)
                {
                    defaultObject.Anim.SetFloat("AttackSpeed", attackspeed > 1f ? attackspeed : 1f);
                    defaultObject.Anim.SetTrigger("Attack1");
                    yield return YieldInstructionCache.WaitForSeconds(attackCoolTime);
                }
                else
                {
                    yield return YieldInstructionCache.WaitForSeconds(0.1f);
                    defaultObject.SetState(StateType.Idle);
                    yield return null;
                }
            }
        }

        public override void OnExit()
        {
        }

        public override void OnFixedUpdate()
        {
        }

        public override void OnUpdate()
        {
        }
    }
}
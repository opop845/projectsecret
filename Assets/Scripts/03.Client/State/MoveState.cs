﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret
{
    public class MoveState : State
    {
        private float runTime;
        public MoveState(DefaultObject obj) : base(obj)
        {
            type = StateType.Move;
        }

        public override IEnumerator OnEnter(params object[] objs)
        {
            runTime = 0f;
            defaultObject.Anim.SetBool("Run", true);
            yield return null;
        }

        public override void OnExit()
        {
            defaultObject.Anim.SetBool("Run", false);
            defaultObject.Anim.SetFloat("RunSpeed", 1.1f);
        }

        public override void OnFixedUpdate()
        {
            defaultObject.Rigid.MovePosition(defaultObject.transform.position + (defaultObject.Dir * Time.deltaTime * defaultObject.MoveSpeed * 10f));
        }

        public override void OnUpdate()
        {
            runTime += Time.deltaTime;
            defaultObject.Anim.SetFloat("RunSpeed", Mathf.Lerp(1.1f, 1.7f, runTime));
        }
    }
}
﻿using System.Collections;
using UnityEngine;

namespace Secret
{
    public abstract class State
    {
        public StateType type { get; protected set; }

        public DefaultObject defaultObject { get; private set; }

        public State(DefaultObject obj)
        {
            defaultObject = obj;
        }

        public abstract IEnumerator OnEnter(params object[] objs);

        public abstract void OnUpdate();
        public abstract void OnFixedUpdate();

        public abstract void OnExit();
    }

    [System.Flags]
    public enum StateType
    {
        Default = 1 << 0,
        Idle = 1 << 1,
        Move = 1 << 2,
        Attack = 1 << 3,
        Skill = 1 << 4,
        Die = 1 << 5,
        Pose = 1 << 6,
    }
}
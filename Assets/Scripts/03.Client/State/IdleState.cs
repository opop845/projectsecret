﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret
{
    public class IdleState : State
    {
        private float runTime;
        public IdleState(DefaultObject obj) : base(obj)
        {
            type = StateType.Idle;
        }

        public override IEnumerator OnEnter(params object[] objs)
        {
            if(defaultObject.target != null)
            {
                defaultObject.SetState(StateType.Attack);
            }else
            {
                defaultObject.SetState(StateType.Move);
            }
            yield return null;
        }

        public override void OnExit()
        {
        }

        public override void OnFixedUpdate()
        {
        }

        public override void OnUpdate()
        {
        }
    }
}
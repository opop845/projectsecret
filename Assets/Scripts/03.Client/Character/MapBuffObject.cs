﻿using PlayFab.ClientModels;
using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Secret
{
    public class MapBuffObject : MonoBehaviour
    {
        public int npcMonsterIdCheck;

        public void InitBuff(Vector3 pos, int npcMonsterId)
        {
            transform.position = pos;
            npcMonsterIdCheck = npcMonsterId;
        }     
    }
}
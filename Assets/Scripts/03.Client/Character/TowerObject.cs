﻿using PlayFab.ClientModels;
using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Secret
{
    public class TowerObject : DefaultObject
    {
        public Monster MonsterData { get; private set; }

        public GameObject TowerModelObject;
        public GameObject Controller;

        public void InitMonster(Monster monster, Vector3 pos)
        {
            transform.position = pos;
            MonsterData = monster;
            Initialize(monster.State);
            ObjectType = ObjectType.Tower;
        }

        protected override void SetDead(DefaultObject obj)
        {
            base.SetDead(obj);
            gameObject.GetComponent<BoxCollider>().enabled = false; // Sanghun, 몬스터 죽으면 바로 콜라이더 꺼버림
            var character = obj as CharacterObject;
            character.Soul += MonsterData.ClearRewardSoul;
            if (MonsterData.ClearRewardSoul > 0 && character.ObjectType == ObjectType.Player)
                GameManager.Instance.DisplaySoulGet(character, MonsterData.ClearRewardSoul);
            //if (character.ObjectType == ObjectType.Player) GameManager.Instance.DisplayGetExp(this, MonsterData.ClearRewardExp); // Sanghun
            Anim.SetTrigger("Die");
            StartCoroutine(SetDisable());
            IEnumerator SetDisable()
            {
                yield return new WaitForSeconds(0.0f);
                GameManager.Instance.MonsterHpbar.Obj = null;
                gameObject.SetActive(false);
                Destroy(gameObject);
            }
        }
    }
}
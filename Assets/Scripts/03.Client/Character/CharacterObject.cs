﻿using Cinemachine;
using Secret;
using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using BehaviorDesigner.Runtime;
using System.Runtime.CompilerServices;
using System.Threading;
using DG.Tweening;
using UnityEngine.UI;

namespace Secret
{
    public class CharacterObject : DefaultObject
    {
        Vector3 SpawnPos;

        #region Model

        public Vector3 CharOriginalModelScale;
        public GameObject Model;

        #endregion 

        public int SkillPoint { get; set; } = 0;
        public int KillPoint { get; private set; } = 0;
        public int TowerDestoryPoint { get; private set; } = 0;
        public Character Character { get; private set; }

        public BattleInventory BattleInventory;

        public RandomSpell RandomItem { get; private set; }

        BattleCamController battleCamController; // Sanghun

        Vector3 StartCharHudTargetPos;
        Vector3 CharHudTargetPos;

        public float FreeRefreshRemainTime = 30; // Sanghun
        public int RefreshFreeCount; // Sanghun
        public bool isRecalling = false;

        public bool isTowerTarget = false;
        public DefaultObject TargetedTower;

        private UniqueAbility uniqueAbility;

        public float KillHeal { get; set; }

        private int _preSoul;
        private float _soul;
        public float Soul
        {
            get => _soul;
            set
            {
                _soul = value;
                if (ObjectType == ObjectType.Player)
                {
                    GameEventManager.Instance.OnEventBattleGoldUpdate();
                    GameManager.Instance.UpdateDisplay();
                    if(_preSoul != Mathf.FloorToInt(_soul))
                    {
                        GameManager.Instance.UpdateSoulInt(_preSoul, Mathf.FloorToInt(_soul));
                        _preSoul = Mathf.FloorToInt(_soul);
                    }
                }
                else if (_soul < 0)
                    Debug.LogError(_soul);
            }
        }
        public int SoulInt => Mathf.FloorToInt(_soul);
        public float MaxSoul => GameDataManager.Instance.GameRule.BattleMaxSoul;

        protected override void OnEventGameState(GameState state)
        {
            base.OnEventGameState(state);
            switch (state)
            {
                case GameState.Init:
                    break;
                case GameState.Ing:
                    SetState(StateType.Move);
                    break;
                case GameState.GameOver:
                    break;
                default:
                    break;
            }
        }


        public void Initialize(ObjectType type, EnemyUser enemyUser = null)
        {
            if (stateType.HasFlag(StateType.Idle)) AddState(new IdleState(this));
            if (stateType.HasFlag(StateType.Move)) AddState(new MoveState(this));
            if (stateType.HasFlag(StateType.Attack)) AddState(new AttackState(this));

            ObjectType = type;
            SpawnPos = transform.position;
            CharOriginalModelScale = Model.transform.localScale;
            _soul = GameDataManager.Instance.GameRule.BattleInitialSoul;
            BattleInventory = new BattleInventory(this);

            if (ObjectType == ObjectType.Player)
            {
                gameObject.layer = LayerMask.NameToLayer("Player");

                Character = GameDataManager.Instance.User.Character;
                RandomItem = new RandomSpell(new List<UserSpell>(GameDataManager.Instance.User.currentCharacter.Deck.Values));
                Initialize(GameDataManager.Instance.User.currentCharacter.State);
                uniqueAbility = gameObject.GetComponent<UniqueAbility>();
                if (uniqueAbility != null)
                {
                    int abilityLevel =  GameDataManager.Instance.User.currentCharacter.CharacterAbilities[0].Level;
                    if (abilityLevel > 0)
                        uniqueAbility.Init(this, abilityLevel);
                    else
                        Destroy(uniqueAbility);
                }
            }
            else
            {
                gameObject.layer = LayerMask.NameToLayer("Enemy");
                Character = enemyUser.Character;
                RandomItem = new RandomSpell(enemyUser.Spell);
                Initialize(Character.State);
            }

            RandomItem.Refresh();
            StartCoroutine(AI_Merge());

        }

        #region Item

        public bool PurchaseItem(Item item)
        {
            return BattleInventory.PurchaseItem(item);
        }

        public void SwitchBattItem(int originIndex, int moveIndex)
        {
            BattleInventory.SwitchBattleItem(originIndex, moveIndex);
        }

        public void SellBattleItem(int index)
        {
            Soul += BattleInventory.SellBattleItem(index);
            GameEventManager.Instance.OnEventBattleItemUpdate();
        }

        #endregion

        public int GetBattleItem(int index) => BattleInventory.GetItemCount(index);

        public bool SetShopRefresh()
        {
            if (Soul >= GameDataManager.Instance.GameRule.RefreshSoulPrice && RefreshFreeCount > 0) // Sanghun
            {
                Soul -= GameDataManager.Instance.GameRule.RefreshSoulPrice;
                RefreshFreeCount--;
                RandomItem.Refresh();
                return true;
            }
            else
                return false;
        }

        public BattleItem GetBattleInventory(ItemType itemType, ItemSubType subType)
        {
            return BattleInventory.GetBattleItem(itemType, subType);
        }

        public BattleItem GetBattleInventory(ItemType itemType)
        {
            return BattleInventory.GetBattleItem(itemType);
        }

        protected override void SetKill(DefaultObject obj)
        {
            base.SetKill(obj);
           // if (behaviorTree != null)
             //   behaviorTree.SetVariableValue("isItemCheck", true);
            //dicAttacker.Clear();

            if (ObjectType == ObjectType.Player)
            {
                //GameManager.Instance.MonsterHpbar.Obj = null;
                if (obj.tag == "EnemyTower")
                {
                    GameManager.Instance.ShowInGameComment(true, false);
                }
                if (obj.ObjectType == ObjectType.Enemy)
                {   
                    GameManager.Instance.ShowInGameComment(true, true);
                    GameManager.Instance.DisplaySoulGet(this, 3);
                    Soul += 3; // 플레이어 죽이면 Soul +3
                    AddBuff(AttrType.MoveSpeed, StatModType.PercentMult, 0.2f, 5.0f, GameDataManager.Instance.GetItem(300601).IconSprite);

                }
            }               

            if (ObjectType == ObjectType.Enemy)
            {
                if (obj.tag == "PlayerTower")
                {
                    GameManager.Instance.ShowInGameComment(false, false);
                }
                if (obj.ObjectType == ObjectType.Player)
                {
                    GameManager.Instance.ShowInGameComment(false, true);
                    Soul += 3; // 플레이어 죽이면 Soul +3  
                    AddBuff(AttrType.MoveSpeed, StatModType.PercentMult, 0.2f, 5.0f, GameDataManager.Instance.GetItem(300601).IconSprite);

                }
            }

            if (obj.ObjectType != ObjectType.Tower)
            {
                if (obj.ObjectType == ObjectType.Player && GameObject.FindGameObjectWithTag("PlayerTower") == null )
                {
                    GameManager.Instance.OnBattleOver(false); // Sanghun
                }

                if (obj.ObjectType == ObjectType.Enemy && GameObject.FindGameObjectWithTag("EnemyTower") == null )
                {
                    GameManager.Instance.OnBattleOver(true); // Sanghun
                }
            }

            if (obj.tag == "Player") KillPoint++;

            if (obj.tag == "PlayerTower" || obj.tag == "EnemyTower") TowerDestoryPoint++;

            //Debug.Log(obj.tag);
            GameManager.Instance.UpdateDisplay();



            SetHeal((int)(Stat.GetStatValue(AttrType.Hp) * KillHeal)); // Sanghun, 무언가 킬하면 25% 회복      

            target = null;
            StartCoroutine(BattleEnd());
            IEnumerator BattleEnd()
            {
                if (ObjectType == ObjectType.Player)
                {
                    if (Anim.GetCurrentAnimatorStateInfo(0).IsName("Skill05"))
                        Anim.SetTrigger("EndAction");
                }
                yield return new WaitForSeconds(0.7f);
                isBattleIng = false;
            }
        }

        protected override void SetDead(DefaultObject obj)
        {
            if (ObjectType == ObjectType.Player)
            {
                GameManager.Instance.RespawnNotice();
            }           
            base.SetDead(obj);

            StopCoroutine("SetRecallStarter");
            isRecalling = false;
            isBattleIng = false;
            if (target != null)
                target.target = null;
            Anim.SetTrigger("Die");
            target = null;

            GameManager.Instance.PlayerObject.isBattleIng = false;
            GameManager.Instance.EnemyObject.isBattleIng = false;
            GameManager.Instance.PlayerObject.target = null;
            GameManager.Instance.EnemyObject.target = null;
         

            foreach (var buff in buffList) 
            {
                buff.DurationTime = 0f;
            }

            // DeadRespawnTime Increase
            RespawnTime = 5.0f + 3.0f * DeadCount;
            DeadCount++;

            if (GameManager.Instance.GameState != GameState.GameOver)
                StartCoroutine(DeadReset());
        }

        
        IEnumerator DeadReset()
        {
            transform.position = SpawnPos; // Sanghun
           
            var obj = gameObject.GetComponent<CharacterObject>().Model;
            obj.transform.localScale = CharOriginalModelScale;
            //obj.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

            yield return new WaitForSeconds(RespawnTime);
            
            Anim.Rebind();
            if (ObjectType == ObjectType.Player)
            {
                GameManager.Instance.isLookPlayer = true;
                GameManager.Instance.MonsterHpbar.Obj = null;
            }
            Hp = (int)Stat.GetStatValue(AttrType.Hp);
            yield return new WaitForSeconds(1f);
            StartCoroutine(HpRestoration());
            StartCoroutine(BuffRoutin());
            if (ObjectType == ObjectType.Enemy)
            {
                StartCoroutine(AI_Merge());
            }
            IsDead = false;
            if (ObjectType == ObjectType.Player) GameManager.Instance.isLookPlayer = false;
            AddBuff(AttrType.MoveSpeed,StatModType.PercentMult, GameDataManager.Instance.GameRule.ReviveMoveSpeed, GameDataManager.Instance.GameRule.ReviveMoveTime, GameDataManager.Instance.GetItem(300601).IconSprite);
            yield return new WaitForSeconds(GameDataManager.Instance.GameRule.ReviveMoveTime);
        }

        public void SetRecall(DefaultObject caster, float CastTime)
        {
            StartCoroutine(SetRecallStarter(caster, CastTime));
        }

       IEnumerator SetRecallStarter(DefaultObject caster, float CastTime)
        {
            Debug.Log("Recall Start");           
            isRecalling = true;            
            yield return new WaitForSeconds(CastTime);

            transform.position = SpawnPos; // Sanghun

            var obj = gameObject.GetComponent<CharacterObject>().Model;
            obj.transform.localScale = CharOriginalModelScale;
            //obj.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

            isBattleIng = false; 
            IsDead = true;

            //List<KeyValuePair<DefaultObject, int>> AttackerList = dicAttacker.ToList();
            //StopCoroutine("BattleStart");
            //for (int i = 0 ; i < AttackerList.Count ; i++)
            //{
            //    Debug.Log("AttackerList.Count:  " + AttackerList.Count);
            //    Debug.Log(AttackerList[i].Key);
            //    AttackerList[i].Key.isBattleIng = false;
            //    AttackerList[i].Key.StopCoroutine("BattleStart");
            //    AttackerList[i].Key.target = null;
            //}
            GameManager.Instance.PlayerObject.target = null;
            GameManager.Instance.EnemyObject.target = null;

            if (ObjectType == ObjectType.Player)
            {
                battleCamController.SetBattleCam("LookPlayer");
                GameManager.Instance.isLookPlayer = true;
                GameManager.Instance.MonsterHpbar.Obj = null;
            }           

            Anim.Rebind();
            SetHeal((int)(Stat.GetStatValue(AttrType.Hp) * 0.50f));   // 귀환하면 50% 회복 
            yield return new WaitForSeconds(1.0f);

            isRecalling = false;
            IsDead = false;
            if (ObjectType == ObjectType.Player) GameManager.Instance.isLookPlayer = false;
            AddBuff(AttrType.MoveSpeed, StatModType.PercentMult, GameDataManager.Instance.GameRule.ReviveMoveSpeed, GameDataManager.Instance.GameRule.ReviveMoveTime, GameDataManager.Instance.GetItem(300601).IconSprite);
            yield return new WaitForSeconds(GameDataManager.Instance.GameRule.ReviveMoveTime);
        }

       

        private void Start()
        {
            // Sanghun
            battleCamController = GameObject.Find("BattleCamController").GetComponent<BattleCamController>();
            StartCharHudTargetPos = HudTarget.transform.position;
            CharHudTargetPos = StartCharHudTargetPos;
        }

        protected override void Update()
        {
            base.Update();
            //if (ObjectType == ObjectType.Player && GameManager.Instance.GameState == GameState.Ing)
            //{
            //    if (Input.GetKeyDown(KeyCode.Q))
            //        behaviorTree?.SetVariableValue("isItemCheck", true);

            //    if (Input.GetKeyDown(KeyCode.K))
            //    {
            //        if (target != null)
            //            target.SetDamage(this, 1000000, false);
            //    }
            //    if (Input.GetKeyDown(KeyCode.W))
            //    {
            //        foreach (var mon in GameManager.Instance.monsterObjects)
            //        {
            //            mon.StopAllCoroutines();
            //        }

            //        GameManager.Instance.PlayerObject.StopAllCoroutines();
            //        GameManager.Instance.EnemyObject.StopAllCoroutines();
            //        GameManager.Instance.OnBattleOver(true);
            //        Anim.SetBool("Run", false);
            //        return;
            //    }
            //    if (Input.GetKeyDown(KeyCode.L))
            //    {
            //        foreach (var mon in GameManager.Instance.monsterObjects)
            //        {
            //            mon.StopAllCoroutines();
            //        }

            //        GameManager.Instance.PlayerObject.StopAllCoroutines();
            //        GameManager.Instance.EnemyObject.StopAllCoroutines();
            //        GameManager.Instance.OnBattleOver(false);
            //        Anim.SetBool("Run", false);
            //        return;
            //    }
            //}


            //if (target != null)
            //{
            //    if (ObjectType == ObjectType.Player)
            //    {
            //        if (target.tag == "EnemyTower")
            //        {
            //            Model.transform.DOLookAt(target.GetComponent<TowerObject>().TowerModelObject.transform.position, 1.0f, AxisConstraint.Y);
            //        }
            //        else if (target.tag == "Monster")
            //        {
            //            Model.transform.DOLookAt(target.transform.position, 1.0f, AxisConstraint.Y);
            //            target.GetComponent<MonsterObject>().MonsterModel.transform.DOLookAt(transform.position, 1.0f, AxisConstraint.Y);
            //        }
            //        else
            //        {
            //            Model.transform.DOLookAt(GameManager.Instance.EnemyObject.transform.position, 1.0f, AxisConstraint.Y);                       
            //        }

            //        //if (target.tag == "Monster")
            //        //{
            //        //    GameManager.Instance.TargetChangeUI.SetActive(true);
            //        //    GameManager.Instance.NullTargetingButton.SetActive(true);
            //        //    GameManager.Instance.TowerTargetingButton.SetActive(false);
            //        //    GameManager.Instance.EnemyTargetingButton.SetActive(true);
            //        //    if ((transform.position - GameManager.Instance.EnemyObject.transform.position).sqrMagnitude < 15f)
            //        //    {
            //        //        GameManager.Instance.EnemyTargetingButton.GetComponent<Button>().interactable = true;
            //        //    }
            //        //    else GameManager.Instance.EnemyTargetingButton.GetComponent<Button>().interactable = false;

            //        //}
            //        //else if (target.tag == "EnemyTower" || target.ObjectType == ObjectType.Enemy )
            //        //{
            //        //    GameManager.Instance.TargetChangeUI.SetActive(true);
            //        //    GameManager.Instance.NullTargetingButton.SetActive(false);

            //        //    if (target.tag == "EnemyTower")
            //        //    {
            //        //        GameManager.Instance.TowerTargetingButton.SetActive(false);
            //        //        GameManager.Instance.EnemyTargetingButton.SetActive(true);
            //        //        if ((transform.position - GameManager.Instance.EnemyObject.transform.position).sqrMagnitude < 15f)
            //        //        {
            //        //            GameManager.Instance.EnemyTargetingButton.GetComponent<Button>().interactable = true;
            //        //        }
            //        //        else GameManager.Instance.EnemyTargetingButton.GetComponent<Button>().interactable = false;
            //        //    }

            //        //    if (target.ObjectType == ObjectType.Enemy)
            //        //    {
            //        //        GameManager.Instance.TowerTargetingButton.SetActive(true);
            //        //        GameManager.Instance.EnemyTargetingButton.SetActive(false);
            //        //        if (TargetedTower != null && TargetedTower.target != null)
            //        //        {
            //        //            //Debug.Log(TargetedTower);
            //        //            GameManager.Instance.TowerTargetingButton.GetComponent<Button>().interactable = true;
            //        //        }
            //        //        else GameManager.Instance.TowerTargetingButton.GetComponent<Button>().interactable = false;
            //        //    }
            //        //}
            //        //else
            //        //{
            //        //    GameManager.Instance.TargetChangeUI.SetActive(false);
            //        //}

            //    }
            //    if (ObjectType == ObjectType.Enemy)
            //    {
            //        if (target.tag == "PlayerTower")
            //        {
            //            Model.transform.DOLookAt(target.GetComponent<TowerObject>().TowerModelObject.transform.position, 1.0f, AxisConstraint.Y);                                       
            //        }
            //        else if (target.tag == "Monster")
            //        {
            //            Model.transform.DOLookAt(target.transform.position, 1.0f, AxisConstraint.Y);
            //            target.GetComponent<MonsterObject>().MonsterModel.transform.DOLookAt(transform.position, 1.0f, AxisConstraint.Y);
            //        }
            //        else
            //        {
            //            Model.transform.DOLookAt(GameManager.Instance.PlayerObject.transform.position, 0.3f, AxisConstraint.Y);                                  
            //        }
            //    }  
            //}
            //else if (target == null)
            //{
            //    if (ObjectType == ObjectType.Player)
            //    {
            //        transform.DOLookAt(GameManager.Instance.EnemyObject.transform.position, 0.3f, AxisConstraint.Y);
            //        GameManager.Instance.TargetChangeUI.SetActive(false);
            //        var obj = gameObject.GetComponent<CharacterObject>().Model;
            //        obj.transform.localScale = CharOriginalModelScale;

            //    }
            //    if (ObjectType == ObjectType.Enemy)
            //    {
            //        Model.transform.DOLookAt(GameManager.Instance.PlayerObject.transform.position, 0.3f, AxisConstraint.Y);
            //        var obj = gameObject.GetComponent<CharacterObject>().Model;
            //        obj.transform.localScale = CharOriginalModelScale;
            //    }
            //}

            //if (IsDead || isBattleIng) return;
            //if (GameManager.Instance.GameState != GameState.Ing) return;

            //if (isRecalling)
            //{
            //    Anim.SetFloat("RunSpeed", RunSpeed);
            //    Anim.SetBool("Run", false);
            //    return;
            //}

            //if (ObjectType == ObjectType.Player)
            //{
            //    if ((transform.position - GameManager.Instance.EnemyObject.transform.position).sqrMagnitude < 15f && GameManager.Instance.EnemyObject.IsDead == false)
            //    {                  
            //        GameManager.Instance.EnemyObject.BattleTarget(this);                  
            //        BattleTarget(GameManager.Instance.EnemyObject);
            //    }

            //    if (target == null)
            //    {
            //        transform.GetComponent<Rigidbody>().MovePosition(transform.position + transform.forward * Time.deltaTime * Stat.GetStatValue(AttrType.MoveSpeed) * 10f);
            //        GameManager.Instance.DisplayRunEffect(this); // Sanghun
            //        RunTime += Time.deltaTime;
            //        RunSpeed = Mathf.Lerp(1.1f, 1.7f, RunTime);
            //        //animator.SetFloat("RunSpeed", RunSpeed);
            //        Anim.SetBool("Run", true);

            //        CharHudTargetPos.y = StartCharHudTargetPos.y - 0.5f; // Sanghun
            //        HudTarget.transform.localPosition = CharHudTargetPos; // Sanghun
            //    }
            //    else
            //    {
            //        Anim.SetFloat("RunSpeed", RunSpeed);
            //        Anim.SetBool("Run", false);
            //    }
            //}

            //if (ObjectType == ObjectType.Enemy)
            //{
            //    if ( (transform.position - GameManager.Instance.PlayerObject.transform.position).sqrMagnitude < 15f && GameManager.Instance.PlayerObject.IsDead == false) // Sanghun
            //    {
            //        GameManager.Instance.PlayerObject.BattleTarget(this); // Sanghun
            //        BattleTarget(GameManager.Instance.PlayerObject); // Sanghun
            //        //animator.SetBool("Run", false);
            //        //return;
            //    }

            //    if (target == null)
            //    {
            //        transform.Translate(-transform.forward * Time.deltaTime * Stat.GetStatValue(AttrType.MoveSpeed) * 10f);
            //        Anim.SetBool("Run", true);
            //    }
            //    else
            //    {
            //        Anim.SetBool("Run", false);
            //    }
            //}

            //if (GameManager.Instance.GameState == GameState.GameOver)
            //{
            //    StopCoroutine(DeadReset());
            //    uniqueAbility?.Relese();
            //}

        }

        private void OnTriggerEnter(Collider other)
        {          
            if (ObjectType == ObjectType.Enemy && other.tag == "PlayerTower")
            {
                var monster = other.GetComponent<DefaultObject>();
                monster.BattleTarget(this);
                BattleTarget(monster);
               GameManager.Instance.TowerHpbar.Init(monster, Camera.main, GameManager.Instance.Canvas);
                //GameManager.Instance.TempMiniMap.SetActive(false);
            }
            else if (ObjectType == ObjectType.Player && other.tag == "EnemyTower")
            {
                var monster = other.GetComponent<DefaultObject>();
                monster.BattleTarget(this);
                BattleTarget(monster);
                GameManager.Instance.TowerHpbar.Init(monster, Camera.main, GameManager.Instance.Canvas);
                //GameManager.Instance.TempMiniMap.SetActive(false);
            }   
            else if(ObjectType == ObjectType.Player && other.GetComponent<CharacterObject>() != null)
            {
                var monster = other.GetComponent<DefaultObject>();
                monster.BattleTarget(this);
                BattleTarget(monster);
            }
        }

        #region AI
        

        public void AI_Start()
        {
            StartCoroutine(AI_Merge());
        }

        IEnumerator AI_Merge()
        {
            while (true)
            {
                yield return YieldInstructionCache.WaitForSeconds(1f);

                for (int i = 0; i < 6; i++)
                {
                    for (int j = i + 1; j < 6; j++)
                    {
                        BattleInventory.MergeBattleItem(i, j);
                    }
                }
            }
        }
        IEnumerator AI_HPItemCheck()
        {
            while(true)
            {
                if(HpSlide<0.5f)
                {
                   // AI_HpItemCheck
                }
            }
        }

        public bool AI_HpItemCheck()
        {
            int index = 0;
            foreach (var spell in RandomItem.ShopSpells)
            {
                if (spell.SpellType == SpellType.Item && Soul >= spell.Soul && spell.Item.ItemType == ItemType.Get && spell.Item.SubType.Contains(ItemSubType.Hp))
                {
                    if (PurchaseItem(spell.Item))
                    {
                        Soul -= spell.Soul;
                        RandomItem.PurchaseItem(index);
                        return true;
                    }
                    else
                    {
                        spell.Item.UseItem(this);
                        Soul -= spell.Soul;
                        RandomItem.PurchaseItem(index);
                        return true;
                    }
                }
                index++;
            }

            return false;
        }

        public void AI_BuffItemCheck()
        {
            var buffItem = GetBattleInventory(ItemType.Buff);
            if (buffItem != null)
                buffItem.UseBattleItem();
        }

        private int purchaseItemIndex = -1;
        private List<int> randomPurchaseList = new List<int>();

        public bool AI_PurchaseItemCheck()
        {
            purchaseItemIndex = -1;
            randomPurchaseList.Clear();

            for (int i = 0; i < RandomItem.ShopSpells.Length; i++)
            {
                int index = i;
                if (Soul >= RandomItem.ShopSpells[i].Soul && RandomItem.ShopSpells[i].SpellType != SpellType.Skill) // Sanghun
                    randomPurchaseList.Add(index);
            }

            if (randomPurchaseList.Count > 0)
            {
                randomPurchaseList.Shuffle();
                purchaseItemIndex = randomPurchaseList[0];
                return true;
            }
            else
                return false;
        }



        public void AI_ItemPurchase()
        {
            var spell = RandomItem.ShopSpells[purchaseItemIndex];
            if (Soul < spell.Soul) return;

            if (spell.SpellType == SpellType.Item)
            {
                if (PurchaseItem(spell.Item))
                {
                    Soul -= spell.Soul;
                    RandomItem.PurchaseItem(purchaseItemIndex);
                }
                else
                {
                    if (spell.Item.ItemType == ItemType.Equipment)
                        SetShopRefresh();
                    else
                    {
                        spell.Item.UseItem(this);
                        RandomItem.PurchaseItem(purchaseItemIndex);
                        Soul -= spell.Soul;
                    }
                }
            }
        }

        public void AI_ItemMerge()
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = i + 1; j < 6; j++)
                {
                    BattleInventory.MergeBattleItem(i, j);
                }
            }
        }

        public void AI_ItemUse()
        {
            var item = GetBattleInventory(ItemType.Get, ItemSubType.Hp);

            if (item != null)
                item.UseBattleItem();
        }

        #endregion
    }
}
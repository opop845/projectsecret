﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret
{
    public class StateMachines : MonoBehaviour
    {
        #region Transform

        private Transform _cachedTransform;
        public new Transform transform
        {
            get
            {
                if (ReferenceEquals(_cachedTransform, null))
                    _cachedTransform = base.transform;
                return _cachedTransform;
            }
        }

        #endregion

        public Animator anim { get; private set; }
        public State currentState { get; private set; }
        public Quaternion direction { get; protected set; }

        Dictionary<StateType, State> stateDic = new Dictionary<StateType, State>();

     
        protected virtual void Awake() { }

        protected virtual void Start() { }

        protected virtual void OnDestroy() { }

        protected virtual void OnEnable() { }

        protected virtual void OnDisable() { }

        protected virtual void Update()
        {
            currentState.OnUpdate();
        }

        protected virtual void Initialize()
        {
            anim = GetComponent<Animator>();
        }
    }
}
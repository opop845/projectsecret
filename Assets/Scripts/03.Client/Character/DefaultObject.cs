﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks.Unity.UnityGameObject;
using DG.Tweening;
using Photon.Pun;
using Photon.Pun.Demo.PunBasics;
using Secret;
using Secret.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
//using System.Diagnostics;
using UnityEngine;
using UnityEngine.Video;
using Random = UnityEngine.Random;

namespace Secret
{
    public abstract class DefaultObject : MonoBehaviour
    {
        public const float assistTime = 5f;
        #region Transform

        private Transform _cachedTransform;
        public new Transform transform
        {
            get
            {
                if (ReferenceEquals(_cachedTransform, null))
                    _cachedTransform = base.transform;
                return _cachedTransform;
            }
        }

        #endregion

        #region Action

        public event Action<DefaultObject> EventHpChanged;
        public event Action<DefaultObject> EventAttack;
        public event Action<DefaultObject, DefaultObject> EventKill; //attacker, target

        void OnEventKill(DefaultObject target) => EventKill?.Invoke(this, target);
        void OnEventHpChanged() => EventHpChanged?.Invoke(this);


        #endregion

        public Animator Anim { get; private set; }
        public Rigidbody Rigid { get; private set; }
        public Vector3 Dir { get; protected set; }
        public Dictionary<DefaultObject, AttackerInfo> attackerDic = new Dictionary<DefaultObject, AttackerInfo>();

        protected virtual void Awake()
        {
            BattleEventManager.Instance.EventGameState += OnEventGameState;
        }

        protected virtual void OnDestroy()
        {
            BattleEventManager.Instance.EventGameState -= OnEventGameState;
        }

        protected virtual void Update()
        {
            currentState?.OnUpdate();
        }

        protected virtual void FixedUpdate()
        {
            currentState?.OnFixedUpdate();
        }

        #region StateMachines

       protected Dictionary<StateType, State> stateDic = new Dictionary<StateType, State>();
        public State currentState { get; protected set; }
        public StateType stateType;
        public StateType currentType;
        public StateType preType;
        public Coroutine coroutineEnter = null;

        public bool CheckState(StateType type) => stateDic.ContainsKey(type);

        public void SetState(StateType type, params object[] objs)
        {
            if (!CheckState(type)) return;
            if (currentType.Equals(type)) return;
            SetStop();
            preType = currentType;
            currentState = stateDic[type];
            currentType = type;
            coroutineEnter = StartCoroutine(currentState.OnEnter(objs));
        }

        public void SetStop()
        {
            if (currentState != null)
            {
                currentState.OnExit();
                preType = currentState.type;
                if (coroutineEnter != null) StopCoroutine(coroutineEnter);
            }
        }

        public void AddState(State state)
        {
            if (state == null) return;
            if (stateDic.ContainsKey(state.type))
                stateDic[state.type] = state;
            else
                stateDic.Add(state.type, state);
        }

        #endregion

        #region Stat

        public CharacterStatTable Stat { get; protected set; }
        public float HpSlide => Hp / Stat.GetStatValue(AttrType.Hp);
        public int Attack => (int)Stat.GetStatValue(AttrType.Attack);
        public float AttackSpeed => Stat.GetStatValue(AttrType.AttackSpeed);
        public float MoveSpeed => Stat.GetStatValue(AttrType.MoveSpeed);
        public float CriticalChance => Stat.GetStatValue(AttrType.CriticalChance);

        public bool Critical => Random.value < CriticalChance;

        private int _hp;
        public int Hp
        {
            get => _hp;
            set
            {
                if (IsDead) return;
                _hp = Mathf.Clamp(value, 0, (int)Stat.GetStatValue(AttrType.Hp));
                OnEventHpChanged();
            }
        }

        public bool IsDead { get; protected set; } = false;
        public int DeadCount { get; protected set; } = 0;

        #endregion

        public bool isBattleIng = false;
        private SkinnedMeshRenderer skinnedMesh;
        private Color mainColor;


        public float RespawnTime = 5.0f;


        public DefaultObject target;
        public Transform HudTarget;
        public Transform HealHudTarget;
        public List<BuffController> buffList = new List<BuffController>();

        
        public ObjectType ObjectType { get; protected set; }

        protected void Initialize(CharacterStatTable state)
        {
            Anim = GetComponent<Animator>();
            Rigid = GetComponent<Rigidbody>();
            skinnedMesh = GetComponentInChildren<SkinnedMeshRenderer>();
            if (skinnedMesh !=null)
                mainColor = skinnedMesh.material.GetColor("_Color");
            Stat = new CharacterStatTable(state);
            _hp = (int)state.GetStatValue(AttrType.Hp);
            Dir = transform.forward;
            StartCoroutine(HpRestoration());
            StartCoroutine(BuffRoutin());
            StartCoroutine(AssistCheck());
        }

        protected virtual void OnEventGameState(GameState state) { }

        //어시스트 체크
        protected IEnumerator AssistCheck()
        {
            while (true)
            {
                if (attackerDic.Count > 0)
                {
                    foreach (var info in attackerDic)
                        info.Value.SetTimer();
                }

                yield return YieldInstructionCache.WaitForSeconds(0.1f);
            }
        }

        protected IEnumerator HpRestoration()
        {
            while (true)
            {
                yield return YieldInstructionCache.WaitForSeconds(1f);
                Hp += (int)(Stat.GetStatValue(AttrType.Hp) * Stat.GetStatValue(AttrType.HpRestorationRate));
            }
        }

        protected IEnumerator BuffRoutin()
        {
            while (true)
            {
                foreach (var buff in buffList)
                {
                    buff.DurationTime -= Time.deltaTime;
                }
                buffList.RemoveAll(x => x.DurationTime <= 0f);
                yield return YieldInstructionCache.WaitForSeconds(0.1f);
            }
        }

   
        public virtual void SetDamage(DefaultObject attacker, int damage, bool isCritical)
        {
            if (!IsDead)
            {
                StartCoroutine(HitColor());
                if (target == null)
                    BattleTarget(attacker);

                if (!attackerDic.ContainsKey(attacker))
                    attackerDic.Add(attacker, new AttackerInfo(damage, assistTime));
                else
                    attackerDic[attacker].Reset(damage, assistTime);

                Hp -= damage;
                IsDead = Hp <= 0;
                if (attacker.GetComponent<TowerObject>() == null)  GameManager.Instance.DisplayDamage(this, damage, isCritical);
                else if (damage > 0) GameManager.Instance.DisplayDamageTower(this, damage);

                //Sanghun, Hit 연출
                if (damage > 0)
                {
                    GameManager.Instance.DisplayHitEffect(attacker.target, isCritical);     

                    if (attacker.tag == "Player")
                    {
                        CinemachineShake.Instance.ShakeCamera(0.5f, 0.1f); // Sanghun
                        if (tag == "PlayerTower" || tag == "EnemyTower")
                        {
                            GameManager.Instance.DisplayTowerHitEffect(attacker.target, isCritical);
                            if (tag == "PlayerTower") GameManager.Instance.ShowTowerAttackedNoti();
                        }
                    }
                }
                EventAttack?.Invoke(this);

                if (IsDead)
                {
              

                
                    SetDead(attacker);
                }
            }
        }

        public void SetPerDamage(DefaultObject attacker,float damage, PerDamageType type,bool isCritical)
        {
            int realDamage = 0;
            switch (type)
            {
                case PerDamageType.MaxHp:
                    realDamage = Mathf.RoundToInt(Stat.GetStatValue(AttrType.Hp) * damage);
                    break;
                case PerDamageType.CurrentHp:
                    realDamage = Mathf.RoundToInt(Hp * damage);
                    break;
            }

            SetDamage(attacker, realDamage, isCritical);
        }

        IEnumerator HitColor()
        {
            skinnedMesh.material.SetColor("_Color", Color.red);
            yield return YieldInstructionCache.WaitForSeconds(0.1f);
            skinnedMesh.material.SetColor("_Color", mainColor);
        }

        public virtual void SetHeal(int heal)
        {
            if (!IsDead) 
            {
                Hp += heal;
                GameManager.Instance.DisplayHeal(this, heal);
            }

            //else if (GetComponent<CharacterObject>() != null)
            //{
            //    if (GetComponent<CharacterObject>().isRecalling == true)
            //    {
            //        Hp += heal;
            //        GameManager.Instance.DisplayHeal(this, heal);
            //    }
            //}
        }

        public virtual void SetHeal(float heal)
        {
            if (!IsDead)
            {
                int value = Mathf.RoundToInt(Stat.GetStatValue(AttrType.Hp) * heal);
                Hp += value;
                GameManager.Instance.DisplayHeal(this, value);
            }

            //else if (GetComponent<CharacterObject>() != null)
            //{
            //    if (GetComponent<CharacterObject>().isRecalling == true)
            //    {
            //        int value = Mathf.RoundToInt(Stat.GetStatValue(AttrType.Hp) * heal);
            //        Hp += value;
            //        GameManager.Instance.DisplayHeal(this, value);
            //    }
            //}

        }

        public virtual void AddBuff(AttrType attrType, StatModType statType, float value, float duractionTime, Sprite sprite)
        {
            var buff = buffList.Find(x => x.attrType == attrType);
            if (buff != null)
                buff.UpdateBuff(value, duractionTime);
            else
            {
                buff = new BuffController(this, attrType, statType, value, duractionTime, sprite);
                buffList.Add(buff);
                GameEventManager.Instance.OnEventNewBuff(this, buff);
            }

            StartCoroutine(ShowBuffEffect(attrType, duractionTime));

            if (attrType == AttrType.AttackSpeed)
            {
                //StartCoroutine(AttackAnimationSpeedUp(value, duractionTime));
            }
        }

        IEnumerator ShowBuffEffect(AttrType attrtype, float durationTime)
        {
            GameObject a = null;

            if (attrtype == AttrType.MoveSpeed)
                a = Instantiate(ResourceManager.Instance.LoadAsset<GameObject>("Effects/MovementBuffEffect"), gameObject.transform);

            else if (attrtype == AttrType.AttackSpeed)
                a = Instantiate(ResourceManager.Instance.LoadAsset<GameObject>("Effects/AttackSpeedBuffEffect"), gameObject.transform);

            else if(attrtype == AttrType.CriticalChance)
                a = Instantiate(ResourceManager.Instance.LoadAsset<GameObject>("Effects/CriticalChanceBuffEffect"), gameObject.transform);

            else if(attrtype == AttrType.CriticalDamage)
                a = Instantiate(ResourceManager.Instance.LoadAsset<GameObject>("Effects/CriticalDamageBuffEffect"), gameObject.transform);

            if (attrtype == AttrType.SuckHp)
                a = Instantiate(ResourceManager.Instance.LoadAsset<GameObject>("Effects/DrainEffect"), gameObject.transform);

            yield return new WaitForSeconds(durationTime);
            Destroy(a);
        }

        protected virtual void SetDead(DefaultObject attacker)
        {
            foreach (var obj in attackerDic)
            {
                if (obj.Key == attacker)
                    obj.Key.SetKill(this);
                else if (obj.Value.assistTime > 0f)
                    obj.Key.SetAssist(this);
            }
            transform.localScale = Vector3.one;
            skinnedMesh.material.SetColor("_Color", mainColor);
            GameEventManager.Instance.OnEventKillNotice(attacker, this);
            //Transform[] childList = GetComponentsInChildren<Transform>(true);
            //if (childList != null)
            //{
            //    for (int i = 0; i < childList.Length; i++)
            //    {
            //        if (childList[i].tag == "BuffEffect")
            //            Destroy(childList[i].gameObject);
            //    }
            //}

        }
        protected virtual void SetKill(DefaultObject target)
        {
            OnEventKill(target);
            GameManager.Instance.DisplayDeadEffect(target); // Sanghun, 몬스터 DeadEffect
            StopCoroutine("BattleStart");
        }

        protected virtual void SetAssist(DefaultObject target)
        {

        }



        public void BattleTarget(DefaultObject obj)
        {
            if (target == null)
            {
                target = obj;
                isBattleIng = true;
                // Anim.SetBool("Run", false);
                // StartCoroutine("BattleStart");
                SetState(StateType.Attack);

                if (tag == "PlayerTower" || tag == "EnemyTower")
                {
                    if (GetComponent<TowerObject>().Controller != null)
                        GetComponent<TowerObject>().Controller.GetComponent<mobaTower>().target = target.transform;
                }

                //if (this.tag == "Player" &&  obj.tag == "Monster")
                //{
                //    //transform.DOLookAt(obj.transform.position, 0.3f, AxisConstraint.Y);
                //    obj.transform.DOLookAt(transform.position, 0.3f, AxisConstraint.Y);
                //    //if (ObjectType == ObjectType.Player) GameManager.Instance.PlayerIndex++;
                //    //else if (ObjectType == ObjectType.Enemy) GameManager.Instance.EnemyIndex--;
                //}

                //if (obj.tag == "PlayerTower")
                //{
                //    GameManager.Instance.PlayerObject.isTowerTarget = true;
                //}

                //if (obj.tag == "EnemyTower")
                //{
                //    GameManager.Instance.EnemyObject.isTowerTarget = true;
                //}

            }    
        }

        public void ForceBattleTarget(DefaultObject obj)
        {
            if (obj == null)
            {
                //StopAllCoroutines();
                //target.StopAllCoroutines();k
                StopCoroutine("BattleStart");
                target.StopCoroutine("BattleStart");
                target.target = null;
                target.isBattleIng = false;
                target = null;
                isBattleIng = false;
                Anim.SetBool("Run", true);
            }
            else if (target != obj)
            {
                StopCoroutine("BattleStart");
                target = obj;
                isBattleIng = true;
                Anim.SetBool("Run", false);
                StartCoroutine("BattleStart");
            }
        }

        protected virtual IEnumerator BattleStart()
        {
            while (true)
            {
                // 평타 애니메이션 랜덤 재생
                //int randomNumber = Random.Range(1, 4);

                if (tag == "Player" || tag == "Monster")
                {
                    Anim?.SetTrigger("Attack" + "1");
                }               

                yield return new WaitForSeconds(1.0f / Stat.GetStatValue(AttrType.AttackSpeed) * 0.3f);

                if (target != null && !target.IsDead)
                {
                    if (gameObject.GetComponent<TowerObject>() == null)
                    {
                        var isCritical = Random.value <= Stat.GetStatValue(AttrType.CriticalChance);
                        var damage = isCritical ? (int)(Stat.GetStatValue(AttrType.Attack) * Stat.GetStatValue(AttrType.CriticalDamage)) : (int)Stat.GetStatValue(AttrType.Attack);
                        target.SetDamage(this, damage, isCritical);
                        var suck = Stat.GetStatValue(AttrType.SuckHp);
                        if (suck > 0f)
                            SetHeal(Mathf.CeilToInt(damage * suck));
                    }                        
                }

                yield return new WaitForSeconds(1.0f / Stat.GetStatValue(AttrType.AttackSpeed) * 0.7f);

                if (target != null && !target.IsDead)
                {
                    if (gameObject.GetComponent<TowerObject>() != null)
                    {
                        var isCritical = Random.value <= Stat.GetStatValue(AttrType.CriticalChance);
                        var damage = isCritical ? (int)(Stat.GetStatValue(AttrType.Attack) * Stat.GetStatValue(AttrType.CriticalDamage)) : (int)Stat.GetStatValue(AttrType.Attack);
                        target.SetDamage(this, damage, isCritical);
                        var suck = Stat.GetStatValue(AttrType.SuckHp);
                        if (suck > 0f)
                            SetHeal(Mathf.CeilToInt(damage * suck));
                    }
                }
            }
        }

        #region Event

        public virtual void SetAttack() => target?.SetDamage(this, Attack, Critical);

        #endregion
    }

    public struct AttackerInfo
    {
        public int damage;
        public float assistTime;

        public AttackerInfo(int damage, float assistTime)
        {
            this.damage = damage;
            this.assistTime = assistTime;
        }
        
        public void Reset(int damage, float time)
        {
            this.damage += damage;
            assistTime = time;
        }

        public void SetTimer()
        {
            if (assistTime > 0f)
                assistTime -= 0.1f;
        }
    }

    public enum ObjectType
    {
        Player,
        Enemy,
        Tower
    }

    public enum PerDamageType
    {
        MaxHp,
        CurrentHp,
    }
}

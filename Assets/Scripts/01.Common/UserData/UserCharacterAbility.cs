﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.Data
{
    public class UserCharacterAbility
    {
        public readonly CharacterAbility characterAbility;
        public int Level { get; private set; }
        public StatValue StatValue { get; private set; }
        public int UsedAbilityPoint { get; private set; }

        public UserCharacterAbility(int characterIndex, int abilityIndex, int level = 0)
        {
            Level = level;
            characterAbility = GameDataManager.Instance.GetCharacterAbility(characterIndex, abilityIndex);
            if (characterAbility.AbilitySubType != AttrType.None)
                StatValue = new StatValue(characterAbility.AbilityParameter * Level, characterAbility.AbilitySubType, StatModType.Flat);
            UsedAbilityPoint = Level * characterAbility.RequiredAbilityPoint;
        }

        public void LevelUpAbility(int level)
        {
            Level = level;
            if (characterAbility.AbilitySubType != AttrType.None)
                StatValue.Value = characterAbility.AbilityParameter * Level;
            UsedAbilityPoint = Level * characterAbility.RequiredAbilityPoint;
        }
    }

}
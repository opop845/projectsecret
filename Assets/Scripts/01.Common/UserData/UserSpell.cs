﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Secret.Server;

namespace Secret.Data
{
    public class UserSpell
    {
        public readonly int Index;
        public int Level { get; set; }
        public int Count { get; set; }
        public Spell Spell { get; private set; }

        public UserSpell(UserSpellBody body)
        {
            Index = body.Index;
            Level = body.Level;
            Count = body.Count;
            Spell = GameDataManager.Instance.GetSpell(Index);
        }

        public UserSpell(int index, int level)
        {
            Index = index;
            Level = level;
            Count = 0;
            Spell = GameDataManager.Instance.GetSpell(Index);
        }
    }
}
﻿using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Storage;
using PlayFab.ClientModels;
using PlayFab.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Secret.Server;
using System.Linq;

namespace Secret.Data
{
    public class User
    {
        #region UserData

        public string NickName;

        public string PlayerId { get; private set; }

        #endregion

        public Dictionary<string, string> serverData = new Dictionary<string, string>();
        public int Gold { get; set; }
        public int Gem { get; set; }
        public int Trophy { get; set; }

        public int HighTrophy { get; set; }
        public int TrophyRewardIndex { get; set; }
        public RewardTrophy NextRewardTrophy
        {
            get
            {
                if (GameDataManager.Instance.RewardTrophies.TryGetValue(TrophyRewardIndex+1, out var value))
                    return value;
                else
                    return GameDataManager.Instance.RewardTrophies.Last().Value;
            }
        }

        public EnemyUser EnemyUser;
        public Character Character => currentCharacter.Character;

        public List<UserSpell> userSpells;
        public List<UserSpell> remainSpells = new List<UserSpell>();
        public Dictionary<int, UserCharacter> userCharactersDic;
        public LinkedList<UserCharacter> linkUserCharacters = new LinkedList<UserCharacter>();
        private UserCharacter _currentCharacter;
        public UserCharacter currentCharacter
        {
            get => _currentCharacter;
            set
            {
                _currentCharacter = value;
                SetUserSpell();
                ServerManager.Instance.isDeckSave = true;
                GameEventManager.Instance.OnEventCharacterChanged(_currentCharacter);
            }
        }

        public void NextCharacter()
        {
            if (linkUserCharacters.Count <= 1) return;
            var node = linkUserCharacters.Find(currentCharacter).Next;
            if (node != null)
                currentCharacter = node.Value;
            else
                currentCharacter = linkUserCharacters.First.Value;
        }

        public void PreCharacter()
        {
            if (linkUserCharacters.Count <= 1) return;
            var node = linkUserCharacters.Find(currentCharacter).Previous;
            if (node != null)
                currentCharacter = node.Value;
            else
                currentCharacter = linkUserCharacters.Last.Value;
        }

        public void ResetLinkUserCharacter()
        {
            linkUserCharacters.Clear();
            foreach (var item in userCharactersDic)
            {
                if (!item.Value.IsLock)
                    linkUserCharacters.AddLast(new LinkedListNode<UserCharacter>(item.Value));
            }
        }

        public void Initialize(GetPlayerCombinedInfoResultPayload data)
        {
            NickName = data.PlayerProfile.DisplayName;
            PlayerId = data.PlayerProfile.PlayerId;
            foreach (var statistic in data.PlayerStatistics)
            {
                switch (statistic.StatisticName)
                {
                    case "Trophy":
                        Trophy = statistic.Value;
                        break;
                }
            }

            Gem = data.UserVirtualCurrency["GM"];
            Gold = data.UserVirtualCurrency["GG"];

            userSpells = PlayFabSimpleJson.DeserializeObject<List<UserSpellBody>>(data.UserData["Spell"].Value).Select(x => new UserSpell(x)).ToList();
            var userCharacterBodies = PlayFabSimpleJson.DeserializeObject<UserCharacterBody[]>(data.UserData["Character"].Value);
            userCharactersDic = new Dictionary<int, UserCharacter>();
            foreach (var characterData in userCharacterBodies)
            {
                if (GameDataManager.Instance.GetCharacter(characterData.Index) == null) continue;
                userCharactersDic.Add(characterData.Index, new UserCharacter(characterData));
            }
            
            if (data.UserData.TryGetValue("EnemyUser",out var enemy))
                EnemyUser = new EnemyUser(PlayFabSimpleJson.DeserializeObject<EnemyUserBody>(enemy.Value));
            foreach (var item in userCharactersDic)
            {
                linkUserCharacters.AddLast(new LinkedListNode<UserCharacter>(item.Value));
            }
            foreach (var character in GameDataManager.Instance.CharacterDict)
            {
                if (!userCharactersDic.ContainsKey(character.Key))
                    userCharactersDic.Add(character.Key,new UserCharacter(character.Key));
            }

            var userInfo = PlayFabSimpleJson.DeserializeObject<UserInfoBody>(data.UserData["UserInfo"].Value);
            if (!userCharactersDic.TryGetValue(userInfo.Character, out _currentCharacter))
                _currentCharacter = userCharactersDic[100001];
            HighTrophy = userInfo.HighTrophy;
            TrophyRewardIndex = userInfo.TrophyRewardIndex;

            SetUserSpell();
        }

        #region Spell

        void SetUserSpell()
        {
            remainSpells.Clear();
            remainSpells.AddRange(userSpells);
            remainSpells.RemoveAll(x => currentCharacter.Deck.Values.Any(y => y != null && y.Index == x.Index));
            remainSpells.RemoveAll(x => x.Spell.CharacterIndex != 0 && x.Spell.CharacterIndex != currentCharacter.CharacterIndex);
        }

        public float GetUserDeckSoulAverage()
        {
            float soul = 0f;
            int count = 0;
            foreach (var spell in currentCharacter.Deck)
            {
                if (spell.Value != null)
                {
                    count++;
                    soul += spell.Value.Spell.Soul;
                }
            }
            return soul / count;
        }

        public void SetDeckOutSpell(UserSpell spell)
        {
            ServerManager.Instance.isDeckSave = true;
            int key = currentCharacter.Deck.FirstOrDefault(x => x.Value == spell).Key;
            currentCharacter.Deck[key] = null;
            remainSpells.Add(spell);
            remainSpells.Sort((x, y) => x.Index.CompareTo(y.Index));
            GameEventManager.Instance.OnEventDeckSpellChanged();
        }

        public void SetDeckInSpell(UserSpell spell, int index = -1)
        {
            ServerManager.Instance.isDeckSave = true;
            if (userSpells == null) return;
            if (index >= 0)
                currentCharacter.Deck[index] = spell;
            else
            {
                bool isIn = false;
                foreach (var deck in currentCharacter.Deck)
                {
                    if (deck.Value == null)
                    {
                        currentCharacter.Deck[deck.Key] = spell;
                        isIn = true;
                        break;
                    }
                }
                if (!isIn)
                {
                    GameEventManager.Instance.OnEventToast("덱을 하나이상 비워주세요");
                    return;
                }
            }
            remainSpells.Remove(spell);
            GameEventManager.Instance.OnEventDeckSpellChanged();
        }

        public UserSpell GetUserSpell(int index) => userSpells.Find(x => x.Index == index);
        public void AddUserSpell(UserSpellBody body)
        {
            if (userSpells.Find(x => x.Index == body.Index) != null) return;

            userSpells.Add(new UserSpell(body));
            remainSpells.Add(new UserSpell(body));
            userSpells.Sort((x, y) => x.Index.CompareTo(y.Index));
            remainSpells.Sort((x, y) => x.Index.CompareTo(y.Index));
        }

        #endregion

    }
}
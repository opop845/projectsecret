﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Secret.Server;
using PlayFab.Json;
using System.Linq;

namespace Secret.Data
{
    public class UserCharacter
    {
        public readonly int CharacterIndex;
        public readonly Character Character;
        public int Level;
        public int Exp;
        public Dictionary<int, UserSpell> Deck;
        public CharacterExpertLevel NextLevel;
        public List<UserCharacterAbility> CharacterAbilities;
        public List<Spell> CharacterSpell;
        public CharacterStatTable State;

        public bool IsLock { get; private set; }
        public int OwnedAbilityPoint { get; private set; }
        public int RemainAbilityPoint => OwnedAbilityPoint - CharacterAbilities.Sum(x => x.UsedAbilityPoint);

        public UserCharacter(int index)
        {
            CharacterIndex = index;
            Character = GameDataManager.Instance.GetCharacter(CharacterIndex);
            Level = 0;
            Exp = 0;

            CharacterAbilities = new List<UserCharacterAbility>();
            State = new CharacterStatTable(Character.State);

            for (int i = 0; i < 5; i++)
            {
                var ability = new UserCharacterAbility(CharacterIndex, i + 1);
                if (ability.StatValue != null)
                    State.AddStat(ability.StatValue);
                CharacterAbilities.Add(ability);
            }

            Deck = new Dictionary<int, UserSpell>();
            for (int i = 0; i < 8; i++)
            {
                int count = i;
                Deck.Add(count, null);
            }

            NextLevel = GameDataManager.Instance.GetCharacterExpertLevel(CharacterIndex, 1);
            CharacterSpell = new List<Spell>();
            foreach (var level in GameDataManager.Instance.CharacterExpertLevels[CharacterIndex])
            {
                if (level.RewardSpell != null)
                    CharacterSpell.Add(level.RewardSpell);
            }

            IsLock = true;
            OwnedAbilityPoint = 0;
        }

        public UserCharacter(UserCharacterBody body)
        {
            CharacterIndex = body.Index;
            Character = GameDataManager.Instance.GetCharacter(CharacterIndex);
            Level = body.Level;
            Exp = body.Exp;
            State = new CharacterStatTable(Character.State);

            CharacterAbilities = new List<UserCharacterAbility>();
            for (int i = 0; i < body.Ability.Length; i++)
            {
                var ability = new UserCharacterAbility(CharacterIndex, i + 1, body.Ability[i]);
                if (ability.StatValue != null)
                    State.AddStat(ability.StatValue);
                CharacterAbilities.Add(ability);
            }

            Deck = new Dictionary<int, UserSpell>();
            for (int i = 0; i < body.Deck.Length; i++)
            {
                int index = i;
                Deck.Add(index, GameDataManager.Instance.User.GetUserSpell(body.Deck[i]));
            }
            NextLevel = GameDataManager.Instance.GetCharacterExpertLevel(CharacterIndex, Level+1);
            CharacterSpell = new List<Spell>();
            foreach (var level in GameDataManager.Instance.CharacterExpertLevels[CharacterIndex])
            {
                if (level.RewardSpell != null)
                    CharacterSpell.Add(level.RewardSpell);
            }

            IsLock = false;
            OwnedAbilityPoint = GameDataManager.Instance.CharacterExpertLevels[CharacterIndex].FindAll(x => x.Level <= Level).Sum(y => y.RewardAbilityPoint);
        }

        public void UnLock(UserCharacterBody body )
        {
            Level = body.Level;
            NextLevel = GameDataManager.Instance.GetCharacterExpertLevel(CharacterIndex, Level + 1);
            IsLock = false;
            OwnedAbilityPoint = GameDataManager.Instance.CharacterExpertLevels[CharacterIndex].FindAll(x => x.Level <= Level).Sum(y => y.RewardAbilityPoint);
            GameDataManager.Instance.User.ResetLinkUserCharacter();
        }

        public bool GetBattleCheck() => !Deck.ContainsValue(null);

        public UserCharacterBody GetServerData()
        {
            var body = new UserCharacterBody();
            body.Index = CharacterIndex;
            body.Deck = new int[8];

            int index = 0;
            foreach (var spell in Deck)
                body.Deck[index++] = spell.Value == null ? 0 : spell.Value.Index;

            return body;
        }

        public int[] GetDeckArray()
        {
            var array = new int[8];

            int index = 0;
            foreach (var spell in Deck)
                array[index++] = spell.Value == null ? 0 : spell.Value.Index;

            return array;
        }

        public BattleInfoBody GetBattleInfo()
        {
            var body = new BattleInfoBody();
            body.CharacterIndex = CharacterIndex;
            body.Level = Level;
            body.Spell = new int[8];
            body.SpellLevel = new int[8];
            body.Ability = new int[5];

            int index = 0;
            foreach (var spell in Deck)
            {
                body.Spell[index] = spell.Value == null ? 0 : spell.Value.Index;
                body.SpellLevel[index] = spell.Value == null ? 1 : spell.Value.Level;
                index++;
            }
            for (int i = 0; i < CharacterAbilities.Count; i++)
            {
                body.Ability[i] = CharacterAbilities[i].Level;
            }

            return body;
        }

        public void LevelUp(UserCharacterBody body)
        {
            Level = body.Level;
            Exp = body.Exp;
            NextLevel = GameDataManager.Instance.GetCharacterExpertLevel(CharacterIndex, Level + 1);
            OwnedAbilityPoint = GameDataManager.Instance.CharacterExpertLevels[CharacterIndex].FindAll(x => x.Level <= Level).Sum(y => y.RewardAbilityPoint);
        }

        public void LevelUpAbility(int index, int level)
        {
            var ability = CharacterAbilities[index];
            ability.LevelUpAbility(level);
            if (ability.StatValue != null)
                State.SetStatDirty(ability.StatValue.AttrType);
        }
    }
}
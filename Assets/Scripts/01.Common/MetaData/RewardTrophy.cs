﻿using Secret.Server;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.Data
{
    public class RewardTrophy
    {
        public readonly int Index;
        public readonly int HighTrophy;
        public readonly RewardTrophyType RewardType;
        public readonly List<Spell> Spell;
        public readonly List<Character> Character;
        public readonly int Gold;

        public RewardTrophy(RewardTrophyBody body)
        {
            Index = body.Index;
            HighTrophy = body.HighTrophy;
            RewardType = (RewardTrophyType)Enum.Parse(typeof(RewardTrophyType), body.RewardType);
            switch (RewardType)
            {
                case RewardTrophyType.Spell:
                    Spell = new List<Spell>();
                    foreach (var spell in body.RewardParameter)
                        Spell.Add(GameDataManager.Instance.GetSpell(spell));
                    break;
                case RewardTrophyType.Character:
                    Character = new List<Character>();
                    foreach (var character in body.RewardParameter)
                        Character.Add(GameDataManager.Instance.GetCharacter(character));
                    break;
                case RewardTrophyType.Gold:
                    Gold = body.RewardParameter[0];
                    break;
            }
        }
    }

    public enum RewardTrophyType
    {
        Spell,
        Character,
        Gold
    }
}
﻿using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.Data
{
    public class CharacterExpertLevel
    {
        public readonly int Index;
        public readonly int Level;
        public readonly int RequiredExp;
        public readonly int RequiredGold;
        public readonly int RewardAbilityPoint;
        public readonly Character Character;
        public readonly Spell RewardSpell;

        public CharacterExpertLevel(CharacterExpertLevelBody body)
        {
            Index = body.Index;
            Level = body.Level;
            RequiredExp = body.RequiredExp;
            RequiredGold = body.RequiredGold;
            RewardAbilityPoint = body.RewardAbilityPoint;
            Character = GameDataManager.Instance.GetCharacter(body.CharacterIndex);
            RewardSpell = GameDataManager.Instance.GetSpell(body.RewardSpell);
        }
    }
}
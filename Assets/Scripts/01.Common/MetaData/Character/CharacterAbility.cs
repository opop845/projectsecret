﻿using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using I2.Loc;
using System;

namespace Secret.Data
{
    public class CharacterAbility
    {
        public readonly int Index;
        public readonly int CharacterIndex;
        public readonly int AbilityIndex;
        public readonly AbilityType AbilityType;
        public readonly AttrType AbilitySubType;
        public readonly float AbilityParameter;
        public readonly int RequiredAbilityPoint;
        public readonly int MaxAbilityLevel;
        public readonly LocalizedString Name;
        public readonly LocalizedString Desc;

        public CharacterAbility(CharacterAbilityBody body)
        {
            Index = body.Index;
            CharacterIndex = body.CharacterIndex;
            AbilityIndex = body.AbilityIndex;
            AbilityType = (AbilityType)Enum.Parse(typeof(AbilityType), body.AbilityType);
            AbilitySubType = (AttrType)Enum.Parse(typeof(AttrType), body.AbilitySubType);
            AbilityParameter = body.AbilityParameter;
            RequiredAbilityPoint = body.RequiredAbilityPoint;
            MaxAbilityLevel = body.MaxAbilityLevel;
            Name = $"Ability/ability_name_{CharacterIndex}_{AbilityIndex}";
            Desc = $"Ability/ability_desc_{CharacterIndex}_{AbilityIndex}";
        }
    }

    public enum AbilityType
    {
        Special,
        Normal,
    }
}
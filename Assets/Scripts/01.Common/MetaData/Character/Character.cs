﻿using Secret.Server;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using I2.Loc;

namespace Secret.Data
{
    public class Character
    {
		public readonly int Index;
		public readonly int AttackDamage;
		public readonly float AttackSpeed;
		public readonly int Hp;
		public readonly float MovementSpeed;
		public readonly float CriticalChance;
		public readonly float CriticalDamage;
		public readonly float HpRestorationRate;
		public readonly float AttackDamageGrowRate;
		public readonly float AttackSpeedGrowRate;
		public readonly float HpGrowRate;
		public readonly float MovementSpeedGrowRate;
		public readonly float CriticalChanceGrowRate;
		public readonly float CriticalDamageGrowRate;
		public readonly float HpRestorationRateGrowRate;
		public readonly int SortOrder;

		public readonly LocalizedString Name;
		public readonly LocalizedString Desc;
		public readonly CharacterStatTable State;

		public Character(CharacterBody body)
        {
			Index = body.Index;
			Name = $"Character/character_name_{Index}";
			Desc = $"Character/character_desc_{Index}";
			AttackDamage = body.AttackDamage;
			AttackSpeed = body.AttackSpeed;
			Hp = body.Hp;
			MovementSpeed = body.MovementSpeed;
			CriticalChance = body.CriticalChance;
			CriticalDamage = body.CriticalDamage;
			HpRestorationRate = body.HpRestorationRate;
			AttackDamageGrowRate = body.AttackDamageGrowRate;
			AttackSpeedGrowRate = body.AttackSpeedGrowRate;
			HpGrowRate = body.HpGrowRate;
			MovementSpeedGrowRate = body.MovementSpeedGrowRate;
			CriticalChanceGrowRate = body.CriticalChanceGrowRate;
			CriticalDamageGrowRate = body.CriticalDamageGrowRate;
			HpRestorationRateGrowRate = body.HpRestorationRateGrowRate;

			State = new CharacterStatTable();
			State.SetBaseValue(AttrType.Attack, AttackDamage);
			State.SetBaseValue(AttrType.AttackSpeed, AttackSpeed);
			State.SetBaseValue(AttrType.Hp, Hp);
			State.SetBaseValue(AttrType.MoveSpeed, MovementSpeed);
			State.SetBaseValue(AttrType.CriticalChance, CriticalChance);
			State.SetBaseValue(AttrType.CriticalDamage, CriticalDamage);
			State.SetBaseValue(AttrType.HpRestorationRate, HpRestorationRate);
			State.SetBaseValue(AttrType.SuckHp, 0f);
			State.SetBaseValue(AttrType.Soul, 0f);
			SortOrder = body.SortOrder;
		}
	}
}
﻿using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.Data
{
    public class Map
    {
        public readonly int Index;
        public readonly string Type;
        public readonly int Parameter1;
        public readonly int Parameter2;

        public Map(MapBody body)
        {
            Index = body.Index;
            Type = body.Type;
            Parameter1 = body.Parameter1;
            Parameter2 = body.Parameter2;
        }
    }
}
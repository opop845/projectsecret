﻿using Secret.Server;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.Data
{
    public class Spell
    {
        public readonly int Index;
        public readonly SpellType SpellType;
        public readonly Item Item;
        public readonly Skill Skill;
        public readonly int SpellSubIndex;
        public readonly int Soul;
        public readonly int DrawWeight;
        public readonly int CharacterIndex;

        public string Name
        {
            get
            {
                if (Item != null) return Item.Name;
                else if (Skill != null) return Skill.Name;
                else return string.Empty;
            }
        }

        public string Desc
        {
            get
            {
                if (Item != null) return Item.Desc;
                else if (Skill != null) return Skill.Desc;
                else return string.Empty;
            }
        }

        public Sprite IconSprite
        {
            get
            {
                if (Item != null) return Item.IconSprite;
                else if (Skill != null) return Skill.IconSprite;
                else return null;
            }
        }

        public Spell(SpellBody body)
        {
            Index = body.Index;
            SpellType = (SpellType)Enum.Parse(typeof(SpellType), body.SpellType);

            switch (SpellType)
            {
                case SpellType.Item:
                    Item = GameDataManager.Instance.GetItem(body.SpellSubIndex);
                    break;
                case SpellType.Skill:
                    Skill = GameDataManager.Instance.GetSkill(body.SpellSubIndex);
                    break;
            }

            SpellSubIndex = body.SpellSubIndex;
            Soul = body.SoulPrice;
            DrawWeight = body.DrawWeight;
            CharacterIndex = body.CharacterIndex;
        }
    }

    public enum SpellType
    {
        Item,
        Skill,
    }
}
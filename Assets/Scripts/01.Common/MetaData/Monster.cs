﻿using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.Data
{
    public class Monster
    {
        public readonly int Index;
        public readonly int AttackDamage;
        public readonly float AttackSpeed;
        public readonly int Hp;
        public readonly float MovementSpeed;
        public readonly float CriticalChance;
        public readonly float CriticalDamage;
        public readonly float HpRestorationRate;
        public readonly int ClearRewardExp;             
        public readonly int ClearRewardSoul;            

        public CharacterStatTable State;

        public Monster(MonsterBody body)
        {
            Index = body.Index;
            AttackDamage = body.AttackDamage;
            AttackSpeed = body.AttackSpeed;
            Hp = body.Hp;
            MovementSpeed = body.MovementSpeed;
            CriticalChance = body.CriticalChance;
            CriticalDamage = body.CriticalDamage;
            HpRestorationRate = body.HpRestorationRate;

            State = new CharacterStatTable();
            State.SetBaseValue(AttrType.Attack, AttackDamage);
            State.SetBaseValue(AttrType.AttackSpeed, AttackSpeed);
            State.SetBaseValue(AttrType.Hp, Hp);
            State.SetBaseValue(AttrType.MoveSpeed, MovementSpeed);
            State.SetBaseValue(AttrType.CriticalChance, CriticalChance);
            State.SetBaseValue(AttrType.CriticalDamage, CriticalDamage);
            State.SetBaseValue(AttrType.HpRestorationRate, HpRestorationRate);
            State.SetBaseValue(AttrType.SuckHp, 0f);
            State.SetBaseValue(AttrType.Soul, 0f);

            ClearRewardExp = body.ClearRewardExp;
            ClearRewardSoul = body.ClearRewardSoul;
        }
    }
}
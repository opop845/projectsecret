﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Secret.Server;
using System;
using I2.Loc;

namespace Secret.Data
{
    public class EnemyUser
    {
        public readonly EnemyType EnemyType;
        public readonly int BaseScore;
        public readonly int EnemyIndex;
        public readonly Character Character;
        public readonly string EnemyName;
        public readonly int Level;
        public readonly List<UserSpell> Spell;
        public readonly List<UserCharacterAbility> Abilities;
        public readonly bool IsLose;


        public EnemyUser(EnemyUserBody body)
        {
            IsLose = body.IsLose;
            EnemyType = (EnemyType)Enum.Parse(typeof(EnemyType), body.EnemyType);
            EnemyIndex = body.EnemyIndex;
            BaseScore = body.BaseScore;
            Character = GameDataManager.Instance.GetCharacter(body.CharacterIndex);
            if (EnemyType == EnemyType.AI)
                EnemyName = LocalizationManager.GetTranslation($"AIUser/aiuser_name_{EnemyIndex}");
            else
                EnemyName = body.EnemyName;
            Level = body.Level;
            Spell = new List<UserSpell>();
            for (int i = 0; i < body.Spell.Length; i++)
            {
                Spell.Add(new UserSpell(body.Spell[i], body.SpellLevel[i]));
            }
            Abilities = new List<UserCharacterAbility>();
            for (int i = 0; i < 4; i++)
                Abilities.Add(new UserCharacterAbility(Character.Index, i + 1, body.Ability[i]));
        }
    }

    public enum EnemyType
    {
        AI,
        User
    }
}
﻿using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.Data
{
    public class EquipmentItem : Item
    {
        public EquipmentItem(ItemBody body)
            : base(body)
        { }

        public void SetEquipItem(DefaultObject obj, BattleItem battleItem)
        {
            for (int i = 0; i < SubType.Length; i++)
            {
                switch (SubType[i])
                {
                    case ItemSubType.AttackDamage:
                        obj.Stat.AddStat(AttrType.Attack, ValueType, Parameter1[i], battleItem);
                        break;
                    case ItemSubType.AttackSpeed:
                        obj.Stat.AddStat(AttrType.AttackSpeed, ValueType, Parameter1[i], battleItem);
                        break;
                    case ItemSubType.MovementSpeed:
                        obj.Stat.AddStat(AttrType.MoveSpeed, ValueType, Parameter1[i], battleItem);
                        break;
                    case ItemSubType.CriticalChance:
                        obj.Stat.AddStat(AttrType.CriticalChance, ValueType, Parameter1[i], battleItem);
                        break;
                    case ItemSubType.CriticalDamage:
                        obj.Stat.AddStat(AttrType.CriticalDamage, ValueType, Parameter1[i], battleItem);
                        break;
                    case ItemSubType.HpRestorationrate:
                        obj.Stat.AddStat(AttrType.HpRestorationRate, ValueType, Parameter1[i], battleItem);
                        break;
                    case ItemSubType.Hp:
                        obj.Stat.AddStat(AttrType.Hp, ValueType, Parameter1[i], battleItem);
                        break;
                    case ItemSubType.Soul:
                        obj.Stat.AddStat(AttrType.Soul, ValueType, Parameter1[i], battleItem);
                        break;
                }
            }
            BattleEventManager.Instance.OnEventStatUpdate(obj, obj.Stat.BattlePower);
        }
        public void SetUnEquipItem(DefaultObject obj, BattleItem battleItem)
        {
            obj.Stat.RemoveStat(battleItem);
            obj.Hp = obj.Hp;
            BattleEventManager.Instance.OnEventStatUpdate(obj, obj.Stat.BattlePower);
        }
    }
}
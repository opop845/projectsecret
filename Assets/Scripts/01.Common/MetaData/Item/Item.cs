﻿using I2.Loc;
using Secret.Server;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Secret.Data
{
    public class Item
    {
        public readonly int Index;
        public readonly ItemType ItemType;
        public readonly ItemSubType[] SubType;
        public readonly StatModType ValueType;
        public readonly float[] Parameter1;
        public readonly float[] Parameter2;
        public readonly float DurationTime;
        public readonly bool IsUsable;
        public readonly int InventoryMaxStackCount;
        public readonly Sprite IconSprite;
        public readonly bool IsMergeable;
        public readonly int[] MergeMaterials;
        public LocalizedString Name;
        private LocalizedString _desc;
        private object[] descValue;
        public string Desc
        {
            get
            {
                string value = _desc.ToString().SetParameter("time", DurationTime.ToString());
                return string.Format(value,descValue);
        
            }
        }

        public Item(ItemBody body)
        {
            Index = body.Index;
            Name = $"Item/item_name_{Index}";
            _desc = $"Item/item_desc_{Index}";
            ItemType = (ItemType)Enum.Parse(typeof(ItemType), body.Type);
            ValueType = (StatModType)Enum.Parse(typeof(StatModType), body.ValueType);
            SubType = new ItemSubType[body.SubType.Length];
            for (int i = 0; i < body.SubType.Length; i++)
                SubType[i] = (ItemSubType)Enum.Parse(typeof(ItemSubType), body.SubType[i]);

            Parameter1 = body.Parameter1;
            Parameter2 = body.Parameter2;
            List<object> temp = new List<object>();
            if (Parameter1 != null)
            {
                foreach (var value in Parameter1)
                    temp.Add(value);
            }
            if (Parameter2 != null)
            {
                foreach (var value in Parameter2)
                    temp.Add(value);
            }
            descValue = temp.ToArray();
            DurationTime = body.DurationTime;
            IsUsable = body.IsUsable;
            IsMergeable = body.IsMergeable;

            MergeMaterials = body.MergeMaterials;
            InventoryMaxStackCount = body.InventoryMaxStackCount;
            IconSprite = ResourceManager.Instance.LoadSprtie($"item_{Index}");
        }

        public bool CheckMerge(int index1, int index2)
        {
            if (MergeMaterials == null) return false;

            for (int i = 0; i < MergeMaterials.Length; i++)
            {
                if (MergeMaterials[i] == index1)
                    index1 = 0;
                else if (MergeMaterials[i] == index2)
                    index2 = 0;
            }
            if (index1 == 0 && index2 == 0) return true;
            else return false;
        }

        public virtual bool UseItem(DefaultObject obj) { return true; }
        public virtual void SellItem(DefaultObject obj) { }
    }

    public enum ItemType
    {
        Get,
        Buff,
        Equipment,
        Damage
    }

    public enum ItemSubType
    {
        Hp,
        Exp,
        SkillPoint,
        AttackDamage,
        AttackSpeed,
        MovementSpeed,
        CriticalChance,
        CriticalDamage,
        HpRestorationrate,
        ClearRewardExp,
        ClearRewardGold,
        ClearRewardBonusGold,
        Gold,
        Monster,
        Soul,
    }
}

﻿using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.Data
{
    public class DamageItem : Item
    {
        public DamageItem(ItemBody body)
          : base(body)
        {

        }
        public override bool UseItem(DefaultObject obj)
        {
            base.UseItem(obj);
            if (obj.ObjectType != ObjectType.Player) return false;
            CharacterObject player = (CharacterObject)obj;
            for (int i = 0; i < SubType.Length; i++)
            {
                switch (SubType[i])
                {
                    case ItemSubType.Monster:
                        if (player.target != null && player.target.ObjectType == ObjectType.Tower)
                            player.target.SetDamage(player, (int)Parameter1[i], false);
                        else
                            return false;
                        break;
                }
            }
            BattleEventManager.Instance.OnEventStatUpdate(obj,obj.Stat.BattlePower);
            return true;
        }
    }
}
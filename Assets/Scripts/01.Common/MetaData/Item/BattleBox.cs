﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Secret.Server;

namespace Secret.Data
{
    public class BattleBox
    {
        public int Index;
        public int Trophy;
        public Spell[] Spells;

        public BattleBox(BattleBoxBody body)
        {
            Index = body.Index;
            Trophy = body.Trophy;
            Spells = new Spell[body.Spell.Length];
            for (int i = 0; i < body.Spell.Length; i++)
            {
                Spells[i] = GameDataManager.Instance.GetSpell(body.Spell[i]);
            }
        }
    }
}
﻿using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.Data
{
    public class GetItem : Item
    {
        public GetItem(ItemBody body)
            : base(body)
        {

        }
        public override bool UseItem(DefaultObject obj)
        {
            base.UseItem(obj);
            CharacterObject player = (CharacterObject)obj;
            for (int i = 0; i < SubType.Length; i++)
            {
                switch (SubType[i])
                {
                    case ItemSubType.Hp:
                        player.SetHeal((int)Parameter1[i]);
                        break;
                    case ItemSubType.SkillPoint:
                        player.SkillPoint += (int)Parameter1[i];
                        break;
                    case ItemSubType.Gold:
                        player.Soul += (int)(Parameter1[i] * Random.Range(1f - Parameter2[i], 1f + Parameter2[i]));
                        break;
                }
            }
            BattleEventManager.Instance.OnEventStatUpdate(obj,obj.Stat.BattlePower);
            return true;
        }
    }
}
﻿using Secret.Data;
using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.Data
{
    public class BuffItem : Item
    {
        public BuffItem(ItemBody body)
            : base(body)
        {
        }

        public override bool UseItem(DefaultObject obj)
        {
            base.UseItem(obj);

            for (int i = 0; i < SubType.Length; i++)
            {
                switch (SubType[i])
                {
                    case ItemSubType.AttackDamage:
                        obj.AddBuff(AttrType.Attack, ValueType, Parameter1[i], Parameter2[i], IconSprite);
                        break;
                    case ItemSubType.AttackSpeed:
                        obj.AddBuff(AttrType.AttackSpeed, ValueType, Parameter1[i], Parameter2[i], IconSprite);
                        break;
                    case ItemSubType.MovementSpeed:
                        obj.AddBuff(AttrType.MoveSpeed, ValueType, Parameter1[i], Parameter2[i], IconSprite);
                        break;
                    case ItemSubType.CriticalChance:
                        obj.AddBuff(AttrType.CriticalChance, ValueType, Parameter1[i], Parameter2[i], IconSprite);
                        break;
                    case ItemSubType.CriticalDamage:
                        obj.AddBuff(AttrType.CriticalDamage, ValueType, Parameter1[i], Parameter2[i], IconSprite);
                        break;
                    case ItemSubType.HpRestorationrate:
                        obj.AddBuff(AttrType.HpRestorationRate, ValueType, Parameter1[i], Parameter2[i], IconSprite);
                        break;
                }
            }
            BattleEventManager.Instance.OnEventStatUpdate(obj,obj.Stat.BattlePower);
            return true;
        }
    }
}
﻿using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.Data
{
    public class SpellLevel
    {
        public int Index;
        public int RequiredSpellCardAmount;
        public int RequiredGold;

        public SpellLevel(SpellLevelBody body)
        {
            Index = body.Index;
            RequiredSpellCardAmount = body.RequiredSpellCardAmount;
            RequiredGold = body.RequiredGold;
        }
    }
}
﻿using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Secret.Data
{
    public class HealSkill : Skill
    {
        public HealSkill(SkillBody body) : base(body)
        {
        }

        public override bool UseSkill(DefaultObject caster)
        {
            if (!base.UseSkill(caster)) return false;
            if (TargetType == TargetType.User)
            {
                for (int i = 0; i < SubType.Length; i++)
                {
                    if (SubType[i] == SkillSubType.Dot)
                    {
                        caster.StartCoroutine(HealDot(Parameter1[i], (int)DurationTime));
                        caster.AddBuff(AttrType.Hp, ValueType, 0f, DurationTime, IconSprite);
                    }
                }
            }

            return true;
            IEnumerator HealDot(float value, int count)
            {
                yield return new WaitForSeconds(0.5f);
                for (int i = 0; i < count; i++)
                {
                    caster.SetHeal(value);
                    yield return new WaitForSeconds(1f);
                }
            }
        }
    }
}
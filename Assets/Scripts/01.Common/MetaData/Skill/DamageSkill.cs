﻿using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using BehaviorDesigner.Runtime.Tasks.Unity.UnityGameObject;
using PlayFab.GroupsModels;

namespace Secret.Data
{
    public class DamageSkill : Skill
    {
        public DamageSkill(SkillBody body) : base(body)
        {
        }

        public override bool UseSkill(DefaultObject caster)
        {
            if (!base.UseSkill(caster)) return false;
            if (caster.target == null) return false;
            if (TargetType == TargetType.All)
            {
                if (SubType[0] == SkillSubType.Hit)
                    caster.StartCoroutine(SetDamage(Parameter1[0]));
                else if (SubType[0] == SkillSubType.MultiHit)
                    caster.StartCoroutine(SetMultiDamage(Parameter1[0], (int)Parameter2[0], Parameter3[0]));
            }

            return true;
            IEnumerator SetDamage(float value)
            {
                //yield return new WaitForSeconds(CastTime);
                if (caster.target != null)
                {
                    var effectObj = ResourceManager.Instance.LoadAsset<GameObject>($"Effects/{TargetEffect}");
                    if (effectObj != null)
                    {
                        var effectPos = new Vector3(caster.target.HudTarget.position.x, caster.target.transform.position.y, caster.target.HudTarget.position.z);
                        var effect = UnityEngine.Object.Instantiate(effectObj, effectPos, Quaternion.identity);
                        effect.transform.Rotate(new Vector3(90.0f, 0.0f, 0.0f)); // Sanghun, 각도 설정할줄 몰라서 강제로 박음
                    }

                    yield return new WaitForSeconds(CastTime); // Sanghun, 이펙트는 딜레이 없이 바로 나가도록
                    CinemachineShake.Instance.ShakeCamera(1.0f, 0.1f); // Sanghun

                    if (caster.target != null) caster.target.SetDamage(caster, (int)value, false); // 딜레이 후에도 타겟 체크

                }
            }

            IEnumerator SetMultiDamage(float value, int hitCount, float time)
            {
                int hit = 0;
                caster.StopCoroutine("BattleStart");
                if (AnimTrigger != string.Empty)
                    caster.Anim.SetTrigger(AnimTrigger);
                yield return new WaitForSeconds(CastTime);
                yield return new WaitForSeconds(0.1f * hitCount);
                while (hit < hitCount)
                {
                    hit++;
                    if (caster.target == null) break;
                    var effectObj = ResourceManager.Instance.LoadAsset<GameObject>($"Effects/{TargetEffect}");
                    if (effectObj != null)
                    {
                        var effectPos = new Vector3(caster.target.HudTarget.position.x, caster.target.transform.position.y, caster.target.HudTarget.position.z);
                        var effect = UnityEngine.Object.Instantiate(effectObj, effectPos, Quaternion.identity);
                        effect.transform.Rotate(new Vector3(90.0f, 0.0f, 0.0f)); // Sanghun, 각도 설정할줄 몰라서 강제로 박음
                    }

                    CinemachineShake.Instance.ShakeCamera(1.0f, 0.1f); // Sanghun
                    var isCritical = UnityEngine.Random.value <= caster.Stat.GetStatValue(AttrType.CriticalChance);
                    var alphaStrikeMultiplier = 0.3f; // 스킬 계수, 각 스킬의 paramaeter로 추가되어야함
                    var damage = isCritical ? ((int)(caster.Stat.GetStatValue(AttrType.Attack)* alphaStrikeMultiplier + value ) * caster.Stat.GetStatValue(AttrType.CriticalDamage)) : ((int)(caster.Stat.GetStatValue(AttrType.Attack)* alphaStrikeMultiplier + value)) ;
                    caster.target.SetDamage(caster, (int)damage, isCritical); // 딜레이 후에도 타겟 체크
                    var suck = caster.Stat.GetStatValue(AttrType.SuckHp);
                    //if (suck > 0f)
                    //    caster.SetHeal(Mathf.CeilToInt(damage * suck));
                    yield return new WaitForSeconds(time - 0.1f);
                }

                if (caster.target != null)
                    caster.StartCoroutine("BattleStart");
                yield return null;
            }
        }
    }
}
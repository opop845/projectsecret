﻿using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Secret.Data
{
    public class BuffSkill : Skill
    {
        public BuffSkill(SkillBody body) : base(body)
        {
        }

        public override bool UseSkill(DefaultObject caster)
        {
            if (!base.UseSkill(caster)) return false;
            if (TargetType == TargetType.User)
            {
                for (int i = 0; i < SubType.Length; i++)
                {
                    if (Enum.TryParse<AttrType>(SubType[i].ToString(), out var type))
                    {
                        caster.AddBuff(type, ValueType, Parameter1[i], DurationTime, IconSprite);
                    }
                }
            }
            return true;
        }
    }
}
﻿using I2.Loc;
using Secret.Server;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.Data
{
    public class Skill
    {
        public readonly int Index;
        public readonly SkillType SkillType;
        public readonly SkillSubType[] SubType;
        public readonly TargetType TargetType;
        public readonly StatModType ValueType;
        public readonly float CastTime;
        public readonly float DurationTime;
        public readonly float[] Parameter1;
        public readonly float[] Parameter2;
        public readonly float[] Parameter3;
        public readonly string AnimTrigger;
        public readonly string CasterEffect;
        public readonly string TargetEffect;
        public readonly Sprite IconSprite;
        public LocalizedString Name;
        private LocalizedString _desc;
        private object[] descValue;

        public string Desc
        {
            get
            {
                string value = _desc.ToString().Replace("{time}", DurationTime.ToString());
                if (descValue.Length > 0)
                    return string.Format(value, descValue);
                else
                    return value;
            }
        }

        public Skill(SkillBody body)
        {
            Index = body.Index;
            Name = $"Skill/skill_name_{Index}";
            _desc = $"Skill/skill_desc_{Index}";
            SkillType = (SkillType)Enum.Parse(typeof(SkillType), body.Type);
            SubType = new SkillSubType[body.SubType.Length];
            for (int i = 0; i < body.SubType.Length; i++)
                SubType[i] = (SkillSubType)Enum.Parse(typeof(SkillSubType), body.SubType[i]);
            TargetType = (TargetType)Enum.Parse(typeof(TargetType), body.Target);
            ValueType = (StatModType)Enum.Parse(typeof(StatModType), body.ValueType);
            CastTime = body.CastTime;
            DurationTime = body.DurationTime;
            Parameter1 = body.Parameter1;
            Parameter2 = body.Parameter2;
            Parameter3 = body.Parameter3;
            List<object> temp = new List<object>();
            if (Parameter1 != null)
            {
                foreach (var value in Parameter1)
                    temp.Add(value);
            }
            if (Parameter2 != null)
            {
                foreach (var value in Parameter2)
                    temp.Add(value);
            }
            if (Parameter3 != null)
            {
                foreach (var value in Parameter3)
                    temp.Add(value);
            }
            descValue = temp.ToArray();
            AnimTrigger = body.AnimTrigger;
            CasterEffect = body.CasterEffect;
            TargetEffect = body.TargetEffect;
            IconSprite = ResourceManager.Instance.LoadSprtie($"skill_{Index}");
        }

        public virtual bool UseSkill(DefaultObject caster)
        {
            if (caster.IsDead) return false;
            else if (TargetType == TargetType.All && caster.target == null) return false;
            return true;
        }
    }

    public enum TargetType
    {
        All,
        Enemy,
        User,
    }

    public enum SkillType
    {
        Damage,
        Debuff,
        Buff,
        Heal,
        Special
    }

    public enum SkillSubType
    {
        Dot,
        SuckHp,
        Hit,
        MultiHit,
        Recall
    }
}
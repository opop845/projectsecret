﻿using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using BehaviorDesigner.Runtime.Tasks.Unity.UnityGameObject;
using PlayFab.GroupsModels;

namespace Secret.Data
{
    public class SpecialSkill : Skill
    {
        public SpecialSkill(SkillBody body) : base(body)
        {
        }

        public override bool UseSkill(DefaultObject caster)
        {
            if (!base.UseSkill(caster)) return false;
            if (TargetType == TargetType.User)
            {
                if (SubType[0] == SkillSubType.Recall)
                    caster.StartCoroutine(Recall());               
            }
            return true;

            IEnumerator Recall()
            {
                if (caster != null)
                {
                    caster.GetComponent<CharacterObject>().SetRecall(caster, CastTime); 

                    var effectObj = ResourceManager.Instance.LoadAsset<GameObject>($"Effects/{TargetEffect}");
                    if (effectObj != null)
                    {
                        var effectPos = new Vector3(caster.transform.position.x, caster.transform.position.y, caster.transform.position.z);
                        var effect = UnityEngine.Object.Instantiate(effectObj, effectPos, Quaternion.identity);
                        effect.transform.Rotate(new Vector3(90.0f, 0.0f, 0.0f)); // Sanghun, 각도 설정할줄 몰라서 강제로 박음
                    }
                    yield return new WaitForSeconds(0.0f);                    
                }
            }
        }
    }    


}
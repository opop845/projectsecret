﻿using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret
{
    public class BattleItem
    {
        private CharacterObject characterObject;
        public Item Item { get; private set; }
        private float priceSoul;
        public int Level;
        private int _count;
        public int Count
        {
            get => _count;
            set
            {
                _count = value;
                if (_count <= 0)
                    Item = null;
                GameEventManager.Instance.OnEventBattleItemUpdate();
            }
        }

        public bool IsFull
        {
            get
            {
                if (Item != null)
                    return Item.InventoryMaxStackCount <= Count;
                else
                    return false;
            }
        }

        public float SoulPrice
        {
            get
            {
                if (Item != null)
                    return priceSoul * Count * GameDataManager.Instance.GameRule.ItemSellGoldPriceRate;
                else
                    return 0;
            }
        }

        public BattleItem(CharacterObject obj)
        {
            characterObject = obj;
            _count = 0;
            Level = 0;
            priceSoul = 0f;
            Item = null;
        }

        public void PurchaseItem(Item item)
        {
            if (Item == null)
            {
                Item = item;
                if (item.ItemType == ItemType.Equipment)
                {
                    var equip = (EquipmentItem)item;
                    equip.SetEquipItem(characterObject, this);
                }
            }
            var spell = GameDataManager.Instance.GetSpell(SpellType.Item, item.Index);
            if (spell != null)
                priceSoul = spell.Soul;
            Count++;
            GameEventManager.Instance.OnEventItemPurchase(characterObject, item);
        }

        public void MergeItem(Item item)
        {
            var level = Level;
            RemoveItem();

            if (Item == null)
            {
                Item = item;
                if (item.ItemType == ItemType.Equipment)
                {
                    var equip = (EquipmentItem)item;
                    equip.SetEquipItem(characterObject, this);
                }
            }
            priceSoul *= 2;
            Level = level + 1;
            Count++;
            GameManager.Instance.DisplayItemMergeEffect(characterObject); // Sanghun
            GameEventManager.Instance.OnEventItemMerge(characterObject, this);
        }

        public void RemoveItem()
        {
            if (Item == null) return;
            if (Item.ItemType == ItemType.Equipment)
            {
                var equip = (EquipmentItem)Item;
                equip.SetUnEquipItem(characterObject, this);
            }

            Level = 0;
            Count = 0;
        }

        public float SellItem()
        {
            if (Item == null) return 0;
            if (Item.ItemType == ItemType.Equipment)
            {
                var equip = (EquipmentItem)Item;
                equip.SetUnEquipItem(characterObject, this);
            }
            float soul = SoulPrice;
            Item = null;
            Level = 0;
            Count = 0;
            priceSoul = 0f;
            return soul;
        }

        public void UseBattleItem()
        {
            if (Item == null) return;
            if (Item.ItemType != ItemType.Equipment)
            {
                if (characterObject.IsDead) return;
                if (Item.UseItem(characterObject))
                {
                    GameEventManager.Instance.OnEventItemUse(characterObject, Item);
                    Count--;
                }
            }
        }
    }
}
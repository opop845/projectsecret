﻿using Secret;
using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Secret
{
    public class BattleInventory
    {
        public BattleItem[] battleItems = new BattleItem[6];
        private CharacterObject characterObject;

        public BattleInventory(CharacterObject character)
        {
            characterObject = character;

            for (int i = 0; i < 6; i++)
                battleItems[i] = new BattleItem(characterObject);
        }

        public bool PurchaseItem(Item item)
        {
            //가지고 있는 아이템인지 체크
            for (int i = 0; i < battleItems.Length; i++)
            {
                if (battleItems[i].Item == item && !battleItems[i].IsFull)
                {
                    battleItems[i].PurchaseItem(item);
                    return true;
                }
            }

            //새로운 아이템 추가
            for (int i = 0; i < battleItems.Length; i++)
            {
                if (battleItems[i].Item == null)
                {
                    battleItems[i].PurchaseItem(item);
                    return true;
                }
            }

            return false;
        }

        public void UseBattleItem(int index)
        {
            battleItems[index].UseBattleItem();
        }

        public float SellBattleItem(int index)
        {
            return battleItems[index].SellItem();
        }

        public void SwitchBattleItem(int originIndex,int moveIndex)
        {
            if (originIndex == moveIndex) return;
            var mergeItem = GameDataManager.Instance.GetMergeItem(battleItems[originIndex].Item, battleItems[moveIndex].Item);
            if (mergeItem != null)
            {
                battleItems[moveIndex].RemoveItem();
                battleItems[originIndex].MergeItem(mergeItem);
            }
            else
            {
                battleItems.Swap(originIndex, moveIndex);
                GameEventManager.Instance.OnEventBattleItemUpdate();
            }
        }
        
        public void MergeBattleItem(int originIndex, int moveIndex)
        {
            if (originIndex == moveIndex) return;
            var mergeItem = GameDataManager.Instance.GetMergeItem(battleItems[originIndex].Item, battleItems[moveIndex].Item);
            if (mergeItem != null)
            {
                battleItems[moveIndex].RemoveItem();
                battleItems[originIndex].MergeItem(mergeItem);
            }
        }

        public BattleItem GetBattleItem(ItemType itemType, ItemSubType subType)
        {
            foreach (var battle in battleItems)
            {
                if (battle.Item != null && battle.Item.ItemType == itemType && battle.Item.SubType.Contains(subType))
                    return battle;
            }

            return null;
        }

        public BattleItem GetBattleItem(ItemType itemType)
        {
            foreach (var battle in battleItems)
            {
                if (battle.Item != null && battle.Item.ItemType == itemType)
                    return battle;
            }

            return null;
        }

        public int GetItemCount(int index) => GetItemCount(GameDataManager.Instance.GetItem(index));

        public int GetItemCount(Item item)
        {
            int count = 0;

            foreach (var battle in battleItems)
            {
                if(battle.Item == item)
                {
                    count += battle.Count;
                }
            }

            return count;
        }
    }
}
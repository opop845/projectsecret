﻿using Secret.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret
{
    public class BuffController
    {
        public event Action EventBuffChanged;
        public event Action EventBuffUpdate;
        public event Action EventBuffEnd;
        
        public AttrType attrType { get; private set; }
        public DefaultObject defaultObject { get; private set; }

        private float _duractionTime;
        public Sprite sprite;
        public float DurationTime
        {
            get => _duractionTime;
            set
            {
                _duractionTime = value;
                if (_duractionTime > 0f)
                    EventBuffUpdate?.Invoke();
                else
                {
                    EventBuffEnd?.Invoke();
                    defaultObject.Stat.RemoveStat(this);
                }
            }
        }

        public BuffController(DefaultObject obj, AttrType type, StatModType statType, float value, float duractionTime,Sprite sprite)
        {
            this.sprite = sprite;
            defaultObject = obj;
            attrType = type;
            obj.Stat.AddStat(type, statType, value, this);
            DurationTime = duractionTime;
        }

        public void UpdateBuff(float value, float duractionTime)
        {
            defaultObject.Stat.RemoveStat(this);
            defaultObject.Stat.AddStat(attrType, StatModType.PercentMult, value, this);
            _duractionTime += duractionTime;
            EventBuffChanged?.Invoke();
        }
    }
}
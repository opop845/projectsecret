﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret
{
    public class TimeManager : MonoSingleton<TimeManager>
    {
        private DateTime serverTime;
        private float startsinceTime = 0f;

        public DateTime ServerTime
        {
            get { return serverTime.AddSeconds(Time.realtimeSinceStartup - startsinceTime); }
            set
            {
                serverTime = value;
                startsinceTime = Time.realtimeSinceStartup;
            }
        }

        public DateTime OfflineTime { get; private set; }

        private WaitForSeconds wait;


        void Awake()
        {
            if (Instance != this)
                Destroy(gameObject);
            else
            {
                wait = new WaitForSeconds(30f);
                DontDestroyOnLoad(gameObject);
            }
        }

        public void Initialize(DateTime serverTime)
        {
            ServerTime = serverTime;
            StartCoroutine("UpdaterServerData");
        }

        IEnumerator UpdaterServerData()
        {
            while (true)
            {
                yield return wait;
                //ServerManager.Instance.UpdatePlayerData();
            }
        }

        private void OnApplicationQuit()
        {
            //ServerManager.Instance.UpdatePlayerData();
        }

        private void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                StopCoroutine("UpdaterServerData");
                //ServerManager.Instance.UpdatePlayerData();
            }
            else
            {
                StartCoroutine("UpdaterServerData");
            }
        }
    }
}
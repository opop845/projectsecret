﻿using Cinemachine;
using Guirao.UltimateTextDamage;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
//using System.Diagnostics;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Secret.Data;
using DG.Tweening;
using BehaviorDesigner.Runtime.Tasks.Unity.UnityGameObject;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI.Extensions.Tweens;
using System.Threading;
using Coffee.UIEffects;

//using UnityEngine.XR.WSA.Input;

namespace Secret
{
    public class GameManager : MonoSingleton<GameManager>
    {
        public TempMiniMap minimap;
        public Image ImageCopy;
        public int MoveIndex = -2;
        public int InvenIndex = -2;
        public int PlayerIndex = 0;
        public int EnemyIndex = 0;
        public Camera UICamera;
        public Canvas Canvas;
        public GameState GameState;
        public GameObject BattleStart;
        public GameObject BattleUser;
        public GameObject BattleOverWin;
        public GameObject BattleOverLose;
        public GameObject PlayInfo_B; // Sanghun, Enemy 전투 진행 정보
        public GameObject PopupResult;  // 결과창 추가 해둠 (Sanghun)     
        public Transform Character;
        public Transform Map;
        public Transform Tower;
        public Transform Box;
        public CinemachineVirtualCamera BattleCam1_Initiate;
        public CinemachineVirtualCamera BattleCam2_LookPlayer;
        public CinemachineVirtualCamera BattleCam3_Run;
        public CinemachineVirtualCamera BattleCam4_Battle;
        public CinemachineVirtualCamera BattleCam5_DogFight;
        public CinemachineVirtualCamera BattleCam6_ResultWin;
        public CinemachineVirtualCamera BattleCam7_ResultLose;
        public CinemachineVirtualCamera BattleCam8_LookEnemy;
        public CinemachineVirtualCamera BattleCam9_EnemyBattle;
        public CinemachineStateDrivenCamera MainCamera;
        public UltimateTextDamageManager textManager;

        [Header("[UI]")]
        //public TextMeshProUGUI TextPlayer;
       // public TextMeshProUGUI TextEnemy;
        public TextMeshProUGUI TextKillScore;
        public TextMeshProUGUI TextGold;
        public TextMeshProUGUI TextSoulGainSpeed; // Sanghun
        public float SoulGainSpeed;
        public Slider SliderSoul;
        public Slider SliderSoul_Int;
        public Image SliderFillImage;
        public TextMeshProUGUI TextSkillPoint;
        public TextMeshProUGUI TextTime;
        public TextMeshProUGUI TextLeftDistance; //Sanghun
        public GameObject LeftDistance; //Sanghun
        public TextMeshProUGUI TextPlayerTotalPower, TextEnemyTotalPower; //Sanghun
        public GameObject PlayerTotalPowerText, EnemyTotalPowerText; //Sanghun
        //public GameObject DeltaTotalPower; //Sanghun
        public GameObject PlayerExpGet;
        public BattleItemSell BattleItemSellPopup;

        public BattleCharacterInfo playerInfo;
        public BattleCharacterInfo enemyInfo;
        public GameObject bothInfo;   // Sanghun    

        public GameObject PlayerComment;
        public GameObject EnemyComment;
        public GameObject TempMiniMap;

        public UIEffect RefreshGrayEffect;


        public CharacterObject PlayerObject { get; private set; }
        public CharacterObject EnemyObject { get; private set; }

        private TimeSpan GameTime = TimeSpan.Zero;

        public float TempDistanceFactor = 8f;

        public PrefabHpbar prefabHpbar;
        public Transform HpbarParent;
        
        public PrefabHpbar PlayerHpBar_T, EnemyHpBar_T, MonsterHpBar_T, TowerHpBar_T; // Sanghun
        
        public PrefabHpbar PlayerHpbar { get; private set; }
        public PrefabHpbar EnemyHpbar { get; private set; }
        public PrefabHpbar MonsterHpbar { get; private set; }
        public PrefabHpbar TowerHpbar { get; private set; }

        public GameObject NotEnoughGold; // Sanghun

        public GameObject EnemyWarning; // Sanghun
        public GameObject HitEffect;
        public GameObject TowerHitEffect;
        public GameObject DeadEffect;
        public GameObject ItemMergeEffect;
        public GameObject LevelUpEffect;
        public GameObject HealEffect;
        public GameObject RunEffect;
        public GameObject GetBuffItemEffect;
        public GameObject TowerAttackedNoti;
        public GameObject Panel; // Sanghun
        public GameObject TopUI; // Sanghun

        public GameObject MonsterTypeIcon_1, MonsterTypeIcon_2, MonsterTypeIcon_3;

        public GameObject TimeNoti; // Sanghun   
        public GameObject RespawnNoti; // Sanghun   
        public GameObject MapBuffSpawnNoti; // Sanghun   

        public GameObject TargetChangeUI;
        public GameObject EnemyTargetingButton;
        public GameObject TowerTargetingButton;
        public GameObject NullTargetingButton;


        public string EnemyInfoTextString; // Sanghun

        BattleCamController battleCamController; // Sanghun

        public bool isLookPlayer = false;
        bool isTowerFight = false;
        bool isPlayerTower = false;
        GameObject TowerTarget;

        public float MapDistance;
        public float PlayerStartPos;
        public float EnemyStartPos;
        public float time = 0f;


        #region Config

        public float KillHeal { get; private set; }

        #endregion

        private void Start()
        {
            KillHeal = 0.25f;
            Panel.SetActive(false);
            TopUI.SetActive(false);
            TargetChangeUI.SetActive(false);


            // Sanghun
            battleCamController = GameObject.Find("BattleCamController").GetComponent<BattleCamController>();

            if (!GameDataManager.Instance.IsInit)
            {
                GameDataManager.Instance.InitLocalData();
                GameDataManager.Instance.SetBattleAI();
            }
            if (GameDataManager.Instance.BattleType == BattleType.AI)
                GameStart();

            // Sanghun
            CalculateTotalPower(PlayerObject);
            //CalculateTotalPower(EnemyObject);
        }


        float PlayerTotalPower, EnemyTotalPower;
        float PlayerOriginalTotalPower = 0, EnemyOriginalTotalPower = 0;
        float PlayerDeltaTotalPower, EnemyDeltaTotalPower;

        // Sanghun
        public void CalculateTotalPower(DefaultObject Obj)
        {
            return;
            if (Obj.ObjectType == ObjectType.Player)
            {
                PlayerTotalPower = Obj.Stat.BattlePower;
                if (PlayerTotalPower != PlayerOriginalTotalPower)
                {
                    PlayerDeltaTotalPower = PlayerTotalPower - PlayerOriginalTotalPower;
                    StartCoroutine(ShowTotalPowerUp(Obj, PlayerOriginalTotalPower, PlayerTotalPower));
                    if (PlayerTotalPower > PlayerOriginalTotalPower) DisplayGetPower(Obj, (int)PlayerDeltaTotalPower);
                    PlayerOriginalTotalPower = PlayerTotalPower;
                }
            }
            else
            {
                EnemyTotalPower = Obj.Stat.BattlePower;
                if (EnemyTotalPower != EnemyOriginalTotalPower)
                {
                    EnemyDeltaTotalPower = EnemyTotalPower - EnemyOriginalTotalPower;
                    StartCoroutine(ShowTotalPowerUp(Obj, EnemyOriginalTotalPower, EnemyTotalPower));
                    if (EnemyTotalPower > EnemyOriginalTotalPower) DisplayGetPower(Obj, (int)EnemyDeltaTotalPower);
                    EnemyOriginalTotalPower = EnemyTotalPower;
                }
            }
        }

        IEnumerator ShowTotalPowerUp(DefaultObject Obj, float originalTotalPower, float TotalPower)
        {
            if (Obj.ObjectType == ObjectType.Player)
            {
                PlayerTotalPowerText.transform.DOScale(3.0f, 0.0f);
                PlayerTotalPowerText.transform.DOScale(0.8f, 0.2f);
                if (TotalPower > originalTotalPower) TextPlayerTotalPower.DOColor(Color.green, 0.0f);
                else TextPlayerTotalPower.DOColor(Color.red, 0.0f);
                TextPlayerTotalPower.text = ((int)TotalPower).ToString("#,##0");
                TextPlayerTotalPower.DOColor(new Color32(254, 255, 227, 255), 0.5f);
                yield return new WaitForSeconds(0.3f);
            }

            else
            {
                EnemyTotalPowerText.transform.DOScale(3.0f, 0.0f);
                EnemyTotalPowerText.transform.DOScale(0.8f, 0.2f);
                if (TotalPower > originalTotalPower) TextEnemyTotalPower.DOColor(Color.green, 0.0f);
                else TextEnemyTotalPower.DOColor(Color.red, 0.0f);
                TextEnemyTotalPower.text = ((int)TotalPower).ToString("#,##0");
                TextEnemyTotalPower.DOColor(new Color32(254, 255, 227, 255), 0.5f);
                yield return new WaitForSeconds(0.3f);
            }
        }      


        public void ShowInGameComment(bool isPlayerObject, bool isKill)
        {
            var canvas = GameObject.Find("Canvas");            
            GameObject y = Instantiate(isPlayerObject == true ? PlayerComment : EnemyComment, canvas.transform);          
            if (isKill)
            {
                y.GetComponent<InGameComment>().KillIcon.SetActive(true);
                y.GetComponent<InGameComment>().TowerIcon.SetActive(false);
                y.GetComponent<InGameComment>().Comment.text = "You`re not my opponent yet.";
            }
            else
            {
                y.GetComponent<InGameComment>().KillIcon.SetActive(false);
                y.GetComponent<InGameComment>().TowerIcon.SetActive(true);
                y.GetComponent<InGameComment>().Comment.text = "I will take this place!";
            }           
        }

        public void TimeNotice(float Time)
        {
            var canvas = GameObject.Find("Canvas");
            string n = "TimeNoti" + ((int)Time).ToString();
            GameObject a = Instantiate(TimeNoti, canvas.transform);
            a.name = n;
        }

        public void RespawnNotice()
        {
            var canvas = GameObject.Find("Canvas");     
            GameObject a = Instantiate(RespawnNoti, canvas.transform);         
        }

        public void ShowTowerAttackedNoti()
        {
            var canvas = GameObject.Find("Canvas");
            GameObject a = Instantiate(TowerAttackedNoti, canvas.transform);
        }

        
        public void DisplayGetBuffItemEffect(DefaultObject obj)
        {
            GameObject a = Instantiate(ItemMergeEffect, obj.transform);
            a.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y, obj.transform.position.z);
        }

        // Sanghun
        public void DisplayHitEffect(DefaultObject obj, bool critical)
        {
            var effects = GameObject.Find("Effects");
            GameObject a = Instantiate(HitEffect, effects.transform);
            a.transform.position = new Vector3(obj.HudTarget.transform.position.x, obj.HudTarget.transform.position.y - 1.2f, obj.HudTarget.transform.position.z);

            DefaultObject attacker = obj.target;

            if (attacker.ObjectType == ObjectType.Player)
            {
                GameObject b = Instantiate(ResourceManager.Instance.LoadAsset<GameObject>($"Effects/HitEffect_" + GameDataManager.Instance.User.Character.Index), effects.transform);
                b.transform.position = new Vector3(obj.HudTarget.transform.position.x, obj.HudTarget.transform.position.y - 1.2f, obj.HudTarget.transform.position.z);
            }

            if (attacker.ObjectType == ObjectType.Enemy)
            {
                GameObject b = Instantiate(ResourceManager.Instance.LoadAsset<GameObject>($"Effects/HitEffect_" + GameDataManager.Instance.User.EnemyUser.Character.Index), effects.transform);
                b.transform.position = new Vector3(obj.HudTarget.transform.position.x, obj.HudTarget.transform.position.y - 1.2f, obj.HudTarget.transform.position.z);
            }
        }

        public void DisplayTowerHitEffect(DefaultObject obj, bool critical)
        {
            var effects = GameObject.Find("Effects");
            GameObject a = Instantiate(TowerHitEffect, effects.transform);
            a.transform.position = new Vector3(obj.HudTarget.transform.position.x, obj.HudTarget.transform.position.y - 1.2f, obj.HudTarget.transform.position.z);
        }

        // Sanghun
        public void DisplayDeadEffect(DefaultObject obj)
        {
            var effects = GameObject.Find("Effects");
            GameObject a = Instantiate(DeadEffect, effects.transform);
            a.transform.position = new Vector3(obj.HudTarget.transform.position.x, obj.HudTarget.transform.position.y - 1.2f, obj.HudTarget.transform.position.z);
        }

        // Sanghun
        public void DisplayItemMergeEffect(DefaultObject obj)
        {
            GameObject a = Instantiate(ItemMergeEffect, obj.transform);
            a.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y, obj.transform.position.z);
        }

        public void DisplaySoulGet(DefaultObject obj, int value)
        {
            textManager.Add($"+{value}", obj.HudTarget, "soulGet");
        }

        public void DisplayDamage(DefaultObject obj, int value, bool critical)
        {
            textManager.Add($"-{value}", obj.HudTarget, critical ? "critical" : "damage");
        }


        public void DisplayDamageTower(DefaultObject obj, int value)
        {
            textManager.Add($"-{value}", obj.HudTarget, "damageTower");
        }


        public void DisplayHeal(DefaultObject obj, int value)
        {
            textManager.Add($"+{value}", obj.HudTarget, "heal"); // Sanghun, 힐 연출 안겹치게 일단 이렇게
            GameObject a = Instantiate(HealEffect, obj.transform);
            a.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y + 1.0f, obj.transform.position.z);
        }

        public void DisplayLevelUp(DefaultObject obj)
        {
            Transform LevelUpPos = obj.HudTarget;
            textManager.Add("LevelUp", LevelUpPos, "levelUp");
            GameObject a = Instantiate(LevelUpEffect, obj.transform);
            a.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y, obj.transform.position.z);

        }

        public void DisplayGetExp(DefaultObject obj, int value)
        {
            Transform GetExpPos = obj.HudTarget;
            textManager.Add($"+{value} XP", GetExpPos, "getExp");
        }

        public void DisplayGetPower(DefaultObject obj, int value)
        {
            Transform GetPower = obj.HudTarget;
            textManager.Add($"+{value}", GetPower, "getPower");
        }

        public void DisplayRunEffect(DefaultObject obj)
        {
            //var effects = GameObject.Find("Effects");
            if (!GameObject.Find("RunEffect(Clone)"))
            {
                GameObject a = Instantiate(RunEffect, obj.transform);
                a.transform.position = new Vector3(obj.HudTarget.transform.position.x, obj.HudTarget.transform.position.y - 2.2f, obj.HudTarget.transform.position.z);
                Destroy(a, 0.5f);
            }
        }

        public void DisplayEnemyInfo()
        {
            var canvas = GameObject.FindWithTag("MainCanvas").GetComponent<Canvas>();
            Instantiate(PlayInfo_B, canvas.transform);
        }

        public void UpdateDisplay()
        {
            TextKillScore.SetText($"{PlayerObject.KillPoint} : {EnemyObject.KillPoint}");
            TextGold.SetText($"{(int)PlayerObject.Soul}/{PlayerObject.MaxSoul}");
            //if (PlayerObject.Soul >= PlayerObject.MaxSoul) SliderFillImage.DOColor(new Color (1.0f,0.0f,0.39f), 0.5f); // Sanghun
            //else SliderFillImage.DOColor(new Color(0.52f,0.71f,0.11f), 0.5f); // Sanghun
            SoulGainSpeed = (GameDataManager.Instance.GetTimeToSoul(time) + PlayerObject.Stat.GetStatValue(AttrType.Soul)) / GameDataManager.Instance.GameRule.BattleBaseSoulPerSeconds[0];
            TextSoulGainSpeed.SetText(string.Format("x{0:0.0}", SoulGainSpeed));
            SliderSoul.value = PlayerObject.Soul / PlayerObject.MaxSoul; // Sanghun
           // SliderSoul_Int.value = (int)(PlayerObject.Soul) * (1.0f / PlayerObject.MaxSoul); // Sanghun
            TextSkillPoint.SetText($"{PlayerObject.SkillPoint}");
        }

        private int PreSoul, OriginSoul;
        public void UpdateSoulInt(int preSoul,int soul)
        {
            PreSoul = preSoul;
            OriginSoul = soul;
            StopCoroutine("SliderSoulInt");
            StartCoroutine("SliderSoulInt");
        
        }

        IEnumerator SliderSoulInt()
        {
            float time = 0f;
            while (time <= 0.3f)
            {
                time += Time.unscaledDeltaTime * 5f;
                SliderSoul_Int.value = Mathf.Lerp(PreSoul, OriginSoul, time);
                yield return null;
            }
            SliderSoul_Int.value = OriginSoul;
        }

        public void BattleBoxInit()
        {           
            var canvas = GameObject.Find("Canvas");
            GameObject a = Instantiate(GameManager.Instance.MapBuffSpawnNoti, canvas.transform);

            for (int i =0; i <4; i++)
            {
                var battleBoxObject = Instantiate(ResourceManager.Instance.LoadAsset<BattleBoxObject>("Monster/BattleBox"),Box);
                battleBoxObject.Init(GameDataManager.Instance.BattleBoxes[1], Box.GetChild(i), minimap.Chest[i]);
            }
        }




        public void GameStart()
        {
            GameState = GameState.Init;
            BattleBoxInit();
            var map = GameDataManager.Instance.MapList;
            for (int i = 0; i < map.Count; i++)
            {
                if (map[i].Type == "Starting")
                {

                    if (map[i].Parameter1 == 1)
                    {
                        PlayerObject = Instantiate(ResourceManager.Instance.LoadAsset<CharacterObject>($"Character/Character_{GameDataManager.Instance.User.currentCharacter.CharacterIndex}"), new Vector3(0.0f, -0.2f, i * TempDistanceFactor), Quaternion.identity, Character);
                        PlayerObject.Initialize(ObjectType.Player);
                        playerInfo.Init(PlayerObject);
                        BattleCam2_LookPlayer.LookAt = PlayerObject.transform;
                        BattleCam2_LookPlayer.Follow = PlayerObject.transform;
                        BattleCam3_Run.LookAt = PlayerObject.transform;
                        BattleCam3_Run.Follow = PlayerObject.transform;
                        BattleCam4_Battle.LookAt = PlayerObject.transform;
                        BattleCam4_Battle.Follow = PlayerObject.transform;
                        BattleCam6_ResultWin.LookAt = PlayerObject.transform;
                        BattleCam6_ResultWin.Follow = PlayerObject.transform;
                    }
                    if (map[i].Parameter1 == 2)
                    {
                        EnemyObject = Instantiate(ResourceManager.Instance.LoadAsset<CharacterObject>("Character/Character_" + GameDataManager.Instance.User.EnemyUser.Character.Index), new Vector3(0.0f, -0.2f, i * TempDistanceFactor), Quaternion.Euler(0f, 180f, 0f), Character);
                        EnemyObject.Initialize(ObjectType.Enemy,GameDataManager.Instance.User.EnemyUser);
                        enemyInfo.Init(EnemyObject);

                        BattleCam1_Initiate.LookAt = EnemyObject.transform;
                        BattleCam1_Initiate.Follow = EnemyObject.transform;
                        BattleCam8_LookEnemy.LookAt = EnemyObject.transform;
                        BattleCam8_LookEnemy.Follow = EnemyObject.transform;
                        BattleCam9_EnemyBattle.LookAt = EnemyObject.transform;
                        BattleCam9_EnemyBattle.Follow = EnemyObject.transform;
                        BattleCam7_ResultLose.LookAt = EnemyObject.transform;
                        BattleCam7_ResultLose.Follow = EnemyObject.transform;
                    }

                   // 본진 Nexus
                    EnemyIndex++;
                    if (map[i].Parameter2 > 0)
                    {
                        var monster = GameDataManager.Instance.GetMonster(map[i].Parameter2);
                        var monsterObj = Instantiate(ResourceManager.Instance.LoadAsset<GameObject>($"Monster/Monster_{monster.Index}"), Tower);
                        var mon = monsterObj.GetComponent<TowerObject>();
                        mon.InitMonster(monster, Vector3.forward * i * TempDistanceFactor);
                    }                   
                }
                else if (map[i].Type == "Point")
                {
                    EnemyIndex++;
                    if (map[i].Parameter2 > 0)
                    {
                        var monster = GameDataManager.Instance.GetMonster(map[i].Parameter2);
                        var monsterObj = Instantiate(ResourceManager.Instance.LoadAsset<GameObject>($"Monster/Monster_{monster.Index}"), Tower);

                        if (map[i].Parameter1 == 3) // Sanghun, PlayerTower 추가
                        {
                            var mon = monsterObj.GetComponent<TowerObject>();
                            mon.InitMonster(monster, Vector3.forward * i * TempDistanceFactor);
                        }
                    }
                }            
            }
            EnemyIndex++;
            SliderSoul_Int.maxValue = PlayerObject.MaxSoul;

            MapDistance = EnemyObject.transform.position.z - PlayerObject.transform.position.z;
            PlayerStartPos = PlayerObject.transform.position.z;
            EnemyStartPos = EnemyObject.transform.position.z;

            InitUI();
            GameEventManager.Instance.OnEventInit();
            BattleEventManager.Instance.OnEventGameState(GameState);
            StartCoroutine("Co_BattleStart");            
        }

        IEnumerator Co_BattleStart()
        {
            var canvas = GameObject.FindWithTag("MainCanvas").GetComponent<Canvas>();

            Instantiate(BattleUser, canvas.transform);

            yield return new WaitForSeconds(1.5f);
            battleCamController.SetBattleCam("LookPlayer");

            yield return new WaitForSeconds(0.5f);
            PlayerObject.Anim.SetTrigger("BattleInitiate"); // Sanghun, Initial Animation 지정    
            
            yield return new WaitForSeconds(0.5f);
            Instantiate(BattleStart, canvas.transform);
            
            yield return new WaitForSeconds(2.5f);            
            GameState = GameState.Ing;
            StartCoroutine("PlayTimeChecker");
            PlayerObject.RefreshFreeCount = 1;
            EnemyObject.RefreshFreeCount = 1;
            StartCoroutine("PlayerFreeRefreshCountChecker");
            StartCoroutine("EnemyFreeRefreshCountChecker");          

            // Sanghun
            PlayerHpbar = PlayerHpBar_T;
            EnemyHpbar = EnemyHpBar_T;
            TowerHpbar = TowerHpBar_T;
            MonsterHpbar = MonsterHpBar_T;

            //PlayerHpbar = Instantiate(prefabHpbar, HpbarParent);
            //EnemyHpbar = Instantiate(prefabHpbar, HpbarParent);
            //MonsterHpbar = Instantiate(prefabHpbar, HpbarParent);

            MonsterHpbar.SetActive(false);
            PlayerHpbar.Init(PlayerObject, Camera.main, Canvas);
            EnemyHpbar.Init(EnemyObject, Camera.main, Canvas);
            BattleEventManager.Instance.OnEventGameState(GameState);
            StartCoroutine(BattleBaseGoldPerSecond());
            StartCoroutine(StartBooster());

            yield return new WaitForSeconds(0.3f);
            Panel.SetActive(true);
            TopUI.SetActive(true);
        }


        IEnumerator StartBooster()
        {
            PlayerObject.AddBuff(AttrType.MoveSpeed, StatModType.PercentMult, GameDataManager.Instance.GameRule.ReviveMoveSpeed, GameDataManager.Instance.GameRule.ReviveMoveTime, GameDataManager.Instance.GetItem(300601).IconSprite);
            EnemyObject.AddBuff(AttrType.MoveSpeed, StatModType.PercentMult, GameDataManager.Instance.GameRule.ReviveMoveSpeed, GameDataManager.Instance.GameRule.ReviveMoveTime, GameDataManager.Instance.GetItem(300601).IconSprite);
            yield return new WaitForSeconds(GameDataManager.Instance.GameRule.ReviveMoveTime);
        }

        IEnumerator PlayTimeChecker()
        {
            yield return new WaitForSeconds(120.0f);  // 120초 남음
            //TimeNotice(120.0f);

            //yield return new WaitForSeconds(30.0f); // 30초 남음
            //TimeNotice(30.0f);

            //yield return new WaitForSeconds(20.0f); // 10초 카운트 다운
            //TimeNotice(10.0f);

            //yield return new WaitForSeconds(11.0f); // 서든데스
            //if (GameState == GameState.Ing) TimeNotice(999.0f);
        }

        IEnumerator PlayerFreeRefreshCountChecker()
        {
            for (int i = 0; i < 1000; i++)
            {
                if (GameState != GameState.Ing) break;
                float refreshTime = 30.0f;
                PlayerObject.FreeRefreshRemainTime = 30.0f;
                for (int k = 0; k < refreshTime; k++)
                {
                    yield return new WaitForSeconds(1.0f);                
                    PlayerObject.FreeRefreshRemainTime--;
                    if (PlayerObject.RefreshFreeCount >= 3)
                    {
                        PlayerObject.FreeRefreshRemainTime = 30.0f;
                        break;
                    }
                }
                if (PlayerObject.RefreshFreeCount < 3) PlayerObject.RefreshFreeCount++;                
                PlayerObject.FreeRefreshRemainTime = 30.0f;           

            }
        }
        IEnumerator EnemyFreeRefreshCountChecker()
        {
            for (int i = 0; i < 1000; i++)
            {
                if (GameState != GameState.Ing) break;
                float refreshTime = 30.0f;
                EnemyObject.FreeRefreshRemainTime = 30.0f;
                for (int k = 0; k < refreshTime; k++)
                {
                    yield return new WaitForSeconds(1.0f);
                    EnemyObject.FreeRefreshRemainTime--;
                    if (EnemyObject.RefreshFreeCount >= 3)
                    {
                        EnemyObject.FreeRefreshRemainTime = 30.0f;
                        break;
                    }
                }
                if (EnemyObject.RefreshFreeCount < 3) EnemyObject.RefreshFreeCount++;
                EnemyObject.FreeRefreshRemainTime = 30.0f;

            }
        }

        public void TowerFightCheck()
        {
            isTowerFight = false;

            GameObject[] a = GameObject.FindGameObjectsWithTag("PlayerTower");
            for (int i = 0 ; i < a.Length ; i++)
            {
                if (a[i].GetComponent<DefaultObject>().target == EnemyObject)
                {
                    isTowerFight = true;
                    isPlayerTower = true;
                    TowerTarget = a[i];
                    return;
                }
            }

            GameObject[] b = GameObject.FindGameObjectsWithTag("EnemyTower");
            for (int i = 0; i < b.Length; i++)
            {
                if (b[i].GetComponent<DefaultObject>().target == PlayerObject)
                {
                    isTowerFight = true;
                    isPlayerTower = false;
                    TowerTarget = b[i];
                    return;
                }
            }
        }

        public void SetBattleTargetToTower()
        {   
            if (PlayerObject.TargetedTower!= null)
            {
                PlayerObject.ForceBattleTarget(PlayerObject.TargetedTower);
                Debug.Log("Target change to Tower");
            }
        }

        public void SetBattleTargetToEnemy()
        {
            PlayerObject.TargetedTower = PlayerObject.target;
            PlayerObject.ForceBattleTarget(EnemyObject);
            Debug.Log("Target change to Enemy");
        }

        public void SetBattleTargetNull()
        {            
            PlayerObject.ForceBattleTarget(null);
            Debug.Log("Target change to Null");
        }

        private void Update()
        {
            if (GameState == GameState.Ing)
            {
                time += Time.deltaTime;
                var ts = TimeSpan.FromSeconds(time);

                TextTime.SetText(string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds));               
                //Debug.Log ((PlayerObject.transform.position - EnemyObject.transform.position).sqrMagnitude);

                // LeftDistance Check
                var leftDistance = EnemyObject.transform.position.z - PlayerObject.transform.position.z;
                TextLeftDistance.SetText(string.Format("{0:0}" + "M", leftDistance));
                TextLeftDistance.fontSize = Mathf.Clamp( 50.0f * (1.0f + 10.0f/leftDistance), 50.0f, 999.0f );
                if (leftDistance < 20.0f) TextLeftDistance.color = new Color32 (190,107,82,230); 
                else TextLeftDistance.color = new Color32 (130, 130, 130,255);
                if (PlayerObject.IsDead == false)
                {
                    if (leftDistance < 5.0f) LeftDistance.SetActive(false);
                    else LeftDistance.SetActive(true);
                }


                // BattleCamControl
                if (isLookPlayer == true)
                    battleCamController.SetBattleCam("LookPlayer");
                else
                {
                    //TowerFightCheck();
                    if (PlayerObject.IsDead == false) // Player 시점 노출
                    {
                        battleCamController.SetBattleCam("Run");
                    }
                    else // Enemy 시점 노출
                    {
                        battleCamController.SetBattleCam("LookEnemy");
                    }
                }

                // Battle Result
                if (GameObject.FindGameObjectsWithTag("PlayerTower").Length <= 0) OnBattleOver(false);
                if (GameObject.FindGameObjectsWithTag("EnemyTower").Length <= 0) OnBattleOver(true);                          
                
                CalculateTotalPower(PlayerObject);
                CalculateTotalPower(EnemyObject);// Sanghun
            }

            if (PlayerObject.RefreshFreeCount <= 0)           
                RefreshGrayEffect.effectFactor = 1f;          
            else
                RefreshGrayEffect.effectFactor = 0f;


        }

        public void InitUI()
        {
           // TextPlayer.SetText($"{"Byabyak"/*GameDataManager.Instance.User.NickName*/}");
            //TextEnemy.SetText("AI");
            TextKillScore.SetText("0 : 0");
            TextGold.SetText($"{(int)PlayerObject.Soul}/{PlayerObject.MaxSoul}");
            TextSoulGainSpeed.SetText(string.Format("x{0:0.0}", (GameDataManager.Instance.GetTimeToSoul(time) + PlayerObject.Stat.GetStatValue(AttrType.Soul)) / GameDataManager.Instance.GameRule.BattleBaseSoulPerSeconds[0]));
            SliderSoul.value = PlayerObject.Soul / PlayerObject.MaxSoul; // Sanghun
            SliderSoul_Int.value = (int)(PlayerObject.Soul) * (1.0f / PlayerObject.MaxSoul); // Sanghun
            TextSkillPoint.SetText(0);
            TextTime.SetText("00:00");
            TextLeftDistance.SetText(string.Format("{0:0}" + "m", EnemyObject.transform.position.z - PlayerObject.transform.position.z)); //Sanghun
        }

        IEnumerator BattleBaseGoldPerSecond()
        {
            while (true)
            {
                if (PlayerObject.Soul <= PlayerObject.MaxSoul) PlayerObject.Soul += (GameDataManager.Instance.GetTimeToSoul(time) + PlayerObject.Stat.GetStatValue(AttrType.Soul)) * Time.deltaTime;
                if (EnemyObject.Soul <= EnemyObject.MaxSoul) EnemyObject.Soul += (GameDataManager.Instance.GetTimeToSoul(time) + EnemyObject.Stat.GetStatValue(AttrType.Soul)) * Time.deltaTime;
                yield return null;
            }
        }

        public void OnBattleOver(bool isWin)
        {
            GameState = GameState.GameOver;

            StopAllCoroutines();

            StartCoroutine(Co_BattleOver());
            IEnumerator Co_BattleOver()
            {
                Panel.SetActive(false);
                TopUI.SetActive(false);

                var canvas = GameObject.FindWithTag("MainCanvas").GetComponent<Canvas>();
                if (isWin)
                {
                    Debug.Log("win");
                    //Instantiate(BattleOverWin, canvas.transform);
                    battleCamController.SetBattleCam("ResultWin");
                    //EnemyObject.SetDamage(PlayerObject, 1000000, false);
                }

                else
                {
                    Debug.Log("lose");
                    //Instantiate(BattleOverLose, canvas.transform);
                    battleCamController.SetBattleCam("ResultLose");
                    //PlayerObject.SetDamage(EnemyObject, 1000000, false);
                }

                yield return new WaitForSeconds(1.5f);
                ServerManager.Instance.BattleResult(isWin);
                yield return null;
            }
        }

        #region ItemMove

        public void SelectBeginDragItem(BattleItem battle, int index)
        {
            InvenIndex = index;
            ImageCopy.SetActive(true);
            ImageCopy.SetSprite(battle.Item.IconSprite);
            BattleItemSellPopup.InitSellPopup(battle);
        }

        public void SelectDragItem(Vector2 pos)
        {
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.GetComponent<RectTransform>(), pos, Canvas.worldCamera, out var temp))
            {
                ImageCopy.rectTransform.anchoredPosition = temp;
                ImageCopy.rectTransform.localScale = new Vector3(2.0f, 2.0f, 0f); // Sanghun
            }
        }

        public void SelectEndDragItem()
        {
            if (MoveIndex >= 0 && InvenIndex != MoveIndex)
            {
                PlayerObject.SwitchBattItem(MoveIndex, InvenIndex);
            }
            else if (MoveIndex == -1)
            {
                PlayerObject.SellBattleItem(InvenIndex);
            }

            MoveIndex = -2;
            InvenIndex = -2;
            ImageCopy.SetActive(false);
            BattleItemSellPopup.Close();
        }

        #endregion

        #region Event

        public void OnClickPause()
        {
            Secret.PopupPause.Instance.Open();
        }

        #endregion 
    }

    public enum GameState
    {
        Init,
        Ing,
        GameOver,
    }
}
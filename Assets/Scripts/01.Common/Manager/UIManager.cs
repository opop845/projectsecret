﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI.Extensions;

namespace Secret
{
    public class UIManager : MonoSingleton<UIManager>
    {
        public GameObject DimObject;
        [SerializeField] GameObject ServerIndicator;

        Stack<BaseUI> popupStack = new Stack<BaseUI>();
        void Awake()
        {
            if (Instance != this)
                Destroy(gameObject);
            else
                DontDestroyOnLoad(gameObject);
        }

        public void SetServerIndicaator(bool isOn)
        {
            ServerIndicator.SetActive(isOn);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (ServerIndicator.activeSelf) return;
                if (popupStack.Count.Equals(0))
                {
                    if (SceneLoadManager.Instance.CurrentSceneName == "Battle")
                        PopupPause.Instance.Open();
                    else
                        PopupGameExit.Instance.Open();
                }
                else
                    popupStack.Peek().Close();
            }
        }

        public void OnEventSceneChanged()
        {
            while (popupStack.Count > 0)
                popupStack.Peek().ForceClose();
        }

        public T CreateInstance<T>() where T : BaseUI
        {
            var prefab = GetPrefab<T>();
            var canvas = GameObject.FindGameObjectWithTag("MainCanvas");

            if (canvas != null)
                return Instantiate(prefab, canvas.transform);
            else
                throw new MissingReferenceException("MainCanvas not found");
        }

        private T GetPrefab<T>() where T : BaseUI
        {
            var prefab = ResourceManager.Instance.LoadAsset<T>($"UI/{typeof(T).Name}");
            /* if (prefab != null) */
            return prefab;

            // throw new MissingReferenceException("Prefab not found for type " + typeof(T));

        }

        public bool CheckPopup()
        {
            return popupStack.Count.Equals(0);
        }

        public void PopUI(BaseUI baseUI)
        {
            if (popupStack.Contains(baseUI))
                popupStack.Pop();
        }

        public void PushUI(BaseUI baseUI)
        {
            if (!popupStack.Contains(baseUI))
            {
                popupStack.Push(baseUI);
                baseUI.canvas.sortingOrder = popupStack.Count * 2;
            }
        }
    }
}
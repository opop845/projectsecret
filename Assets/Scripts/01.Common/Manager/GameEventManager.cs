﻿
using Secret.Data;
using Secret.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;

namespace Secret
{
    public class GameEventManager : Singleton<GameEventManager>
    {
        public event Action<string> EventToast;
        public void OnEventToast(string value) => EventToast?.Invoke(value);

        public event Action<int> EventGoldChanged;
        public void OnEventGoldChanged(int gold) => EventGoldChanged?.Invoke(gold);

        public event Action EventExpUp;
        public void OnEventExpUp() => EventExpUp?.Invoke();



        public event Action EventInit;
        public void OnEventInit() => EventInit?.Invoke();

        public event Action<bool> EventUIHide;
        public void OnEventUIHide(bool isHide) => EventUIHide?.Invoke(isHide);

        #region Battle

        public event Action EventBattleLevelUp;
        public void OnEventBattleLevelUp() => EventBattleLevelUp?.Invoke();

        public event Action EventBattleGoldUpdate;
        public void OnEventBattleGoldUpdate() => EventBattleGoldUpdate?.Invoke();

        public event Action EventBattleItemUpdate;
        public void OnEventBattleItemUpdate() => EventBattleItemUpdate?.Invoke();

        public event Action<DefaultObject, BuffController> EventNewBuff;
        public void OnEventNewBuff(DefaultObject obj, BuffController buff) => EventNewBuff?.Invoke(obj, buff);

        public event Action<float, bool> EventBossHpGauge;
        public void OnEventBossHpGauge(float value, bool isOn) => EventBossHpGauge?.Invoke(value, isOn);

        public event Action<DefaultObject, DefaultObject> EventKillNotice;
        public void OnEventKillNotice(DefaultObject attacker, DefaultObject deadObject) => EventKillNotice?.Invoke(attacker, deadObject);

        #endregion

        #region BattleItem

        public event Action<Spell, bool> EventSpellInfo;
        public void OnEventSpellInfo(Spell spell, bool isPress) => EventSpellInfo?.Invoke(spell, isPress);

        public event Action<Item, bool> EventItemInfo;
        public void OnEventItemInfo(Item item, bool isPress) => EventItemInfo?.Invoke(item, isPress);

        //아이템 구매 이벤트
        public event Action<CharacterObject, Item> EventItemPurchase;
        public void OnEventItemPurchase(CharacterObject obj, Item item) => EventItemPurchase?.Invoke(obj, item);

        //아이템 사용 이벤트
        public event Action<CharacterObject, Item> EventItemUse;
        public void OnEventItemUse(CharacterObject obj, Item item) => EventItemUse?.Invoke(obj, item);

        //아이템 합성 이벤트
        public event Action<CharacterObject, BattleItem> EventItemMerge;
        public void OnEventItemMerge(CharacterObject obj, BattleItem item) => EventItemMerge?.Invoke(obj, item);

        //인벤토리 Refresh 이벤트
        public event Action<CharacterObject> EventInventoryRefresh;
        public void OnEventItemListRefresh(CharacterObject obj) => EventInventoryRefresh?.Invoke(obj);

        public event Action<Skill> EventUseSkill;
        public void OnEventUseSkill(Skill skill) => EventUseSkill?.Invoke(skill);

        #endregion

        #region Lobby

        public event Action EventDeckSpellChanged;
        public void OnEventDeckSpellChanged() => EventDeckSpellChanged?.Invoke();

        public event Action<UserCharacter> EventCharacterChanged;
        public void OnEventCharacterChanged(UserCharacter character) => EventCharacterChanged?.Invoke(character);

        public event Action<MainTabType> EventLobbyTabChanged;
        public void OnEventLobbyTabChanged(MainTabType type) => EventLobbyTabChanged?.Invoke(type);

        public event Action<UserCharacter> EventCharacterExpertLevelUp;
        public void OnEventCharacterExpertLevelUp(UserCharacter character) => EventCharacterExpertLevelUp?.Invoke(character);

        public event Action<UserCharacter> EventCharacterAbilityLevelUp;
        public void OnEventCharacterAbilityLevelUp(UserCharacter character) => EventCharacterAbilityLevelUp?.Invoke(character);

        public event Action EventNewEnemyUser;
        public void OnEventNewEnemyUser() => EventNewEnemyUser?.Invoke();

        public event Action EventRewardTrophy;
        public void OnEventRewardTrophy() => EventRewardTrophy?.Invoke();

        public event Action EventForceQuitLose;
        public void OnEventForceQuitLose() => EventForceQuitLose?.Invoke();

        #endregion
    }
    public class BattleEventManager : Singleton<BattleEventManager>
    {
        //게임상태 이벤트
        public event Action<GameState> EventGameState;
        public void OnEventGameState(GameState state) => EventGameState?.Invoke(state);

        //캐릭터 스탯 변화 이벤트
        public event Action<DefaultObject, int> EventStatUpdate;
        public void OnEventStatUpdate(DefaultObject obj, int battlePower) => EventStatUpdate?.Invoke(obj, battlePower);
    }

}
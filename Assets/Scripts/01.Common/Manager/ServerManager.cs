﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.EventsModels;
using System;
using CodeStage.AntiCheat.Storage;
using PlayFab.Events;
using Photon.Pun;
using Photon.Realtime;
using I2.Loc;
using UnityEngine.Networking;
using Secret.Server;
using PlayFab.Json;
using Secret.Data;
using Secret.UI;

namespace Secret
{
    public class ServerManager : MonoSingleton<ServerManager>
    {
        public bool IsLocalData;

        #region KEY
        public const string KEY_USER_ID = "user_id";
        #endregion

        bool prosscing = false;
        bool isLogin = false;

        public string SesstionTicket { get; private set; }
        public string PlayfabId { get; private set; }
        public string DisplayName { get; private set; }
        public bool NewUser { get; private set; } = false;

        public bool isDeckSave { get; set; } = false;

        private Dictionary<string, object> dicSave = new Dictionary<string, object>();


        void Awake()
        {
            if (Instance != this)
                Destroy(gameObject);
            else
            {
                Application.quitting += OnQuitting;
                ObscuredPrefs.OnAlterationDetected += OnAlterationDetected;
                DontDestroyOnLoad(gameObject);
            }
        }

        void OnDestroy()
        {
            Application.quitting -= OnQuitting;
            ObscuredPrefs.OnAlterationDetected -= OnAlterationDetected;
        }

        private void Start()
        {
            PopupState.Instance.OpenState(PopupStateType.Login);
            GuestLogin();
        }

        void OnPlayFabError(PlayFabError error) => Debug.Log(error.GenerateErrorReport());

        #region EventHandler

        void OnQuitting()
        {
        }

        void OnAlterationDetected()
        {
            Debug.Log("로컬 데이터가 변환되었습니다.");
        }

        #endregion EventHanlder

        public void GuestLogin()
        {
            if (!prosscing && !isLogin)
            {
                prosscing = true;
                string customId;
                if (ObscuredPrefs.HasKey(KEY_USER_ID))
                    customId = ObscuredPrefs.GetString(KEY_USER_ID);
                else
                {
                    customId = Guid.NewGuid().ToString();
                    ObscuredPrefs.SetString(KEY_USER_ID, customId);
                }
                var request = new LoginWithCustomIDRequest
                {
                    CustomId = customId,
                    CreateAccount = true,

                    InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
                    {
                        GetPlayerProfile = true
                    }
                };
                PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnPlayFabError);
            }
        }

        public void OnApplicationQuit()
        {
            if (isDeckSave)
            {
                var dic = new Dictionary<string, object>();
                List<UserCharacterBody> characters = new List<UserCharacterBody>();
                foreach (var character in GameDataManager.Instance.User.userCharactersDic)
                {
                    if (!character.Value.IsLock)
                        characters.Add(character.Value.GetServerData());
                }

                dic.Add("Character", GameDataManager.Instance.User.currentCharacter.CharacterIndex);
                dic.Add("Spell", characters);
                var request = new ExecuteCloudScriptRequest()
                {
                    FunctionName = "SaveDeck",
                    FunctionParameter = dic,
                    GeneratePlayStreamEvent = true,

                };
                PlayFabClientAPI.ExecuteCloudScript(request, null, OnPlayFabError); ;
            }
        }

        public void GoogleLogin()
        {
            Debug.Log("준비중");
        }

        void OnLoginSuccess(LoginResult result)
        {
            NewUser = result.NewlyCreated;
            if (NewUser)
                PlayFabProfilesAPI.SetProfileLanguage(new PlayFab.ProfilesModels.SetProfileLanguageRequest() { Language = LocalizationManager.CurrentLanguageCode }, (tt) => { }, OnPlayFabError);
            SesstionTicket = result.SessionTicket;
            PlayfabId = result.PlayFabId;
            if (result.NewlyCreated || string.IsNullOrEmpty(result.InfoResultPayload.PlayerProfile.DisplayName))
                PopupNickname.Instance.Open();
            else
                GetServerData();
        }

        public void GetServerData()
        {
            var request = new GetPlayerCombinedInfoRequest
            {
                InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
                {
                    GetTitleData = !IsLocalData,
                    GetPlayerStatistics = true,
                    GetUserVirtualCurrency = true,
                    GetUserData = true,
                    GetPlayerProfile = true
                },
                PlayFabId = PlayfabId
            };
            PlayFabClientAPI.GetPlayerCombinedInfo(request, OnGetServerData, OnPlayFabError);
            GetLeaderBord();
        }

        void OnGetServerData(GetPlayerCombinedInfoResult result)
        {
            GameDataManager.Instance.InitServerData(result.InfoResultPayload);
            RequestPhotonToken();
        }

        public void SetNickName(string nickname)
        {
            var request = new UpdateUserTitleDisplayNameRequest()
            {
                DisplayName = nickname
            };

            PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnNickNameSetting, OnPlayFabError);
        }

        void OnNickNameSetting(UpdateUserTitleDisplayNameResult result)
        {
            // GameDataManager.Instance.User.NickName = result.DisplayName;
            PopupNickname.Instance.ForceClose();
            GetServerData();
        }

        // void GetServerTime() => PlayFabClientAPI.GetTime(new GetTimeRequest(), OnGetServerTime, OnPlayFabError);

        // void OnGetServerTime(GetTimeResult result)
        // {
        //    // GameDataManager.Instance.User.SetServerTime(result.Time);
        ////     SceneLoadManager.Instance.LoadScene("Lobby");

        //     //GetStore();
        // }
        private void RequestPhotonToken()
        {
            PlayFabClientAPI.GetPhotonAuthenticationToken(new GetPhotonAuthenticationTokenRequest()
            {
                PhotonApplicationId = PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime
            }, AuthenticateWithPhoton, OnPlayFabError);
        }

        private void AuthenticateWithPhoton(GetPhotonAuthenticationTokenResult obj)
        {
            Debug.Log("Photon token acquired: " + obj.PhotonCustomAuthenticationToken + "  Authentication complete.");
            //We set AuthType to custom, meaning we bring our own, PlayFab authentication procedure.
            var customAuth = new AuthenticationValues { AuthType = CustomAuthenticationType.Custom };

            //We add "username" parameter. Do not let it confuse you: PlayFab is expecting this parameter to contain player PlayFab ID (!) and not username.
            customAuth.AddAuthParameter("username", PlayfabId);    // expected by PlayFab custom auth service

            //We add "token" parameter. PlayFab expects it to contain Photon Authentication Token issues to your during previous step.
            customAuth.AddAuthParameter("token", obj.PhotonCustomAuthenticationToken);

            //We finally tell Photon to use this authentication parameters throughout the entire application.
            PhotonNetwork.AuthValues = customAuth;
            PopupState.Instance.ForceClose();
            GameEventManager.Instance.OnEventInit();
        }

        #region Notice

        public void GetNotice()
        {
            PlayFabClientAPI.GetTitleNews(new GetTitleNewsRequest(), OnGetNotice, OnPlayFabError);
        }

        void OnGetNotice(GetTitleNewsResult result)
        {
            Debug.Log(result.News[0].Body);
        }

        #endregion

        public void DownloadFileFromCDN(string key)
        {
            GetDownloadUrl(key, presignedUrl =>
            {
                GetFile(presignedUrl);
            });
        }

        void GetDownloadUrl(string key, Action<string> onComplete)
        {
            PlayFabClientAPI.GetContentDownloadUrl(new GetContentDownloadUrlRequest()
            {
                Key = key,
                ThruCDN = true
            }, result => onComplete(result.URL),
            error => Debug.LogError(error.GenerateErrorReport()));
        }

        void GetFile(string preauthorizedUrl)
        {
            Debug.Log(preauthorizedUrl);
            StartCoroutine(Request());
            // Implement your own GET HTTP request here.
            IEnumerator Request()
            {
                using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(preauthorizedUrl))
                {
                    yield return uwr.SendWebRequest();

                    if (uwr.isNetworkError || uwr.isHttpError)
                    {
                        Debug.Log(uwr.error);
                    }
                    else
                    {
                        // Get downloaded asset bundle
                        var texture = DownloadHandlerTexture.GetContent(uwr);
                    }
                }
            }
        }

        public void GetLeaderBord()
        {
            PlayFabClientAPI.GetLeaderboardAroundPlayer(new GetLeaderboardAroundPlayerRequest() { MaxResultsCount = 10, StatisticName = "Score", PlayFabId = PlayfabId }, temp, OnPlayFabError);
        }


        void temp(GetLeaderboardAroundPlayerResult result)
        {
            Debug.Log(result);
        }

        #region Set

        public void SetUserData()
        {
            //var request = new UpdateUserDataRequest()
            //{
            //    Data = GameDataManager.Instance.User.GetPlayerData()
            //};
            //PlayFabClientAPI.UpdateUserData(request, (result) => Debug.Log("ok"), OnPlayFabError);
        }

        #endregion

        #region Photon

        public void MasterConnect()
        {
            PhotonNetwork.ConnectUsingSettings();
        }

        public void OnDisConnect()
        {
            PhotonNetwork.Disconnect();
        }

        public void CreateRoom()
        {

#if UNITY_EDITOR
            PhotonNetwork.CreateRoom(null);
#elif UNITY_ANDROID
            PhotonNetwork.JoinRandomRoom();
#endif
        }

        #endregion

        #region Character

        //캐릭터 레벨업 UIManager.Instance.SetServerIndicaator(true);
        public void LevelUpCharacter(UserCharacter userCharacter)
        {
            if (userCharacter.NextLevel == null)
                GameEventManager.Instance.OnEventToast("만렙입니다.");
            else if (userCharacter.NextLevel.RequiredExp > userCharacter.Exp)
                GameEventManager.Instance.OnEventToast("경험치가 부족합니다..");
            else if (userCharacter.NextLevel.RequiredGold > GameDataManager.Instance.User.Gold)
                GameEventManager.Instance.OnEventToast("골드가 부족합니다.");
            else
            {
                dicSave.Clear();
                dicSave.Add("CharacterIndex", userCharacter.CharacterIndex);

                var request = new ExecuteCloudScriptRequest()
                {
                    FunctionName = "LevelUpCharacter",
                    FunctionParameter = dicSave,
                    GeneratePlayStreamEvent = true,
                };
                PlayFabClientAPI.ExecuteCloudScript(request, OnResultLevelUpCharacter, OnPlayFabError);
                UIManager.Instance.SetServerIndicaator(true);
            }
        }

        void OnResultLevelUpCharacter(ExecuteCloudScriptResult result)
        {
            UIManager.Instance.SetServerIndicaator(false);
            if (result.Logs.Count > 0)
                GameEventManager.Instance.OnEventToast(result.Logs[0].Message);
            else
            {
                var body = PlayFabSimpleJson.DeserializeObject<LevelUpCharacterResponseBody>(result.FunctionResult.ToString());
                GameEventManager.Instance.OnEventToast("캐릭터 레벨업!!.");
                if (body != null)
                {
                    GameDataManager.Instance.User.userCharactersDic[body.UserCharacter.Index].LevelUp(body.UserCharacter);
                    GameDataManager.Instance.User.Gold = body.GG;
                    if (body.UserSpell != null)
                    {
                        GameDataManager.Instance.User.AddUserSpell(body.UserSpell);
                    }
                    GameEventManager.Instance.OnEventCharacterExpertLevelUp(GameDataManager.Instance.User.userCharactersDic[body.UserCharacter.Index]);
                }
            }
        }

        //어빌리티 레벨업
        public void LevelUpAbility(UserCharacter userCharacter, UserCharacterAbility ability)
        {
            if (userCharacter.RemainAbilityPoint < ability.characterAbility.RequiredAbilityPoint)
                GameEventManager.Instance.OnEventToast("어빌리티가 부족합니다.");
            else
            {
                dicSave.Clear();
                dicSave.Add("CharacterIndex", userCharacter.CharacterIndex);
                dicSave.Add("AbilityIndex", ability.characterAbility.AbilityIndex);

                var request = new ExecuteCloudScriptRequest()
                {
                    FunctionName = "LevelUpAbility",
                    FunctionParameter = dicSave,
                    GeneratePlayStreamEvent = true,
                };
                PlayFabClientAPI.ExecuteCloudScript(request, OnResultLevelUpAbility, OnPlayFabError);
                UIManager.Instance.SetServerIndicaator(true);
            }
        }

        void OnResultLevelUpAbility(ExecuteCloudScriptResult result)
        {
            UIManager.Instance.SetServerIndicaator(false);
            if (result.Logs.Count > 0)
                GameEventManager.Instance.OnEventToast(result.Logs[0].Message);
            else
            {
                var body = PlayFabSimpleJson.DeserializeObject<LevelUpAbilityResponseBody>(result.FunctionResult.ToString());
                GameEventManager.Instance.OnEventToast("어빌리티 레벨업!!.");
                if (body != null)
                {
                    GameDataManager.Instance.User.userCharactersDic[body.CharacterIndex].LevelUpAbility(body.AbilityIndex - 1, body.AbilityLevel);
                    GameEventManager.Instance.OnEventCharacterAbilityLevelUp(GameDataManager.Instance.User.userCharactersDic[body.CharacterIndex]);
                }
            }
        }

        #endregion

        #region Spell

        public void LevelUpSpell(UserSpell userSpell)
        {
            var nextSpellLevel = GameDataManager.Instance.GetSpellLevel(userSpell.Level + 1);
            if (nextSpellLevel != null)
            {
                if (GameDataManager.Instance.User.Gold < nextSpellLevel.RequiredGold)
                    GameEventManager.Instance.OnEventToast("골드가 부족합니다.");
                else if (userSpell.Count < nextSpellLevel.RequiredSpellCardAmount)
                    GameEventManager.Instance.OnEventToast("조각이 부족합니다.");
                else
                {
                    var dic = new Dictionary<string, int>();
                    dic.Add("SpellIndex", userSpell.Index);
                    var request = new ExecuteCloudScriptRequest()
                    {
                        FunctionName = "LevelUpSpell",
                        FunctionParameter = dic,
                        GeneratePlayStreamEvent = true,

                    };
                    PlayFabClientAPI.ExecuteCloudScript(request, OnResultLevelUpSpell, OnPlayFabError);
                    UIManager.Instance.SetServerIndicaator(true);
                }
            }
            else
            {
                GameEventManager.Instance.OnEventToast("최대 레벨입니다.");
            }
        }

        void OnResultLevelUpSpell(ExecuteCloudScriptResult result)
        {
            UIManager.Instance.SetServerIndicaator(false);
            if (result.Logs.Count > 0)
                GameEventManager.Instance.OnEventToast(result.Logs[0].Message);
            else
            {
                var body = PlayFabSimpleJson.DeserializeObject<LevelUpSpellResponseBody>(result.FunctionResult.ToString());
                GameEventManager.Instance.OnEventToast("레벨업!!.");
                if (body != null)
                {
                    var userspell = GameDataManager.Instance.User.GetUserSpell(body.UserSpell.Index);
                    userspell.Level = body.UserSpell.Level;
                    userspell.Count = body.UserSpell.Count;
                    GameDataManager.Instance.User.Gold = body.GG;
                    GameEventManager.Instance.OnEventGoldChanged(body.GG);
                    GameEventManager.Instance.OnEventDeckSpellChanged();
                }
            }
        }

 

        #endregion

        #region Battle

        public void NewEnemyUser()
        {
            UIManager.Instance.SetServerIndicaator(true);

            var request = new ExecuteCloudScriptRequest()
            {
                FunctionName = "NewEnemyMatching",
                GeneratePlayStreamEvent = true,
            };
            PlayFabClientAPI.ExecuteCloudScript(request, OnResultNewEnemyUser, OnPlayFabError);
            UIManager.Instance.SetServerIndicaator(true);
        }

        void OnResultNewEnemyUser(ExecuteCloudScriptResult result)
        {
            UIManager.Instance.SetServerIndicaator(false);
            if (result.Logs.Count > 0)
                GameEventManager.Instance.OnEventToast(result.Logs[0].Message);
            else
            {
                var body = PlayFabSimpleJson.DeserializeObject<EnemyUserBody>(result.FunctionResult.ToString());
                if (body != null)
                {
                    GameDataManager.Instance.User.EnemyUser = new EnemyUser(body);
                    GameEventManager.Instance.OnEventNewEnemyUser();
                }
            }
        }

        public void BattleStart()
        {
            dicSave.Clear();
            if (isDeckSave)
            {
                List<UserCharacterBody> characters = new List<UserCharacterBody>();
                foreach (var character in GameDataManager.Instance.User.userCharactersDic)
                {
                    if (!character.Value.IsLock)
                        characters.Add(character.Value.GetServerData());
                }

                dicSave.Add("Character", GameDataManager.Instance.User.currentCharacter.CharacterIndex);
                dicSave.Add("Spell", characters);
                isDeckSave = false;
            }
            dicSave.Add("BattleInfo", GameDataManager.Instance.User.currentCharacter.GetBattleInfo());
            var request = new ExecuteCloudScriptRequest()
            {
                FunctionName = "BattleStart",
                FunctionParameter = dicSave,
                GeneratePlayStreamEvent = true,

            };
            PlayFabClientAPI.ExecuteCloudScript(request, OnResultBattleStart, OnPlayFabError);
            UIManager.Instance.SetServerIndicaator(true);
        }

        void OnResultBattleStart(ExecuteCloudScriptResult result)
        {
            UIManager.Instance.SetServerIndicaator(false);
            PopupMatching.Instance.BattleStart();
        }

        public void ForceQuitLose()
        {
            UIManager.Instance.SetServerIndicaator(true);

            var request = new ExecuteCloudScriptRequest()
            {
                FunctionName = "ForceQuitLose",
                GeneratePlayStreamEvent = true,
            };
            PlayFabClientAPI.ExecuteCloudScript(request, OnResultForceQuitLose, OnPlayFabError);
        }

        void OnResultForceQuitLose(ExecuteCloudScriptResult  result)
        {
            UIManager.Instance.SetServerIndicaator(false);
            if (result.Logs.Count > 0)
                GameEventManager.Instance.OnEventToast(result.Logs[0].Message);
            else
            {
                var body = PlayFabSimpleJson.DeserializeObject<ForceQuitLoseResponseBody>(result.FunctionResult.ToString());
                if (body != null)
                {
                    GameEventManager.Instance.OnEventToast($"강제종료로 패배처리되었습니다. 트로피 점수 -{ GameDataManager.Instance.User.Trophy - body.Trophy}");
                    GameDataManager.Instance.User.Trophy = body.Trophy;
                    GameDataManager.Instance.User.EnemyUser = new EnemyUser(body.EnemyUser);
                    GameEventManager.Instance.OnEventForceQuitLose();
                }
            }
        }

        public void BattleResult(bool isWin)
        {
            dicSave.Clear();
            dicSave.Add("IsWin", isWin);
            var request = new ExecuteCloudScriptRequest()
            {
                FunctionName = "BattleResult",
                FunctionParameter = dicSave,
                GeneratePlayStreamEvent = true,

            };
            PlayFabClientAPI.ExecuteCloudScript(request, OnResultBattleResult, OnPlayFabError);
          //  UIManager.Instance.SetServerIndicaator(true);
        }

        void OnResultBattleResult(ExecuteCloudScriptResult result)
        {
           // UIManager.Instance.SetServerIndicaator(false);
            if (result.Logs.Count > 0)
                GameEventManager.Instance.OnEventToast(result.Logs[0].Message);
            else
            {
                var body = PlayFabSimpleJson.DeserializeObject<BattleResultResponseBody>(result.FunctionResult.ToString());
                if (body.IsWin)
                {
                    if (body != null)
                    {
                        if (body.Reward != null)
                        {
                            foreach (var reward in body.Reward)
                            {
                                switch (reward.RewardType)
                                {
                                    case "SpellCard":
                                        var userSpell = GameDataManager.Instance.User.userSpells.Find(x => x.Index == reward.Index);
                                        if (userSpell != null)
                                            userSpell.Count += reward.Value;
                                        break;
                                }
                            }
                        }
                        GameDataManager.Instance.User.Gold = body.GG;
                        GameDataManager.Instance.User.EnemyUser = null;
                        GameDataManager.Instance.User.Trophy = body.Trophy;
                        GameDataManager.Instance.User.HighTrophy = body.HighTrophy;
                        GameDataManager.Instance.User.currentCharacter.Exp = body.Exp;
                        PopupResult.Instance.Init(body);
                    }
                }else
                {
                    PopupResult.Instance.Init(body);
                }
            }
        }

        #endregion

        #region Reward

        //트로피 보상
        public void RewardTrophy(RewardTrophy rewardTrophy)
        {
            if(rewardTrophy.Index < GameDataManager.Instance.User.TrophyRewardIndex+1)
                GameEventManager.Instance.OnEventToast("이미 받은 보상입니다..");
            else if(rewardTrophy.Index > GameDataManager.Instance.User.TrophyRewardIndex + 1)
                GameEventManager.Instance.OnEventToast("이전 보상을 받아야합니다.");
            else if(rewardTrophy.HighTrophy> GameDataManager.Instance.User.HighTrophy)
                GameEventManager.Instance.OnEventToast("트로피 점수가 부족합니다.");
            else
            {
                dicSave.Clear();
                dicSave.Add("RewardIndex", rewardTrophy.Index);

                var request = new ExecuteCloudScriptRequest()
                {
                    FunctionName = "RewardTrophy",
                    FunctionParameter = dicSave,
                    GeneratePlayStreamEvent = true,
                };
                PlayFabClientAPI.ExecuteCloudScript(request, OnResultRewardTrophy, OnPlayFabError);
                UIManager.Instance.SetServerIndicaator(true);
            }
        }

        void OnResultRewardTrophy(ExecuteCloudScriptResult result)
        {
            UIManager.Instance.SetServerIndicaator(false);
            if (result.Logs.Count > 0)
                GameEventManager.Instance.OnEventToast(result.Logs[0].Message);
            else
            {
                var body = PlayFabSimpleJson.DeserializeObject<RewardTrophyResponseBody>(result.FunctionResult.ToString());
                GameEventManager.Instance.OnEventToast("보상을 받았습니다.");
                if (body != null)
                {
                    switch (GameDataManager.Instance.User.NextRewardTrophy.RewardType)
                    {
                        case RewardTrophyType.Spell:
                            foreach (var spell in body.Spell)
                                GameDataManager.Instance.User.AddUserSpell(spell);
                            break;
                        case RewardTrophyType.Character:
                            GameDataManager.Instance.User.userCharactersDic[body.Character.Index].UnLock(body.Character);
                            break;
                        case RewardTrophyType.Gold:
                            GameDataManager.Instance.User.Gold = body.GG;
                            GameEventManager.Instance.OnEventGoldChanged(body.GG);
                            break;
                    }

                    GameDataManager.Instance.User.TrophyRewardIndex = body.TrophyRewardIndex;
                    GameEventManager.Instance.OnEventRewardTrophy();
                }
            }
        }



        #endregion

        #region Tool

        public void ToolExpUp(UserCharacter userCharacter)
        {
            dicSave.Clear();
            dicSave.Add("CharacterIndex", userCharacter.CharacterIndex);
            var request = new ExecuteCloudScriptRequest()
            {
                FunctionName = "TempExpUp",
                FunctionParameter = dicSave,
                GeneratePlayStreamEvent = true,

            };
            PlayFabClientAPI.ExecuteCloudScript(request, OnResultToolxpUp, OnPlayFabError);
            UIManager.Instance.SetServerIndicaator(true);
        }

        void OnResultToolxpUp(ExecuteCloudScriptResult result)
        {
            UIManager.Instance.SetServerIndicaator(false);
            var body = PlayFabSimpleJson.DeserializeObject<UserCharacterBody>(result.FunctionResult.ToString());
            GameDataManager.Instance.User.userCharactersDic[body.Index].Exp = body.Exp;
            GameEventManager.Instance.OnEventCharacterChanged(GameDataManager.Instance.User.userCharactersDic[body.Index]);
        }

        public void ToolHighTrophyUp()
        {
            var userInfo = new UserInfoBody();
            userInfo.Character = GameDataManager.Instance.User.currentCharacter.CharacterIndex;
            userInfo.HighTrophy = GameDataManager.Instance.User.HighTrophy + 500;
            userInfo.TrophyRewardIndex = GameDataManager.Instance.User.TrophyRewardIndex;
            var dic = new Dictionary<string, string>();
            dic.Add("UserInfo", PlayFabSimpleJson.SerializeObject(userInfo));

            PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
            {
                Data = dic
            }, (result) => {
                GameDataManager.Instance.User.HighTrophy += 500;
                GameEventManager.Instance.OnEventRewardTrophy();
            }, OnPlayFabError);
        }

        #endregion

    }

    [Serializable]
    public class NoticeBody
    {
        public string Body;
        public string ImageUrl;
    }
}
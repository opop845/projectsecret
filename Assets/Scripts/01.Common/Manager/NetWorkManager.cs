﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Secret;
using Photon.Realtime;

public class NetWorkManager : MonoBehaviourPunCallbacks
{
    public override void OnJoinedLobby() => print("로비접속완료");
    public override void OnConnectedToMaster()
    {
        print(PhotonNetwork.LocalPlayer.NickName + "서버접속완료");
        ServerManager.Instance.CreateRoom();
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("매칭방을 만들었습니다.");
    }

    public override void OnConnected()
    {
        Debug.Log("연결.");
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");
        PhotonNetwork.Instantiate("Unit/Unit_100001", Vector3.zero, Quaternion.identity);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log("OnPlayerEnteredRoom :: "+ newPlayer.NickName);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Debug.Log("OnPlayerLeftRoom:: " + otherPlayer.NickName);
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        Debug.Log("OnPlayerPropertiesUpdate:: " + targetPlayer.NickName);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("OnDisconnected:: " + cause);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Secret.Server;
using PlayFab.ClientModels;
using PlayFab.Json;
using Secret.Data;
using System.Linq;

namespace Secret
{
    public class GameDataManager : Singleton<GameDataManager>
    {
        public bool isDirty = false;

        public User User { get; private set; }
        public GameRuleBody GameRule { get; private set; }
        public bool IsInit { get; private set; } = false;

        Dictionary<int, Monster> MonsterDict = new Dictionary<int, Monster>();
        Dictionary<int, Map> MapDict = new Dictionary<int, Map>();
       //Dictionary<int, AIUser> AIUserDic = new Dictionary<int, AIUser>();
        public Dictionary<int, Item> ItemDict = new Dictionary<int, Item>();
        Dictionary<int, Skill> SkillDic = new Dictionary<int, Skill>();
        public IReadOnlyDictionary<int, Character> CharacterDict;
        public IReadOnlyDictionary<int, Spell> SpellDic;
        public IReadOnlyDictionary<int, SpellLevel> SpellLevelDic;
        public IReadOnlyDictionary<int, List<CharacterExpertLevel>> CharacterExpertLevels;
        public IReadOnlyDictionary<int, List<CharacterAbility>> CharacterAbilites;
        public IReadOnlyDictionary<int, RewardTrophy> RewardTrophies;
        public IReadOnlyDictionary<int, BattleBox> BattleBoxes;


        public void InitServerData(GetPlayerCombinedInfoResultPayload data)
        {
            IsInit = true;
            Dictionary<string, string> gameData = ServerManager.Instance.IsLocalData ? CSVReader.LocalDataRead() : data.TitleData;
            AddCharacter(PlayFabSimpleJson.DeserializeObject<CharacterBody[]>(gameData["Character"]));
            AddMap(PlayFabSimpleJson.DeserializeObject<MapBody[]>(gameData["Map"]));
            AddMonster(PlayFabSimpleJson.DeserializeObject<MonsterBody[]>(gameData["Monster"]));
            AddItem(PlayFabSimpleJson.DeserializeObject<ItemBody[]>(gameData["Item"]));
            AddSkill(PlayFabSimpleJson.DeserializeObject<SkillBody[]>(gameData["Skill"]));
            AddSpell(PlayFabSimpleJson.DeserializeObject<SpellBody[]>(gameData["Spell"]));
            AddSpellLevel(PlayFabSimpleJson.DeserializeObject<SpellLevelBody[]>(gameData["SpellLevel"]));
            AddCharacterExpertLevel(PlayFabSimpleJson.DeserializeObject<CharacterExpertLevelBody[]>(gameData["CharacterExpertLevel"]));
            AddCharacterAbility(PlayFabSimpleJson.DeserializeObject<CharacterAbilityBody[]>(gameData["CharacterAbility"]));
            AddRewardTrophy(PlayFabSimpleJson.DeserializeObject<RewardTrophyBody[]>(gameData["RewardTrophy"]));
            AddBattleBoxes(PlayFabSimpleJson.DeserializeObject<BattleBoxBody[]>(gameData["BattleBox"]));

            GameRule = PlayFabSimpleJson.DeserializeObject<GameRuleBody>(gameData["GameRule"]);
            User = new User();
            User.Initialize(data);
            if (User.EnemyUser != null && User.EnemyUser.IsLose)
            {
                ServerManager.Instance.ForceQuitLose();
            }
        }

        public void InitLocalData()
        {
            IsInit = true;
            Dictionary<string, string> localdata = CSVReader.LocalDataRead();
            AddCharacter(PlayFabSimpleJson.DeserializeObject<CharacterBody[]>(localdata["Character"]));
            AddMap(PlayFabSimpleJson.DeserializeObject<MapBody[]>(localdata["Map"]));
            AddMonster(PlayFabSimpleJson.DeserializeObject<MonsterBody[]>(localdata["Monster"]));
            AddItem(PlayFabSimpleJson.DeserializeObject<ItemBody[]>(localdata["Item"]));
            AddSkill(PlayFabSimpleJson.DeserializeObject<SkillBody[]>(localdata["Skill"]));
            AddSpell(PlayFabSimpleJson.DeserializeObject<SpellBody[]>(localdata["Spell"]));
            AddSpellLevel(PlayFabSimpleJson.DeserializeObject<SpellLevelBody[]>(localdata["SpellLevel"]));
            AddCharacterExpertLevel(PlayFabSimpleJson.DeserializeObject<CharacterExpertLevelBody[]>(localdata["CharacterExpertLevel"]));
            AddCharacterAbility(PlayFabSimpleJson.DeserializeObject<CharacterAbilityBody[]>(localdata["CharacterAbility"]));
            AddRewardTrophy(PlayFabSimpleJson.DeserializeObject<RewardTrophyBody[]>(localdata["RewardTrophy"]));
            AddBattleBoxes(PlayFabSimpleJson.DeserializeObject<BattleBoxBody[]>(localdata["BattleBox"]));

            GameRule = PlayFabSimpleJson.DeserializeObject<GameRuleBody>(localdata["GameRule"]);
            User = new User();
            GameEventManager.Instance.OnEventInit();
        }

        void AddCharacter(CharacterBody[] bodies)
        {
            var dic = new Dictionary<int, Character>();
            foreach (var body in bodies)
            {
                if (body.SortOrder >= 0)
                    dic.Add(body.Index, new Character(body));
            }

            CharacterDict = dic;
        }

        void AddMap(MapBody[] bodies)
        {
            MapDict.Clear();
            foreach (var body in bodies)
                MapDict.Add(body.Index, new Map(body));
        }

        void AddMonster(MonsterBody[] bodies)
        {
            MonsterDict.Clear();
            foreach (var body in bodies)
                MonsterDict.Add(body.Index, new Monster(body));
        }

        void AddItem(ItemBody[] bodies)
        {
            ItemDict.Clear();
            foreach (var body in bodies)
            {
                Item item;
                switch (body.Type)
                {
                    case "Get":
                        item = new GetItem(body);
                        break;
                    case "Buff":
                        item = new BuffItem(body);
                        break;
                    case "Equipment":
                        item = new EquipmentItem(body);
                        break;
                    case "Damage":
                        item = new DamageItem(body);
                        break;
                    default:
                        item = new Item(body);
                        break;
                }
                ItemDict.Add(body.Index, item);
            }
        }

        void AddSkill(SkillBody[] bodies)
        {
            SkillDic.Clear();
            foreach (var body in bodies)
            {
                switch (body.Type)
                {
                    case "Buff":
                    case "Debuff":
                        SkillDic.Add(body.Index, new BuffSkill(body));
                        break;
                    case "Heal":
                        SkillDic.Add(body.Index, new HealSkill(body));
                        break;
                    case "Damage":
                        SkillDic.Add(body.Index, new DamageSkill(body));
                        break;
                    case "Special":
                        SkillDic.Add(body.Index, new SpecialSkill(body));
                        break;
                    default:
                        SkillDic.Add(body.Index, new Skill(body));
                        break;
                }
            }
        }

        void AddSpell(SpellBody[] bodies)
        {
            var dic = new Dictionary<int, Spell>();
            foreach (var body in bodies)
                dic.Add(body.Index, new Spell(body));
            SpellDic = dic;
        }

        void AddSpellLevel(SpellLevelBody[] bodies)
        {
            var dic = new Dictionary<int, SpellLevel>();
            foreach (var body in bodies)
                dic.Add(body.Index, new SpellLevel(body));
            SpellLevelDic = dic;
        }

        void AddCharacterExpertLevel(CharacterExpertLevelBody[] bodies)
        {
            var dic = new Dictionary<int, List<CharacterExpertLevel>>();
            foreach (var body in bodies)
            {
                if (!dic.ContainsKey(body.CharacterIndex))
                    dic.Add(body.CharacterIndex, new List<CharacterExpertLevel>());

                dic[body.CharacterIndex].Add(new CharacterExpertLevel(body));
            }
            foreach (var item in dic)
            {
                item.Value.Sort((x, y) => x.Level.CompareTo(y.Level));
            }
            CharacterExpertLevels = dic;
        }

        void AddCharacterAbility(CharacterAbilityBody[] bodies)
        {
            var dic = new Dictionary<int, List<CharacterAbility>>();
            foreach (var body in bodies)
            {
                if (!dic.ContainsKey(body.CharacterIndex))
                    dic.Add(body.CharacterIndex, new List<CharacterAbility>());

                dic[body.CharacterIndex].Add(new CharacterAbility(body));
            }

            foreach (var item in dic)
            {
                item.Value.Sort((x, y) => x.AbilityIndex.CompareTo(y.AbilityIndex));
            }

            CharacterAbilites = dic;
        }

        void AddRewardTrophy(RewardTrophyBody[] bodies)
        {
            var dic = new Dictionary<int, RewardTrophy>();
            foreach (var body in bodies)
                dic.Add(body.Index, new RewardTrophy(body));
            RewardTrophies = dic;
        }

        void AddBattleBoxes(BattleBoxBody[] bodies)
        {
            var dic = new Dictionary<int, BattleBox>();
            foreach (var body in bodies)
                dic.Add(body.Index, new BattleBox(body));
            BattleBoxes = dic;
        }

        #region GetValue

        public Spell GetSpell(SpellType type, int index)
        {
            return SpellDic.Values.First(x => x.SpellType == type && x.SpellSubIndex == index);
        }

        public Spell GetSpell(int index)
        {
            if (SpellDic.TryGetValue(index, out var value))
                return value;
            else
                return null;
        }

        public Item GetItem(int index)
        {
            if (ItemDict.TryGetValue(index, out var value))
                return value;

            Debug.LogError($"Item Index : {index} Not Found");
            return null;
        }

        public Skill GetSkill(int index)
        {
            if (SkillDic.TryGetValue(index, out var value))
                return value;

            Debug.LogError($"Item Index : {index} Not Found");
            return null;
        }

        public Item GetMergeItem(Item item1, Item item2)
        {
            if (item1 == null || item2 == null) return null;
            return ItemDict.Values.FirstOrDefault(x => x.CheckMerge(item1.Index, item2.Index));
        }

        public Character GetCharacter(int index)
        {
            if (CharacterDict.TryGetValue(index, out var value))
                return value;

            return null;
        }

        public Monster GetMonster(int index)
        {
            if (MonsterDict.TryGetValue(index, out var value))
                return value;

            Debug.LogError($"Monster Index : {index} Not Found");
            return null;
        }

        public List<Map> GetMapList()
        {
            return MapDict.Values.OrderBy(x => x.Index).ToList();
        }

        public float GetTimeToSoul(float time)
        {
            for (int i = GameRule.BattleBaseSoulPerSecondsTimeSection.Length-1; i >=0 ; i--)
            {
                if (time >= GameRule.BattleBaseSoulPerSecondsTimeSection[i])
                    return GameRule.BattleBaseSoulPerSeconds[i];
            }
            return GameRule.BattleBaseSoulPerSeconds[0];
        }

        public SpellLevel GetSpellLevel(int level)
        {
            if (SpellLevelDic.TryGetValue(level, out var value))
                return value;
            else
                return null;
        }

        public CharacterExpertLevel GetCharacterExpertLevel(int characterIndex,int level)
        {
            if (CharacterExpertLevels.TryGetValue(characterIndex, out var value))
                return value.Find(x => x.Level == level);
            else
                return null;
        }

        public CharacterAbility GetCharacterAbility(int characterIndex, int abilityIndex)
        {
            if(CharacterAbilites.TryGetValue(characterIndex,out var value))
            {
                var ability = value.Find(x => x.AbilityIndex == abilityIndex);
                if (ability != null)
                    return ability;
            }
            return null;
        }

        #endregion

        #region Battle

        public List<Map> MapList;
        public BattleType BattleType;

        public void SetBattleAI()
        {
            BattleType = BattleType.AI;
            MapList = GetMapList();
        }

        #endregion
    }
}

public enum BattleType
{
    PVP,
    AI
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.U2D;

public class ResourceManager : Singleton<ResourceManager>
{
    Dictionary<string, WeakReference> trey = new Dictionary<string, WeakReference>();

    //---------------------------------------------------------------------------------------------------
    public ResourceManager()
    {
    }

    //---------------------------------------------------------------------------------------------------
    public T LoadAsset<T>(string strPath) where T : UnityEngine.Object
    {
        if (trey.ContainsKey(strPath))
        {
            if (trey[strPath].IsAlive && trey[strPath].Target.GetType().Equals(typeof(T)))
            {
                return trey[strPath].Target as T;
            }
            else
            {
                var _dat = Resources.Load<T>(strPath);
                trey[strPath] = new WeakReference(_dat);
                return _dat;
            }
        }
        else
        {
            var _dat = Resources.Load<T>(strPath);
            trey.Add(strPath, new WeakReference(_dat));
            return _dat;
        }
    }

    //---------------------------------------------------------------------------------------------------
    public void Clear()
    {
        trey.Where(item => item.Value.IsAlive).ToDictionary(i => i.Key);
    }

    SpriteAtlas[] atlas;
    public Sprite LoadSprtie(string iconKey)
    {
        if (atlas == null) atlas = Resources.LoadAll<SpriteAtlas>("Atlas");

        foreach (var item in atlas)
        {
            var icon = item.GetSprite(iconKey);
            if (icon != null) return icon;
        }

        return null;
    }
}

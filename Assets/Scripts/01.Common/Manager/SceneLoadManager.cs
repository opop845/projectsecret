﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Secret
{
    public class SceneLoadManager : MonoSingleton<SceneLoadManager>
    {
        bool m_bIsDone = false;
        AsyncOperation async_operation;
        Scene GameScene;
        Scene UIScene;

        void Awake()
        {

            if (Instance != this)
            {
                Destroy(gameObject);  // 임시로 일단 이렇게 고쳐둠 (Sanghun)
                return;
            }
            else
                DontDestroyOnLoad(gameObject);
            //  Screen.SetResolution(Screen.width, Screen.width * 16 / 9, true);
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            Application.targetFrameRate = 60;
            //Camera camera = Camera.main;
            //Rect rect = camera.rect;
            //float scaleheight = ((float)Screen.width / Screen.height) / ((float)9 / 16); // (가로 / 세로)
            //float scalewidth = 1f / scaleheight;
            //if (scaleheight < 1)
            //{
            //    rect.height = scaleheight;
            //    rect.y = (1f - scaleheight) / 2f;
            //}
            //else
            //{
            //    rect.width = scalewidth;
            //    rect.x = (1f - scalewidth) / 2f;
            //}
            //camera.rect = rect;
        }

        public void IntroLoad()
        {
            StartCoroutine("StartIntroLoad");
        }

        public void LoadFrist()
        {
        }

        public void LoadScene(string strMap)
        {
        }

        public void LoadLobby()
        {
            Time.timeScale = 1f;
            StartCoroutine(SceneLoadCompeleted());
            IEnumerator SceneLoadCompeleted()
            {
                UIManager.Instance.OnEventSceneChanged();
                yield return SceneManager.LoadSceneAsync("Lobby");
                GameEventManager.Instance.OnEventInit();
            }
        }

        public void LoadBattle()
        {
            UIManager.Instance.OnEventSceneChanged();
            SceneManager.LoadScene("Battle");
            SceneManager.LoadScene("00_Default", LoadSceneMode.Additive);
        }

        //---------------------------------------------------------------------------------------------------
        public void ResetScene()
        {
            SceneManager.LoadScene("Intro");
        }

        public string CurrentSceneName => SceneManager.GetActiveScene().name;

        public void LoadIntro()
        {
            StartCoroutine(CoIntro());
            IEnumerator CoIntro()
            {
                AsyncOperation asyncOperation;
                asyncOperation = SceneManager.LoadSceneAsync("Lobby", LoadSceneMode.Additive);
                yield return asyncOperation;
                yield return new WaitForSeconds(3f);
                asyncOperation = SceneManager.UnloadSceneAsync("Intro");
                yield return asyncOperation;
            }
        }


        //---------------------------------------------------------------------------------------------------
        //IEnumerator LoadSceneMainUICoroutine(string gameSceneName, string uiSceneName)
        //{
        //    yield return LoadLoadingFade(true);

        //    AsyncOperation asyncOperation;
        //    AsyncOperation asyncOperationDelete;

        //    if (!UIScene.name.Equals(uiSceneName))
        //    {
        //        asyncOperationDelete = SceneManager.UnloadSceneAsync(UIScene);
        //        yield return asyncOperationDelete;

        //        asyncOperation = SceneManager.LoadSceneAsync(uiSceneName, LoadSceneMode.Additive);
        //        yield return asyncOperation;

        //        UIScene = SceneManager.GetSceneByName(uiSceneName);
        //    }

        //    //if(!GameScene.name.Equals(gameSceneName))
        //    {
        //        asyncOperationDelete = SceneManager.UnloadSceneAsync(GameScene);
        //        yield return asyncOperationDelete;

        //        asyncOperation = SceneManager.LoadSceneAsync(gameSceneName, LoadSceneMode.Additive);
        //        yield return asyncOperation;

        //        GameScene = SceneManager.GetSceneByName(gameSceneName);
        //        SceneManager.SetActiveScene(GameScene);
        //    }

        //    GC.Collect();
        //    yield return new WaitForSecondsRealtime(1f);

        //    if (GameScene.name.Equals("Home"))
        //        GameUIEventManager.Instance.OnEventHome();
        //    else
        //        GameUIEventManager.Instance.OnEventMapChange();


        //    yield return LoadLoadingFade(false);
        //}

        ////---------------------------------------------------------------------------------------------------
        //IEnumerator LoadLoadingFade(bool isOn)
        //{
        //    if (isOn)
        //    {
        //        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("Loading", LoadSceneMode.Additive);
        //        yield return asyncOperation;
        //        GameUIEventManager.Instance.OnEventFade(true);
        //        yield return new WaitForSecondsRealtime(1f);
        //    }
        //    else
        //    {
        //        GameUIEventManager.Instance.OnEventFade(false);
        //        yield return new WaitForSecondsRealtime(1f);

        //        AsyncOperation asyncOperation = SceneManager.UnloadSceneAsync("Loading");
        //        yield return asyncOperation;
        //    }
        //}

        ////---------------------------------------------------------------------------------------------------
        //IEnumerator LoadSceneFirst()
        //{
        //    yield return LoadLoadingFade(true);

        //    AsyncOperation asyncOperation = SceneManager.UnloadSceneAsync("Intro");
        //    yield return asyncOperation;

        //    asyncOperation = SceneManager.LoadSceneAsync("MainUI", LoadSceneMode.Additive);
        //    yield return asyncOperation;
        //    UIScene = SceneManager.GetSceneByName("MainUI");

        //    asyncOperation = SceneManager.LoadSceneAsync("Home", LoadSceneMode.Additive);
        //    yield return asyncOperation;
        //    SceneManager.SetActiveScene(SceneManager.GetSceneByName("Home"));
        //    GameScene = SceneManager.GetActiveScene();

        //    yield return new WaitForSecondsRealtime(1f);

        //    yield return LoadLoadingFade(false);
        //}
    }
}
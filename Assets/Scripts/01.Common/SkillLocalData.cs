﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Secret.Data
{
    public class SkillLocalData : ScriptableObject
    {
        [SerializeField] int intdex;
        [SerializeField] string animationTrigger;
        [SerializeField] GameObject casterEffect;
        [SerializeField] GameObject targetEffect;

        public int Index { get => Index; }
        public string AnimationTrigger { get => animationTrigger; }
        public GameObject TargetEffect1 { get => targetEffect;  }
        public GameObject CasterEffect { get => casterEffect;  }
    }
}
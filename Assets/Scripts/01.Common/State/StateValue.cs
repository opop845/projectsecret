﻿using I2.Loc;
using UnityEngine;

public enum StatModType
{
    Flat = 1,
    PercentAdd = 2,
    PercentMult = 3,
}

public class StatValue
{
    public readonly StatModType Type;
    public readonly int Order;
    public readonly object Source;
    public readonly AttrType AttrType;
    public readonly LocalizedString Name;
    public float Value { get; set; }

    public StatValue(StatValue stat)
    {
        Value = stat.Value;
        AttrType = stat.AttrType;
        Type = stat.Type;
        Order = stat.Order;
        Source = this;
        //Name = $"{GameDataManager.NameFlag}attr_{(int)AttrType}";
    }

    public StatValue(float value, AttrType attrType, StatModType type, int order, object source)
    {
        Value = value;
        AttrType = attrType;
        Type = type;
        Order = order;
        Source = source;
       // Name = $"{GameDataManager.NameFlag}attr_{(int)attrType}";
    }

    public StatValue(float value, AttrType attrType, StatModType type) : this(value, attrType, type, (int)type, null) { }

    public StatValue(float value, AttrType attrType, StatModType type, int order) : this(value, attrType, type, order, null) { }

    public StatValue(float value, AttrType attrType, StatModType type, object source) : this(value, attrType, type, (int)type, source) { }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public enum AttrType
{
    None = 0,
    Attack = 1,
    AttackSpeed,
    Hp,
    MoveSpeed,
    CriticalChance,
    CriticalDamage,
    HpRestorationRate,
    SuckHp,
    Soul,
}

public class CharacterStatTable
{
    Dictionary<AttrType, CharacterStat> StatTable = new Dictionary<AttrType, CharacterStat>();

    public CharacterStatTable()
    {
        foreach (AttrType attr in Enum.GetValues(typeof(AttrType)))
        {
            if (attr == AttrType.None) continue;
            StatTable.Add(attr, new CharacterStat(0f));
        }
    }

    public CharacterStatTable(CharacterStatTable stat)
    {
        foreach (var state in stat.StatTable)
        {
            if (state.Key == AttrType.None) continue;
            var characterStat = new CharacterStat(state.Value.BaseValue);

            foreach (var value in state.Value.StatModifiers)
            {
                characterStat.AddModifier(new StatValue(value));
            }
            StatTable.Add(state.Key, characterStat);
        }
    }

    public void SetBaseValue(AttrType attrType, float value)
    {
        if (attrType == AttrType.None) return;
        StatTable[attrType].BaseValue = value;
    }

    public void AddStat(AttrType attrType, StatModType statModType, float value, object obj)
    {
        if (attrType == AttrType.None) return;
        StatTable[attrType].AddModifier(new StatValue(value, attrType, statModType, obj));
    }

    public void AddStat(StatValue statModifier)
    {
        if (statModifier.AttrType == AttrType.None) return;
        StatTable[statModifier.AttrType].AddModifier(statModifier);
    }

    public void RemoveStat(AttrType attrType, object obj)
    {
        if (attrType == AttrType.None) return;
        StatTable[attrType].RemoveAllModifiersFromSource(obj);
    }

    public void RemoveStat(object obj)
    {
        foreach (var stat in StatTable)
        {
            stat.Value.RemoveAllModifiersFromSource(obj);
        }
    }

    public void RemoveAll()
    {
        foreach (var stat in StatTable)
        {
            stat.Value.RemoveAll();
        }
    }

    public void SetStatDirty(AttrType type)
    {
        if (StatTable.ContainsKey(type))
            StatTable[type].isDirty = true;
    }

    public int BattlePower
    {
        get
        {
            return Mathf.RoundToInt
                (GetStatValue(AttrType.Attack)
                * GetStatValue(AttrType.AttackSpeed)
                * (1 + GetStatValue(AttrType.CriticalChance) * GetStatValue(AttrType.CriticalDamage))
                * GetStatValue(AttrType.Hp) / 42.7f
                * (1 + GetStatValue(AttrType.HpRestorationRate) * 10));
        }
    }

    public float GetStatValue(AttrType attrType)
    {
        if (attrType == AttrType.None) return 0f;
        else return StatTable[attrType].Value;
    }
}

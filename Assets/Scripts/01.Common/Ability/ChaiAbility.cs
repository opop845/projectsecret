﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Secret
{
    public class ChaiAbility : UniqueAbility
    {
        private float time = 0f;
        private bool isApply = false;
        private StatValue statValue;
        private int count = 0;

        protected override void Update()
        {
            base.Update();
            if (isApply && time <= 0f)
            {
                isApply = false;
                count = 0;
                time = 0f;
                statValue.Value = 0f;
                characterObject.Stat.SetStatDirty(statValue.AttrType);
                BattleEventManager.Instance.OnEventStatUpdate(characterObject, characterObject.Stat.BattlePower);
            }
            else if (isApply)
            {
                time -= Time.deltaTime;
            }
        }

        public override void Init(CharacterObject character, int level)
        {
            base.Init(character, level);
            characterObject.EventAttack += OnEventAttack;

            statValue = new StatValue(0f, AttrType.AttackSpeed, StatModType.PercentAdd, this);
            characterObject.Stat.AddStat(statValue);
        }

        public override void Relese()
        {
            characterObject.EventAttack -= OnEventAttack;
        }

        void OnEventAttack(DefaultObject obj)
        {
            isApply = true;
            time = 5f;

            if (count < 5)
            {
                count++;
                statValue.Value = 0.1f * count;
                characterObject.Stat.SetStatDirty(statValue.AttrType);
                BattleEventManager.Instance.OnEventStatUpdate(characterObject, characterObject.Stat.BattlePower);
            }
        }
    }
}
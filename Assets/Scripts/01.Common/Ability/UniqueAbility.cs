﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret
{
    public abstract class UniqueAbility: MonoBehaviour
    {
        protected CharacterObject characterObject;
        protected int abilityLevel;

        protected virtual void Update()
        {

        }

        public virtual void Init(CharacterObject character, int level)
        {
            characterObject = character;
            abilityLevel = level;
        }

        public abstract void Relese();
    }

}
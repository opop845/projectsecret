﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret
{
    public class DrakeAbility : UniqueAbility
    {
        private int count = 0;

        public override void Init(CharacterObject character, int level)
        {
            base.Init(character, level);
            characterObject.EventAttack += OnEventAttack;
        }


        public override void Relese()
        {
            characterObject.EventAttack -= OnEventAttack;
        }

        void OnEventAttack(DefaultObject obj)
        {
            count++;
            if(count>4)
            {
                if (obj.target != null)
                {
                    obj.target.SetPerDamage(obj, 0.05f, PerDamageType.MaxHp, false);
                    count = 0;
                }
            }
        }
    }
}
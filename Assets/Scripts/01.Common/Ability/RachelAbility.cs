﻿using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret
{
    public class RachelAbility : UniqueAbility
    {
        private Spell apple;
        public override void Init(CharacterObject character, int level)
        {
            base.Init(character, level);
            characterObject.EventKill += OnEventKill;
            apple = GameDataManager.Instance.GetSpell(500001);
            characterObject.KillHeal += 0.1f;
        }

        public override void Relese()
        {
            characterObject.EventKill -= OnEventKill;
        }

        void OnEventKill(DefaultObject attacker,DefaultObject target)
        {
            characterObject.PurchaseItem(apple.Item);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Storage;

namespace Secret.Data
{
    [CreateAssetMenu(menuName = "Skill/AttackSkill")]
    public class AttackSkillData : SkillLocalData
    {
        [SerializeField] ObscuredInt attackCount = 1;
        [SerializeField] ObscuredFloat attackTime;

        public int AttackCount { get => attackCount; }
        public float AttackTime { get => attackTime; }
    }
}
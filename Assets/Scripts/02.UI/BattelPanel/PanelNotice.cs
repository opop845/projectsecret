﻿using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Secret
{
    public class PanelNotice : MonoBehaviour
    {
        [SerializeField] PrefabItemInfo prefabItemInfo;
        [SerializeField] PrefabEnemyInfo prefabEnemyInfo;
        [SerializeField] SkillCutScene skillCutScene;
        [SerializeField] Slider bossGauge;

        public void Awake()
        {
            GameEventManager.Instance.EventItemInfo += OnEventItemInfo;
            GameEventManager.Instance.EventSpellInfo += OnEventSpellInfo;
            GameEventManager.Instance.EventItemUse += OnEventItemUse;
            GameEventManager.Instance.EventItemPurchase += OnEventItemPurchase;
            GameEventManager.Instance.EventItemMerge += OnEventItemMerge;
            GameEventManager.Instance.EventBossHpGauge += OnEventBossHpGauge;
            GameEventManager.Instance.EventKillNotice += OnEventKillNotice;
            GameEventManager.Instance.EventUseSkill += OnEventUseSkill;
        }
        public void OnDestroy()
        {
            GameEventManager.Instance.EventItemInfo -= OnEventItemInfo;
            GameEventManager.Instance.EventSpellInfo -= OnEventSpellInfo;
            GameEventManager.Instance.EventItemUse -= OnEventItemUse;
            GameEventManager.Instance.EventItemPurchase -= OnEventItemPurchase;
            GameEventManager.Instance.EventItemMerge -= OnEventItemMerge;
            GameEventManager.Instance.EventBossHpGauge -= OnEventBossHpGauge;
            GameEventManager.Instance.EventKillNotice -= OnEventKillNotice;
            GameEventManager.Instance.EventUseSkill -= OnEventUseSkill;
        }

        #region EventHandler

        private void OnEventItemInfo(Item item, bool isPress)
        {
            prefabItemInfo.OnItemInfo(item, isPress);
        }
        
        private void OnEventSpellInfo(Spell spell, bool isPress)
        {
            prefabItemInfo.OnSpellInfo(spell, isPress);
        }

        private void OnEventItemMerge(CharacterObject obj, BattleItem battleItem)
        {
            //if (obj.ObjectType == ObjectType.Enemy)
            //    prefabEnemyInfo.DisplayMerge(battleItem.Item);
        }

        private void OnEventItemPurchase(CharacterObject obj, Item item)
        {
            if (obj.ObjectType == ObjectType.Enemy)
                prefabEnemyInfo.DisplayPurchase(item);
        }

        private void OnEventItemUse(CharacterObject obj, Item item)
        {
            if (obj.ObjectType == ObjectType.Enemy)
                prefabEnemyInfo.DisplayUse(item);
        }

        private void OnEventBossHpGauge(float value, bool isOn)
        {
            bossGauge.SetActive(isOn);
            bossGauge.value = value;
        }

        private void OnEventKillNotice(DefaultObject attacker, DefaultObject deadObject)
        {
            //if (attacker.ObjectType != ObjectType.Player && deadObject.ObjectType != ObjectType.Player)
            //    prefabEnemyInfo.DisplayKill(attacker.name, deadObject.name);
        }

        private void OnEventUseSkill(Skill skill)
        {
            skillCutScene.SetSkillCut(skill);
        }


        #endregion

    }
}
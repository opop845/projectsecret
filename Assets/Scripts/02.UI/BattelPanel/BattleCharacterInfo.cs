﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Secret.Data;
using UnityEngine.UI;
using TMPro;

namespace Secret
{
    public class BattleCharacterInfo : MonoBehaviour
    {
        private CharacterObject character;
        public TextMeshProUGUI Gold, Hp, AttackDamage, AttackSpeed, MovementSpeed, CriticalChance, CriticalDamage, HpRestorationRate;
        [SerializeField] Image[] ImageItems;
        [SerializeField] TextMeshProUGUI[] TextItemCount;

        private void Awake()
        {
            GameEventManager.Instance.EventItemUse += OnEventItemUse;
            GameEventManager.Instance.EventItemPurchase += OnEventItemPurchase;
            GameEventManager.Instance.EventItemMerge += OnEventItemMerge;
            GameEventManager.Instance.EventInventoryRefresh += OnEventInventoryRefresh;
            BattleEventManager.Instance.EventStatUpdate += OnEventStatUpdate;
            OnDisplay();
        }

        private void OnDestroy()
        {
            GameEventManager.Instance.EventItemUse -= OnEventItemUse;
            GameEventManager.Instance.EventItemPurchase -= OnEventItemPurchase;
            GameEventManager.Instance.EventItemMerge -= OnEventItemMerge;
            GameEventManager.Instance.EventInventoryRefresh -= OnEventInventoryRefresh;
            BattleEventManager.Instance.EventStatUpdate -= OnEventStatUpdate;
        }

        private void Update()
        {
            if (character != null)
            {
                Gold.SetText(character.Soul);
                Hp.SetText($"{character.Hp}/{character.Stat.GetStatValue(AttrType.Hp)}");
            }

            OnDisplay();  // Sanghun, Update가 아직 잘안되서 일단 걍 여기 박아버림
        }

        public void Init(CharacterObject characterObject)
        {
            character = characterObject;
            OnDisplay();
        }

        #region EventHandler

        private void OnEventItemUse(CharacterObject obj, Item item)
        {
            if (obj == character)
                OnDisplay();
        }

        private void OnEventInventoryRefresh(CharacterObject obj)
        {
            if (obj == character)
                OnDisplay();
        }

        private void OnEventItemMerge(CharacterObject obj, BattleItem item)
        {
            if (obj == character)
                OnDisplay();
        }

        private void OnEventItemPurchase(CharacterObject obj, Item item)
        {
            if (obj == character)
                OnDisplay();
        }

        private void OnEventStatUpdate(DefaultObject obj, int battlePower)
        {
            if (obj == character)
                OnDisplay();
        }

        #endregion

        void OnDisplay()
        {
            if (character != null)
            {
                Gold.SetText((int)character.Soul);
                Hp.SetText($"{character.Hp}/{character.Stat.GetStatValue(AttrType.Hp)}");
                AttackDamage.SetText($"{character.Stat.GetStatValue(AttrType.Attack):#,##0}");
                AttackSpeed.SetText($"{character.Stat.GetStatValue(AttrType.AttackSpeed):0.00}");
                MovementSpeed.SetText($"{character.Stat.GetStatValue(AttrType.MoveSpeed):0.00}");
                CriticalChance.SetText($"{character.Stat.GetStatValue(AttrType.CriticalChance):0.00}");
                CriticalDamage.SetText($"{character.Stat.GetStatValue(AttrType.CriticalDamage):0.00}");
                HpRestorationRate.SetText($"{character.Stat.GetStatValue(AttrType.HpRestorationRate):0.00}");

                for (int i = 0; i < character.BattleInventory.battleItems.Length; i++)
                {
                    if (character.BattleInventory.battleItems[i].Item != null)
                    {
                        ImageItems[i].SetSprite(character.BattleInventory.battleItems[i].Item.IconSprite);
                        ImageItems[i].SetActive(true);

                        if (character.BattleInventory.battleItems[i].Item.ItemType == Data.ItemType.Equipment)
                        {
                            if (character.BattleInventory.battleItems[i].Level > 0)
                                TextItemCount[i].SetText($"+{character.BattleInventory.battleItems[i].Level}");
                            else
                                TextItemCount[i].SetText("");
                        }
                        else
                            TextItemCount[i].SetText(character.BattleInventory.battleItems[i].Count);
                    }
                    else
                    {
                        ImageItems[i].SetActive(false);
                        TextItemCount[i].SetText("");
                    }
                }
            }
        }
    }
}
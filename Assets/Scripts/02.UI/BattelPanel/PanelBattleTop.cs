﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Secret.UI
{
    public class PanelBattleTop : MonoBehaviour
    {
        [Header("[Player]")]
        [SerializeField] Image imagePlayerIcon;
        [SerializeField] TextMeshProUGUI textPlayerBP;
        [SerializeField] DOTweenAnimation tweenPlayerBP;

        [Header("[Enemy]")]
        [SerializeField] Image imageEnemyIcon;
        [SerializeField] TextMeshProUGUI textEnemyBP;
        [SerializeField] DOTweenAnimation tweenEnemyBP;

        private Color originalBPColor;
        private int oldPlayerBP;
        private int oldEnemyBP;

        private void Awake()
        {
            BattleEventManager.Instance.EventGameState += OnEventGameState;
            BattleEventManager.Instance.EventStatUpdate += OnEventStatUpdate;

            originalBPColor = textPlayerBP.color;
        }

        private void OnDestroy()
        {
            BattleEventManager.Instance.EventGameState -= OnEventGameState;
            BattleEventManager.Instance.EventStatUpdate -= OnEventStatUpdate;
        }

        #region EventHandler

        private void OnEventGameState(GameState state)
        {
            if(state == GameState.Init)
            {
                var player = GameManager.Instance.PlayerObject;
                var enemy = GameManager.Instance.EnemyObject;

                imagePlayerIcon.SetSprite(ResourceManager.Instance.LoadSprtie($"CharacterIcon_{player.Character.Index}"));
                oldPlayerBP = player.Stat.BattlePower;
                textPlayerBP.SetText($"{oldPlayerBP:#,##0}");

                imageEnemyIcon.SetSprite(ResourceManager.Instance.LoadSprtie($"CharacterIcon_{enemy.Character.Index}"));
                oldEnemyBP = enemy.Stat.BattlePower;
                textEnemyBP.SetText($"{oldEnemyBP:#,##0}");
            }
        }

        void OnEventStatUpdate(DefaultObject obj, int battlePower)
        {
            if (obj.ObjectType == ObjectType.Player)
            {
                if (oldPlayerBP == battlePower) return;
                tweenPlayerBP.DORestart();
                textPlayerBP.color = oldPlayerBP < battlePower ? Color.green : Color.red;
                textPlayerBP.SetText($"{battlePower:#,##0}");
                textPlayerBP.DOColor(originalBPColor, 0.3f);
                oldPlayerBP = battlePower;
            }
            else
            {
                if (oldEnemyBP == battlePower) return;
                tweenEnemyBP.DORestart();
                textEnemyBP.color = oldEnemyBP < battlePower ? Color.green : Color.red;
                textEnemyBP.SetText($"{battlePower:#,##0}");
                textEnemyBP.DOColor(originalBPColor, 0.3f);
                oldEnemyBP = battlePower;
            }
        }

        #endregion
    }
}
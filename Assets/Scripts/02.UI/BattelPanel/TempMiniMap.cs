﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Secret
{
    public class TempMiniMap : MonoBehaviour
    {
        public GameObject PlayerIcon;
        public GameObject EnemyIcon;
        public GameObject[] PlayerTowerObject, EnemyTowerObject;
        public GameObject[] Chest;
        public RectTransform Link;

        float MinimapLength;
        Vector3 oriPlayerPos;
        Vector3 oriEnemyPos;

        private void Start()
        {
            for (int i = 0; i < 3; i++)
            {
                PlayerTowerObject[i].SetActive(true);
                EnemyTowerObject[i].SetActive(true);
            }

            MinimapLength = Link.rect.width;
            oriPlayerPos = PlayerIcon.transform.localPosition;
            oriEnemyPos = EnemyIcon.transform.localPosition;
        }


        private void Update()
        {
            float mapDistance = GameManager.Instance.MapDistance;
            float PlayerObjectPos = GameManager.Instance.PlayerObject.transform.position.z;
            float EnemyObjectPos = GameManager.Instance.EnemyObject.transform.position.z;
            float PlayerStartPos = GameManager.Instance.PlayerStartPos;
            float EnemyStartPos = GameManager.Instance.EnemyStartPos;

            PlayerIcon.transform.localPosition = new Vector3(oriPlayerPos.x + MinimapLength*(PlayerObjectPos - PlayerStartPos)/mapDistance, oriPlayerPos.y, oriPlayerPos.z);
            EnemyIcon.transform.localPosition = new Vector3(oriPlayerPos.x + MinimapLength*(EnemyObjectPos - PlayerStartPos)/mapDistance, oriEnemyPos.y, oriEnemyPos.z);

            if (GameManager.Instance.PlayerObject.TowerDestoryPoint > 0)
                EnemyTowerObject[(EnemyTowerObject.Length) - GameManager.Instance.PlayerObject.TowerDestoryPoint].SetActive(false);

            if (GameManager.Instance.EnemyObject.TowerDestoryPoint > 0)
                PlayerTowerObject[(PlayerTowerObject.Length) - GameManager.Instance.EnemyObject.TowerDestoryPoint].SetActive(false);

        }
    }
}
﻿using Coffee.UIEffects;
using DG.Tweening;
using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net.Configuration;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Secret
{
    public class ShopItemSlot : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField, GetComponentInChildrenName("ImageIcon")] Image ImageIcon;
        [SerializeField, GetComponentInChildrenName("TextName")] TextMeshProUGUI TextName;
        [SerializeField, GetComponentInChildrenName("TextPrice")] TextMeshProUGUI TextPrice;
        [SerializeField, GetComponentInChildrenName("Price")] GameObject Price; //Sanghun
        [SerializeField, GetComponentInChildrenName("IconBackground")] GameObject IconBackground; //Sanghun
        [SerializeField, GetComponentInChildrenName("SkillIndicator")] GameObject SkillIndicator; //Sanghun
        [SerializeField, GetComponentInChildrenName("ItemBackground")] GameObject ItemBackground;
        [SerializeField, GetComponentInChildrenName("CoolTimeBG")] Image CoolTimeBG;
        [SerializeField, GetComponentInChildrenName("ImageUseSkill")] GameObject ImageUseSkill;
        [SerializeField, GetComponentInChildrenName("SlotBack")] Image SlotBack;
        [SerializeField, GetComponent] DOTweenAnimation tween;
        [SerializeField] DOTweenAnimation tweenScale;

        [SerializeField, GetComponentInChildrenOnly] UIEffect[] grayEffect;
        //[SerializeField, GetComponent] Button ButtonPurchase;

        Vector2 SetSkilIconSize;
        Vector3 SetIconScale;


        private Spell shopSpell;
        public int Index;
        public bool isPurchase { get; private set; } = false;

        private void Awake()
        {
            // ButtonPurchase.OnClick(OnClickPurchase);
            GameEventManager.Instance.EventItemUse += UpdateDisplay;
            GameEventManager.Instance.EventBattleItemUpdate += UpdateDisplay;
            GameEventManager.Instance.EventBattleGoldUpdate += OnEventBattleGoldUpdate;

            SetSkilIconSize = IconBackground.GetComponent<RectTransform>().sizeDelta;
            SetIconScale = ImageIcon.transform.localScale;

        }

        private void OnDestroy()
        {
            GameEventManager.Instance.EventItemUse -= UpdateDisplay;
            GameEventManager.Instance.EventBattleItemUpdate -= UpdateDisplay;
            GameEventManager.Instance.EventBattleGoldUpdate -= OnEventBattleGoldUpdate;
        }

        public void Init(Spell item)
        {
            shopSpell = item;
            isPurchase = false;
            ImageUseSkill.SetActive(false);
            SlotBack.SetActive(false);
            Price.SetActive(true);          
            ItemBackground.SetActive(true);
            if (shopSpell.SpellType == SpellType.Item)
            {
                IconBackground.SetActive(true);
                SkillIndicator.SetActive(false); // Sanghun
                ImageIcon.SetSprite(shopSpell.Item.IconSprite);
                ImageIcon.transform.localScale = SetIconScale; // Sanghun
                ImageIcon.transform.localPosition = new Vector3(0, 30.0f, 0);
                //ImageIcon.SetNativeSize(); // Sanghun
                TextName.SetText(shopSpell.Item.Name);
            }
            else
            {
                IconBackground.SetActive(false);
                SkillIndicator.SetActive(true); // Sanghun
                ImageIcon.SetSprite(shopSpell.Skill.IconSprite);
                ImageIcon.rectTransform.sizeDelta = SetSkilIconSize; // Sanghun
                ImageIcon.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f); // Sanghun
                ImageIcon.transform.localPosition = new Vector3(0, 0, 0);
                TextName.SetText(shopSpell.Skill.Name);
            }
          
            if(GameManager.Instance.PlayerObject.Soul >= shopSpell.Soul)
            {
                isCheckPurchase = true;
                CoolTimeBG.fillAmount = 0f;
                foreach (var gray in grayEffect)
                    gray.effectFactor = 0f;
            }
            else
            {
                isCheckPurchase = false;
                CoolTimeBG.fillAmount = 1f - GameManager.Instance.PlayerObject.Soul / shopSpell.Soul;
                foreach (var gray in grayEffect)
                    gray.effectFactor = 1f;
            }
            TextPrice.SetText(shopSpell.Soul > 0 ? shopSpell.Soul.ToString() : "Free");
            tween.DORestart();
        }

        public void UpdateDisplay()
        {

        }
        public void UpdateDisplay(CharacterObject obj, Item item)
        {
            //Price.SetActive(true);
        }

        bool isCheckPurchase = false;
        private void OnEventBattleGoldUpdate()
        {
            if (shopSpell != null)
            {
                if(isPurchase) return;

                if (GameManager.Instance.PlayerObject.Soul >= shopSpell.Soul)
                {
                    if (!isCheckPurchase)
                    {
                        CoolTimeBG.fillAmount = 0f;
                        isCheckPurchase = true;
                        tweenScale.DORestart();
                        foreach (var gray in grayEffect)
                            gray.effectFactor = 0f;
                    }
                }
                else
                {
                    if (isCheckPurchase)
                    {
                        isCheckPurchase = false;
                        foreach (var gray in grayEffect)
                            gray.effectFactor = 1f;
                    }
                    CoolTimeBG.fillAmount =1f- GameManager.Instance.PlayerObject.Soul / shopSpell.Soul;
                }
            }
        }

        public void OnClickPurchase()
        {
            if (GameManager.Instance.PlayerObject.Soul < shopSpell.Soul)
            {
                GameManager.Instance.NotEnoughGold.SetActive(true);
                return;
            }

            if (shopSpell.SpellType == SpellType.Item)
            {
                if (!GameManager.Instance.PlayerObject.PurchaseItem(shopSpell.Item))
                {
                    if (shopSpell.Item.ItemType != ItemType.Equipment)
                    {
                        shopSpell.Item.UseItem(GameManager.Instance.PlayerObject);
                    }
                    else
                        return;
                }
            }
            else if(shopSpell.SpellType == SpellType.Skill)
            {
                if (shopSpell.Skill.UseSkill(GameManager.Instance.PlayerObject))
                {
                    //Price.SetActive(false);
                    //SkillIndicator.SetActive(false);
                    //ImageUseSkill.SetActive(true);
                    //GameEventManager.Instance.OnEventUseSkill(shopSpell.Skill);
                }
                else
                    return;
            }

            GameManager.Instance.PlayerObject.RandomItem.PurchaseItem(Index);
            GameManager.Instance.PlayerObject.Soul -= shopSpell.Soul;
            isPurchase = true;
            foreach (var gray in grayEffect)
                gray.effectFactor = 0f;
            CoolTimeBG.fillAmount = 0f;
            if (shopSpell.SpellType == SpellType.Item)
            {
                StartCoroutine(Respawn());
            }
            else if (shopSpell.SpellType == SpellType.Skill)
            {
                StartCoroutine(Respawn());
                //StartCoroutine(Respawn(shopSpell.Skill.DurationTime));
            }

            IEnumerator Respawn(float time = 0f)
            {
                yield return new WaitForSeconds(time);
                ItemBackground.SetActive(false);
                SlotBack.SetActive(true);            
                yield return new WaitForSeconds(0.5f);
                Init(GameManager.Instance.PlayerObject.RandomItem.ShopSpells[Index]);
            }
        }

        private bool IsLongPress = false;
        IEnumerator LongPress()
        {
            yield return new WaitForSecondsRealtime(0.2f);
            GameEventManager.Instance.OnEventSpellInfo(shopSpell, true);
            IsLongPress = true;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            StartCoroutine("LongPress");
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            StopCoroutine("LongPress");
            if (IsLongPress)
                GameEventManager.Instance.OnEventSpellInfo(null, false);
            else
                OnClickPurchase();

            IsLongPress = false;
        }
    }
}
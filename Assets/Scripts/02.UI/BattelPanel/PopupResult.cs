﻿using Secret;
using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Secret.UI
{
    public class PopupResult : BaseUI<PopupResult>
    {
        [SerializeField] TextMeshProUGUI textTrophy;
        [SerializeField] TextMeshProUGUI textClose;
        [SerializeField] TextMeshProUGUI textLevel;
        [SerializeField] TextMeshProUGUI textExp;
        [SerializeField] PrefabResultReward[] prefabResultRewards;
        [SerializeField] Slider sliderExp;

        bool isAction = true;

        public void Init(BattleResultResponseBody body)
        {
            Open();
            isAction = true;
            textClose.SetActive(false);
            int trophy = body.Trophy - body.PreTrophy;
            if (body.Trophy > 0)
                textTrophy.SetText($"{body.PreTrophy} (+{trophy})");
            else
                textTrophy.SetText($"{body.PreTrophy} ({trophy})");

            if (body.Reward != null)
            {
                for (int i = 0; i < body.Reward.Length; i++)
                {
                    prefabResultRewards[i].Init(body.Reward[i]);
                }
            }
            else
            {
                foreach (var prefab in prefabResultRewards)
                {
                    prefab.gameObject.SetActive(false);
                }
            }

            var userCharacter = GameDataManager.Instance.User.currentCharacter;
            sliderExp.maxValue = userCharacter.NextLevel != null ? userCharacter.NextLevel.RequiredExp : userCharacter.Exp;
            sliderExp.value = userCharacter.Exp;
            textLevel.SetText(userCharacter.Level);
            if (userCharacter.NextLevel != null)
                textExp.SetText($"{userCharacter.Exp}/{userCharacter.NextLevel.RequiredExp}");
            else
                textExp.SetText($"{userCharacter.Exp}/0");

            StartCoroutine(ScoreAction());

            IEnumerator ScoreAction()
            {
                int count = 0;
                while (Mathf.Abs(trophy) > count)
                {
                    yield return new WaitForSeconds(0.05f);
                    count++;
                    if (trophy > 0)
                    {
                        body.PreTrophy += 1;
                        textTrophy.SetText($"{body.PreTrophy} (+{trophy})");
                    }
                    else
                    {
                        body.PreTrophy -= 1;
                        textTrophy.SetText($"{body.PreTrophy} ({trophy})");
                    }
                }
                isAction = false;
                textClose.SetActive(true);
            }
        }

        public override void Close()
        {
            if (isAction) return;
            base.Close();
            SceneLoadManager.Instance.LoadLobby();
        }
    }
}
﻿using DG.Tweening;
using Photon.Pun.Demo.PunBasics;
using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Secret
{
    public class BattleItemSell : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        private BattleItem BattleItem;
        [SerializeField, GetComponentInChildrenName("TextPrice")] TextMeshProUGUI TextPrice;
        [SerializeField, GetComponent] DOTweenAnimation tween;
        public void InitSellPopup(BattleItem battleItem)
        {
            BattleItem = battleItem;
            if(BattleItem.Item != null)
            {
                TextPrice.SetText($"+{BattleItem.SoulPrice}");
                gameObject.SetActive(true);
            }
        }

        public void Close()
        {
            tween.DOPlayBackwards();
            gameObject.SetActive(false);
        }

        #region Event
        public void OnPointerEnter(PointerEventData eventData)
        {
            tween.DOPlayForward();
            GameManager.Instance.MoveIndex = -1;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            tween.DOPlayBackwards();
            GameManager.Instance.MoveIndex = -2;
        }


        #endregion
    }
}

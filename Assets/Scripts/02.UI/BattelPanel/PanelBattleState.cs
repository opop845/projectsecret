﻿using Secret;
using Secret.Server;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PanelBattleState : MonoBehaviour
{
    public TextMeshProUGUI AttackDamage, AttackSpeed, MovementSpeed, CriticalChance, CriticalDamage, HpRestorationRate;
    public TextMeshProUGUI AttackDamage_Enemy, AttackSpeed_Enemy, MovementSpeed_Enemy, CriticalChance_Enemy, CriticalDamage_Enemy, HpRestorationRate_Enemy; //Enemy Stats 정보 추가

    public ShopItemSlot[] ShopItemSlots;
    public BattleItemSlot[] BattleItemSlots;
    public Button ButtonShopReset;
    //public Image ImageRefreshIcon;
    public GameObject Price;
    public GameObject FreeCount;
    public TextMeshProUGUI TextRefreshTime;
    public TextMeshProUGUI TextFreeRefreshCount;


    // HpAlert 연출, Sanghun
    public GameObject HpAlert;

    private void Awake()
    {
        BattleEventManager.Instance.EventGameState += Init;
        GameEventManager.Instance.EventBattleLevelUp += UpdateState;
        GameEventManager.Instance.EventBattleGoldUpdate += OnEventBattleGoldUpdate;
    }

    private void OnDestroy()
    {
        BattleEventManager.Instance.EventGameState -= Init;
        GameEventManager.Instance.EventBattleLevelUp -= UpdateState;
        GameEventManager.Instance.EventBattleGoldUpdate -= OnEventBattleGoldUpdate;
    }

    private void Init(GameState state)
    {
        if (state != GameState.Ing) return;
        UpdateState();
        //UpdateHp();

        TextRefreshTime.SetText("(" + $"{GameManager.Instance.PlayerObject.FreeRefreshRemainTime}" + ")"); // Sanghun
        TextFreeRefreshCount.SetText($"{GameManager.Instance.PlayerObject.RefreshFreeCount}"); // Sanghun
        Price.SetActive(true); // Sanghun
        FreeCount.SetActive(false); // Sanghun

        ButtonShopReset.OnClick(OnClickShopReset);
        for (int i = 0; i < GameManager.Instance.PlayerObject.RandomItem.ShopSpells.Length; i++)
        {
            ShopItemSlots[i].Init(GameManager.Instance.PlayerObject.RandomItem.ShopSpells[i]);
        }

        for (int i = 0; i < GameManager.Instance.PlayerObject.BattleInventory.battleItems.Length; i++)
        {
            int index = i;
            BattleItemSlots[i].Init(index);
        }
    }

    private void Update()
    {
        TextRefreshTime.SetText("(" + $"{GameManager.Instance.PlayerObject.FreeRefreshRemainTime}" +")"); // Sanghun
        TextFreeRefreshCount.SetText($"{GameManager.Instance.PlayerObject.RefreshFreeCount}"); // Sanghun
        if(GameManager.Instance.PlayerObject.RefreshFreeCount > 0)  FreeCount.SetActive(true); // Sanghun
        else FreeCount.SetActive(false);
    }

    void UpdateState()
    {
        AttackDamage.SetText($"{GameManager.Instance.PlayerObject.Stat.GetStatValue(AttrType.Attack):#,##0}");
        AttackSpeed.SetText($"{GameManager.Instance.PlayerObject.Stat.GetStatValue(AttrType.AttackSpeed):0.00}");
        MovementSpeed.SetText($"{GameManager.Instance.PlayerObject.Stat.GetStatValue(AttrType.MoveSpeed):#,##0}");
        CriticalChance.SetText($"{GameManager.Instance.PlayerObject.Stat.GetStatValue(AttrType.CriticalChance):p1}");
        CriticalDamage.SetText($"{GameManager.Instance.PlayerObject.Stat.GetStatValue(AttrType.CriticalDamage):p1}");
        HpRestorationRate.SetText($"{GameManager.Instance.PlayerObject.Stat.GetStatValue(AttrType.HpRestorationRate):p1}");

        //Enemy Stats 정보 추가
        AttackDamage_Enemy.SetText($"{GameManager.Instance.EnemyObject.Stat.GetStatValue(AttrType.Attack):#,##0}");
        AttackSpeed_Enemy.SetText($"{GameManager.Instance.EnemyObject.Stat.GetStatValue(AttrType.AttackSpeed):0.00}");
        MovementSpeed_Enemy.SetText($"{GameManager.Instance.EnemyObject.Stat.GetStatValue(AttrType.MoveSpeed):#,##0}");
        CriticalChance_Enemy.SetText($"{GameManager.Instance.EnemyObject.Stat.GetStatValue(AttrType.CriticalChance):p1}");
        CriticalDamage_Enemy.SetText($"{GameManager.Instance.EnemyObject.Stat.GetStatValue(AttrType.CriticalDamage):p1}");
        HpRestorationRate_Enemy.SetText($"{GameManager.Instance.EnemyObject.Stat.GetStatValue(AttrType.HpRestorationRate):p1}");
    }

    void UpdateHp()
    {
        //// HpAlert 기능 임시로 만들어 둠. Sanghun
        //if (GameManager.Instance.PlayerObject.Hp < (int)(GameManager.Instance.PlayerObject.State.GetStatValue(AttrType.Hp)) * 0.3f) HpAlert.SetActive(true);
        //else if (GameManager.Instance.PlayerObject.Hp >= (int)(GameManager.Instance.PlayerObject.State.GetStatValue(AttrType.Hp)) * 0.35f) HpAlert.SetActive(false);

    }
    private void OnEventBattleGoldUpdate()
    {
        //if (GameManager.Instance.PlayerObject.Soul >= GameDataManager.Instance.GameRule.RefreshSoulPrice)
        //    TextFreeRefreshCount.color = new Color32(94, 83, 38, 255);
        //else
        //    TextFreeRefreshCount.color = Color.red;
    }

    #region Event

    public void OnClickShopReset()
    {
        if(GameManager.Instance.PlayerObject.SetShopRefresh())
        { 
            for (int i = 0; i < GameManager.Instance.PlayerObject.RandomItem.ShopSpells.Length; i++)
            {
                if (ShopItemSlots[i].isPurchase) continue;
                ShopItemSlots[i].Init(GameManager.Instance.PlayerObject.RandomItem.ShopSpells[i]);
            }
        }
    }

    public void ShopResetWhenPurchasedItem()
    {

        GameManager.Instance.PlayerObject.SetShopRefresh();
        for (int i = 0; i < GameManager.Instance.PlayerObject.RandomItem.ShopSpells.Length; i++)
        {
            ShopItemSlots[i].Init(GameManager.Instance.PlayerObject.RandomItem.ShopSpells[i]);
        }
    }

    #endregion
}

﻿using DG.Tweening;
using Secret;
using SRF;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Secret
{

    public class BattleItemSlot : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField, GetComponentInChildrenName("TextName")] TextMeshProUGUI TextName;
        [SerializeField, GetComponentInChildrenName("ItemCount")] TextMeshProUGUI ItemCount; // Sanghun
        [SerializeField, GetComponentInChildrenName("MergeCount")] TextMeshProUGUI MergeCount; // Sanghun
        [SerializeField, GetComponentInChildrenName("Item")] Image ImageItem;
        [SerializeField, GetComponentInChildrenName("Notification")] Image Notification;
        [SerializeField, GetComponentInChildrenName("ItemBackground")] GameObject ItemBackground; // Sanghun
        [SerializeField, GetComponentInChildrenName("Badge_Merge")] GameObject Badge_Merge; // Sanghun
        [SerializeField, GetComponent] Image MyImage;
        [SerializeField, GetComponent] DOTweenAnimation tween;
        [SerializeField] GameObject MergeEffect;
        // [SerializeField, GetComponent] Button ButtonUse;
        private bool IsPress = false;
        private CharacterObject characterObject;
        private int Index;
        private BattleItem battleItem => characterObject.BattleInventory.battleItems[Index];

        private void Awake()
        {
            // ButtonUse.OnClick(OnClickUse);
            GameEventManager.Instance.EventBattleItemUpdate += Display;
            GameEventManager.Instance.EventItemMerge += OnEventItemMerge;
        }

        private void OnDestroy()
        {
            GameEventManager.Instance.EventBattleItemUpdate -= Display;
            GameEventManager.Instance.EventItemMerge -= OnEventItemMerge;
        }

        public void Init(int index)
        {
            characterObject = GameManager.Instance.PlayerObject;
            Index = index;
            Display();
        }

        void Display()
        {
            if (battleItem.Item != null)
            {
                //TextName.SetText(battleItem.Item.Index);
                if (battleItem.Item.ItemType == Data.ItemType.Equipment && battleItem.Item.IsMergeable == true)
                {             
                    Badge_Merge.SetActive(true); // Sanghun
                    if (battleItem.Level > 0)
                        MergeCount.SetText($"{battleItem.Level+1}");
                    else
                        MergeCount.SetText("1");
                    ItemCount.SetText(""); // Sanghun
                }
                else if (battleItem.Item.ItemType == Data.ItemType.Equipment)
                {
                    Badge_Merge.SetActive(false); // Sanghun
                    ItemCount.SetText(""); // Sanghun
                }
                else
                {
                    Badge_Merge.SetActive(false); // Sanghun
                    ItemCount.SetText(battleItem.Count);
                }


                ImageItem.SetSprite(battleItem.Item.IconSprite);
                //ImageItem.SetNativeSize(); // Sanghun
                //ImageItem.rectTransform.sizeDelta *= new Vector2(0.7f, 0.7f); // Sanghun
                ItemBackground.SetActive(true);
                ImageItem.SetActive(true);
                Notification.gameObject.SetActive(false);
                foreach (var battle in GameManager.Instance.PlayerObject.BattleInventory.battleItems)
                {
                    if (battle.Item != null && battle != battleItem)
                    {
                        var mergeItem = GameDataManager.Instance.GetMergeItem(battleItem.Item, battle.Item);
                        if (mergeItem != null)
                        {
                            Notification.gameObject.SetActive(true);
                            return;
                        }
                    }
                }
            }
            else
            {
                ItemBackground.SetActive(false);
                ImageItem.SetActive(false);
            }
            tween.DOPlayBackwards();
        }

        private void OnEventItemMerge(CharacterObject obj, BattleItem item)
        {
            if (battleItem == item)
                MergeEffect.SetActive(true);
        }

        #region Event

        void OnClickUse()
        {
            if (!IsPress && !IsLongPress)
                battleItem?.UseBattleItem();
        }


        public void OnBeginDrag(PointerEventData eventData)
        {
            if (battleItem.Item == null) return;
            IsPress = true;
            GameManager.Instance.SelectBeginDragItem(battleItem, Index);
            StopAllCoroutines();
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (IsPress)
                GameManager.Instance.SelectDragItem(eventData.position);
        }


        public void OnEndDrag(PointerEventData eventData)
        {
            if (IsPress)
            {
                tween.DOPlayBackwards();
                GameManager.Instance.SelectEndDragItem();
                IsPress = false;
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (GameManager.Instance.InvenIndex >= 0)
            {
                tween.DOPlayForward();
                GameManager.Instance.MoveIndex = Index;
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (GameManager.Instance.InvenIndex >= 0)
            {
                tween.DOPlayBackwards();
                if (GameManager.Instance.MoveIndex == Index)
                    GameManager.Instance.MoveIndex = -2;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            //OnClickUse();
        }

        private bool IsLongPress = false;
        IEnumerator LongPress()
        {
            if (battleItem.Item == null) yield break;
            yield return new WaitForSecondsRealtime(0.2f);
            GameEventManager.Instance.OnEventItemInfo(battleItem.Item, true);
            IsLongPress = true;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            StartCoroutine(LongPress());
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            StopAllCoroutines();
            if (IsLongPress)
            {
                GameEventManager.Instance.OnEventItemInfo(battleItem.Item, false);
                IsLongPress = false;
            }
            else
                OnClickUse();
        }


        #endregion
    }

}
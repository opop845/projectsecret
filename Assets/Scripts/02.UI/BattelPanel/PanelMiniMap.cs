﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Secret
{
    public class PanelMiniMap : MonoBehaviour
    {
        public GameObject MiniMapCell;
        public GameObject Link;
        public ScrollRect Scroll;
        public RectTransform StartPos;
        public RectTransform EndPos;
        public RectTransform Player;
        public RectTransform Enemy;
        private List<RectTransform> MiniCells = new List<RectTransform>();
        float xSize;

        private void Awake()
        {
            BattleEventManager.Instance.EventGameState += Init;
            xSize = MiniMapCell.GetComponent<RectTransform>().sizeDelta.x + Link.GetComponent<RectTransform>().sizeDelta.x;
        }

        private void OnDestroy()
        {
            BattleEventManager.Instance.EventGameState -= Init;
        }

        private void Update()
        {
            if (init)
            {
                if (GameManager.Instance.PlayerIndex <= 2)
                    Scroll.horizontalNormalizedPosition = 0f;
                else
                    Scroll.horizontalNormalizedPosition = (GameManager.Instance.PlayerIndex - 2) * xSize / 2850f;

                if(GameManager.Instance.PlayerIndex == GameManager.Instance.EnemyIndex)
                {
                    Player.anchoredPosition = MiniCells[GameManager.Instance.PlayerIndex].anchoredPosition + new Vector2(-25f,0f);
                    Enemy.anchoredPosition = MiniCells[GameManager.Instance.EnemyIndex].anchoredPosition + new Vector2(25f, 0f);
                }
                else
                {
                    Player.anchoredPosition = MiniCells[GameManager.Instance.PlayerIndex].anchoredPosition;
                    Enemy.anchoredPosition = MiniCells[GameManager.Instance.EnemyIndex].anchoredPosition;
                }
               
            }
        }

        bool init = false;
        public void Init(GameState state)
        {
            if (state == GameState.Init)
            {
                MiniCells.Add(StartPos);
                init = true;
                int index = 2;
                int pointIndex = 1;
                var map = GameDataManager.Instance.MapList;
                for (int i = 0; i < map.Count; i++)
                {
                    if (map[i].Type == "Point")
                    {
                        var mini = Instantiate(MiniMapCell, Scroll.content);
                        var link = Instantiate(Link, Scroll.content);
                        MiniCells.Add(mini.GetComponent<RectTransform>());
                        mini.transform.Find("Text").GetComponent<TextMeshProUGUI>().SetText(pointIndex++);
                        mini.transform.SetSiblingIndex(index++);
                        link.transform.SetSiblingIndex(index++);
                        mini.SetActive(true);
                    }
                }
                MiniCells.Add(EndPos);
            }
        }
    }
}
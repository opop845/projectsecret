﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Secret
{
    public class PopupGameExit : BaseUI<PopupGameExit>
    {
        [SerializeField, GetComponentInChildrenName("ButtonExit")] Button ButtonExit;
        protected override void Initialize()
        {
            base.Initialize();
            ButtonExit.OnClick(() => Application.Quit());
        }
    }
}
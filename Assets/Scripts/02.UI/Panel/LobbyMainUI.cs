﻿using Cinemachine;
using Secret;
using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


namespace Secret.UI
{
    public class LobbyMainUI : MonoSingleton<LobbyMainUI>
    {
        [SerializeField] PanelShop panelShop;
        [SerializeField] PanelLeaderBoard panelLeaderBoard;
        [SerializeField] PanelBattle panelBattle;
        /*[SerializeField]*/public  PanelCharacter panelCharacter;
        [SerializeField] PanelSpell panelSpell;

        [SerializeField, GetComponentInChildrenName("TextLevel")] TextMeshProUGUI TextLevel;
        [SerializeField, GetComponentInChildrenName("TextName")] TextMeshProUGUI TextName;

        [SerializeField] List<GameObject> characters;
        [SerializeField] ParticleSystem characterChangedParticle;
        [SerializeField] float effectTime;
        private UserCharacter userCharacter;

        public Canvas canvas;
        public GameObject PanelTop;

        public string DocURL;

        [SerializeField] Toggle[] mainToggles;

        private void Awake()
        {
            GameEventManager.Instance.EventInit += Init;
            GameEventManager.Instance.EventUIHide += OnEventUIHide;
            GameEventManager.Instance.EventCharacterChanged += OnEventCharacterChanged;
        }

        private void OnDestroy()
        {
            GameEventManager.Instance.EventInit -= Init;
            GameEventManager.Instance.EventUIHide -= OnEventUIHide;
            GameEventManager.Instance.EventCharacterChanged -= OnEventCharacterChanged;
        }

        public void OnEventUIHide(bool isHide)
        {
            canvas.enabled = !isHide;
        }

        void Init()
        {
            userCharacter = GameDataManager.Instance.User.currentCharacter;
            TextName.SetText(GameDataManager.Instance.User.NickName);
            Display();

            for (int i = 0; i < mainToggles.Length; i++)
            {
                MainTabType type = (MainTabType)i;
                mainToggles[i].OnValueChanged((isOn) => OnValueChangedTab(isOn, type));
            }
            panelShop.Initialize();
            panelLeaderBoard.Initialize();
            panelBattle.Initialize();
            panelCharacter.Initialize();
            panelSpell.Initialize();
        }

        void OnEventCharacterChanged(UserCharacter character)
        {
            userCharacter = character;
            Display();
        }

        void OnEventLobbyTabChanged()
        {
            if (userCharacter != GameDataManager.Instance.User.currentCharacter)
            {
                userCharacter = GameDataManager.Instance.User.currentCharacter;
                Display();
            }
        }

        public void Display()
        {
            if (userCharacter != null)
            {
                foreach (var character in characters)
                {
                    character.SetActive(false);
                }
                StopCoroutine("ChangedEffect");
                StartCoroutine("ChangedEffect");
            }
        }

        IEnumerator ChangedEffect()
        {
            if (userCharacter != null)
            {
                characterChangedParticle.Play();
                yield return new WaitForSeconds(effectTime);
                switch (userCharacter.Character.Index)
                {
                    case 100001:
                        characters[0].SetActive(true);
                        break;
                    case 100002:
                        characters[1].SetActive(true);
                        break;
                    case 100003:
                        characters[2].SetActive(true);
                        break;
                    case 100004:
                        characters[3].SetActive(true);
                        break;
                    case 100005:
                        characters[4].SetActive(true);
                        break;
                    case 100006:
                        characters[5].SetActive(true);
                        break;
                    case 100007:
                        characters[6].SetActive(true);
                        break;
                    case 100008:
                        characters[7].SetActive(true);
                        break;
                    case 100009:
                        characters[8].SetActive(true);
                        break;
                }
            }
        }

        public void OnValueChangedTab(bool isOn, MainTabType type)
        {
            if(isOn)
            {
                GameEventManager.Instance.OnEventLobbyTabChanged(type);
            }
        }
    }

    public enum MainTabType
    {
        Shop = 0,
        LeaderBoard,
        Battle,
        Character,
        Spell
    }
}

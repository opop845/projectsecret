﻿using Photon.Pun.Demo.PunBasics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Secret
{
    public class PopupPause : BaseUI<PopupPause>
    {

        public Button ButtonExit;
        public Button ButtonResume;
        [SerializeField, GetComponentInChildrenName("Toggle2X")] Toggle Toggle2X;
        [SerializeField, GetComponentInChildrenName("ToggleState")] Toggle ToggleState;

        protected override void Initialize()
        {
            base.Initialize();
            ButtonExit.OnClick(OnClickExit);
            ButtonResume.OnClick(Close);
        }

        void OnClickExit()
        {
            Close();
            SceneLoadManager.Instance.LoadLobby();
        }

        public override void Open()
        {
            base.Open();
            Time.timeScale = 0f;
        }

        public override void Close()
        {
            base.Close();
            Time.timeScale = Toggle2X.isOn ? 2f : 1f;
          //  GameManager.Instance.playerInfo.SetActive(ToggleState.isOn);
            //GameManager.Instance.enemyInfo.SetActive(ToggleState.isOn);
        }
    }
}
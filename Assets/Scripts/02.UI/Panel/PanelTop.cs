﻿using Secret;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Secret.UI
{
    public class PanelTop : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI TextTrophy;
        [SerializeField] TextMeshProUGUI TextName;
        [SerializeField] TextMeshProUGUI TextGold;
        [SerializeField] TextMeshProUGUI TextGem;

        private void Awake()
        {
            GameEventManager.Instance.EventInit += Init;
            GameEventManager.Instance.EventGoldChanged += OnEventGoldChanged;
            GameEventManager.Instance.EventForceQuitLose += UpdateTrophy;
        }


        private void OnDestroy()
        {
            GameEventManager.Instance.EventInit -= Init;
            GameEventManager.Instance.EventGoldChanged -= OnEventGoldChanged;
            GameEventManager.Instance.EventForceQuitLose -= UpdateTrophy;
        }

        void Init()
        {
            TextName.SetText(GameDataManager.Instance.User.NickName);
            TextGold.SetText(GameDataManager.Instance.User.Gold);
            TextGem.SetText(GameDataManager.Instance.User.Gem);
            TextTrophy.SetText(GameDataManager.Instance.User.Trophy);
        }

        void UpdateTrophy()
        {
            TextTrophy.SetText(GameDataManager.Instance.User.Trophy);
        }

        #region Event

        public void OnClickTrophyPopup()
        {
            PopupTrophyReward.Instance.Open();
        }

        public void OnClickSettings()
        {
            PopupSettings.Instance.Open();
        }

        #endregion

        #region EventHandler

        private void OnEventGoldChanged(int gold)
        {
            TextGold.SetText(GameDataManager.Instance.User.Gold);
        }


        #endregion
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Secret.UI
{
    public class PopupAlert : BaseUI<PopupAlert>
    {
        [SerializeField] GameObject headerNoti;
        [SerializeField] GameObject headerDeco;
        [SerializeField] Button btnYes;
        [SerializeField] Button btnNo;
        [SerializeField] Button btnClose;
        [SerializeField] TextMeshProUGUI textContent;

        public void OpenNoti(string value, bool isForce = false, UnityAction actionYes = null)
        {
            textContent.SetText(value);
            btnNo.SetActive(false);
            headerNoti.SetActive(true);
            headerDeco.SetActive(false);
            btnYes.onClick.RemoveAllListeners();
            btnYes.OnClick(ForceClose);
            if (actionYes != null)
                btnYes.OnClick(actionYes);
            btnClose.SetActive(!isForce);
        }

        public void OpenOneAlert()
        {

        }

        public override void Close()
        {
            base.Close();
        }
    }
}
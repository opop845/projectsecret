﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Secret.Data;
using DG.Tweening;

namespace Secret
{
    public class PrefabItemInfo : MonoBehaviour
    {
        [SerializeField, GetComponentInChildrenName("ImageIcon")] Image ImageIcon;
        [SerializeField, GetComponentInChildrenName("TextName")] TextMeshProUGUI TextName;
        [SerializeField, GetComponentInChildrenName("ItemDesc")] TextMeshProUGUI ItemDesc;
        [SerializeField] DOTweenAnimation tween;

        Vector2 SetSkilIconSize;
        Vector3 SetIconScale;

        private void Awake()
        {   
            SetIconScale = ImageIcon.transform.localScale;
        }

        public void OnItemInfo(Item item, bool isPress)
        {
            if(isPress)
            {
                gameObject.SetActive(true);
                ImageIcon.SetSprite(item.IconSprite);
                ImageIcon.transform.localScale = SetIconScale;
                TextName.SetText(item.Name);
                ItemDesc.SetText(item.Desc);
                tween.DOPlayForward();
            }
            else
            {
                tween.DOPlayBackwards();
            } 
        }
        public void OnSkillInfo(Skill skill, bool isPress)
        {
            if (isPress)
            {
                gameObject.SetActive(true);
                ImageIcon.SetSprite(skill.IconSprite);
                ImageIcon.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f); // Sanghun
                TextName.SetText(skill.Name);
                ItemDesc.SetText(skill.Desc);
                tween.DOPlayForward();
            }
            else
            {
                tween.DOPlayBackwards();
            }
        }


        public void OnSpellInfo(Spell spell, bool isPress)
        {
            if(isPress)
            {
                if (spell.SpellType == SpellType.Item)
                    OnItemInfo(spell.Item, isPress);
                else
                    OnSkillInfo(spell.Skill, isPress);
            }
            else
            {
                tween.DOPlayBackwards();
            }
        }
    }
}
﻿using DG.Tweening;
using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Secret
{
    public class PrefabEnemyInfo : MonoBehaviour
    {
        [SerializeField, GetComponentInChildrenName("TextState")] TextMeshProUGUI TextState;
        [SerializeField, GetComponentInChildrenName("ImageIcon")] Image ImageIcon;
        [SerializeField] GameObject ItemIcon; // Sanghun
        [SerializeField] GameObject Killcon; // Sanghun
        List<ItemInfo> itemInfos = new List<ItemInfo>();
        private float disappearTime = 1.0f;
        private float timer = 0f;
        private bool isForward = false;
        public DOTweenAnimation tween;

        public void OnRewind()
        {
            if (itemInfos.Count > 0)
            {
                if (itemInfos[0].noticeType == NoticeType.Kill)
                {
                    TextState.SetText($"Enemy Killed\n{itemInfos[0].deadObject}."); // Sanghun                   
                    ItemIcon.SetActive(false);
                    Killcon.SetActive(true);
                }
                else
                {
                    TextState.SetText($"{itemInfos[0].noticeType}");
                    //TextState.SetText($"Enemy {itemInfos[0].noticeType}\n{itemInfos[0].item.Name}");
                    ImageIcon.SetSprite(itemInfos[0].item.IconSprite);
                    ItemIcon.SetActive(true);
                    Killcon.SetActive(false);
                }
                itemInfos.RemoveAt(0);
                tween.DORestart();
                isForward = true;
                timer = 0f;
            }
            else
                gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            tween.DORestart();
            isForward = true;
            timer = 0f;
        }

        private void Update()
        {
            if (isForward)
            {
                timer += Time.deltaTime;
                if (timer >= disappearTime)
                {
                    isForward = false;
                    tween.DOPlayBackwards();
                }
            }
        }

        public void DisplayPurchase(Item item)
        {
            if (gameObject.activeSelf)
                itemInfos.Add(new ItemInfo(item, NoticeType.Purchased));
            else
            {
                TextState.SetText($"{NoticeType.Purchased}");
                //TextState.SetText($"Enemy {NoticeType.Purchased}\n{item.Name}");
                ImageIcon.SetSprite(item.IconSprite);
                ItemIcon.SetActive(true);
                Killcon.SetActive(false);
                gameObject.SetActive(true);
                tween.DORestart();
                isForward = true;
                timer = 0f;
            }
        }


        public void DisplayMerge(Item item)
        {
            if (gameObject.activeSelf)
                itemInfos.Add(new ItemInfo(item, NoticeType.Merged));
            else
            {
                TextState.SetText($"Enemy {NoticeType.Merged}\n{item.Name}");
                ImageIcon.SetSprite(item.IconSprite);
                ItemIcon.SetActive(true);
                Killcon.SetActive(false);
                gameObject.SetActive(true);
                tween.DORestart();
                isForward = true;
                timer = 0f;
            }
        }

        public void DisplayUse(Item item)
        {
            if (gameObject.activeSelf)
                itemInfos.Add(new ItemInfo(item, NoticeType.Used));
            else
            {
                TextState.SetText($"{NoticeType.Used}");
                //TextState.SetText($"Enemy {NoticeType.Used}\n{item.Name}");
                ImageIcon.SetSprite(item.IconSprite);
                ItemIcon.SetActive(true);
                Killcon.SetActive(false);
                gameObject.SetActive(true);
                tween.DORestart();
                isForward = true;
                timer = 0f;
            }
        }

        public void DisplayKill(string attacker,string dead)
        {
            if (gameObject.activeSelf)
                itemInfos.Add(new ItemInfo(null, NoticeType.Kill,attacker,dead));
            else
            {
                TextState.SetText($"Enemy Killed\n{dead}."); // Sanghun
                Killcon.SetActive(true);
                gameObject.SetActive(true);
                tween.DORestart();
                isForward = true;
                timer = 0f;
            }
        }

        public enum NoticeType
        {
            Purchased,
            Merged,
            Used,
            Kill,
        }

        struct ItemInfo
        {
            public NoticeType noticeType;
            public Item item;
            public string attacker;
            public string deadObject;
            public ItemInfo(Item item, NoticeType type,string attacker="", string dead ="")
            {
                this.item = item;
                noticeType = type;
                this.attacker = attacker;
                deadObject = dead;
            }
        }

    }
}
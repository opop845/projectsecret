﻿using BehaviorDesigner.Runtime.Tasks.Unity.UnityVector2;
using Secret;
using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PrefabHpbar : MonoBehaviour
{

    private RectTransform rectTransform;
    [HideInInspector]
    public DefaultObject Obj;
    private CharacterObject CharacterObject;
    public TextMeshProUGUI TextHp;
    public Slider SliderHp;
    public Slider SlowSliderHp;
    private Camera mainCam;
    private Camera uiCam;
    public Vector2 Offset;
    public GameObject SliderHp_Object;
    public GameObject SlowSliderHp_Object;
    public GameObject RespawnTime_Object;

    public GameObject PlayerTowerIcon, EnemyTowerIcon;


    public TextMeshProUGUI RespawnTime_Text;

    float HpSlideShowTime = 0.35f;
    float startValue, deltaValue;
    float timer = 0.0f;
    bool showRespawnTime = false;

    Vector2 originalOutput;




    [Header("Buff")]
    public PrefabBuff PrefabBuff;
    public Transform BuffList;
    private PrefabBuff[] prefabBuffs;


    private void Awake()
    {
        prefabBuffs = new PrefabBuff[8];
        for (int i = 0; i < 8; i++)
        {
            prefabBuffs[i] = Instantiate(PrefabBuff, BuffList);
        }
        GameEventManager.Instance.EventNewBuff += OnEventNewBuff;
    }

    private void OnDestroy()
    {
        GameEventManager.Instance.EventNewBuff -= OnEventNewBuff;
    }

    public void Init(DefaultObject obj, Camera mainCam, Canvas uiCanvas)
    {
        transform.name = obj.tag.ToString();
        rectTransform = GetComponent<RectTransform>();
        this.mainCam = mainCam;
        uiCam = uiCanvas.worldCamera;
        Obj = obj;
        gameObject.SetActive(true);

        startValue = obj.HpSlide;
        SlowSliderHp.value = startValue;

        if (gameObject.name == "Player" || gameObject.name == "Monster")
        {
            if (mainCam != null)
            {
                Vector2 screenPoint;
                if (gameObject.tag == "Monster") screenPoint = mainCam.WorldToScreenPoint(new Vector3 (Obj.transform.position.x, -0.4f, Obj.transform.position.z));
                else screenPoint = mainCam.WorldToScreenPoint(Obj.transform.position);
                Vector2 output;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent as RectTransform, screenPoint + Vector2.up * Offset, uiCam, out output);
                rectTransform.anchoredPosition3D = output;
                originalOutput = output;
            }
            else
            {
                gameObject.SetActive(false);
                //Debug.Log("HP 사라지는것 찾기1");

            }
        }     
    }

    void Update()
    {
        if (Obj != null)
        {
            if (Obj.tag == "PlayerTower")
            {
                if (Obj.Hp <= 0) gameObject.SetActive(false);
                PlayerTowerIcon.SetActive(true);
                EnemyTowerIcon.SetActive(false);
            }

            if (Obj.tag == "EnemyTower")
            {
                if (Obj.Hp <= 0) gameObject.SetActive(false);
                PlayerTowerIcon.SetActive(false);
                EnemyTowerIcon.SetActive(true);
            }

            TextHp.SetText($"{Obj.Hp}");
            //TextHp.SetText($"{Obj.Hp}/{(int)Obj.State.GetStatValue(AttrType.Hp)}");
            

            // Sanghun, HP 연출
            if (Obj.HpSlide != startValue)
            {               
                deltaValue = (startValue - Obj.HpSlide) / HpSlideShowTime;
                timer += Time.deltaTime;
                SlowSliderHp.value -= deltaValue * Time.deltaTime;

               if (timer > HpSlideShowTime)
                {
                    startValue = Obj.HpSlide;
                    timer = 0.0f;
                }                      
            }
            else
            {
                startValue = Obj.HpSlide;
                SlowSliderHp.value = startValue;
                timer = 0.0f;
            } 

            SliderHp.value = Obj.HpSlide;

            // 죽었을때 RespawnTime 노출
            if (Obj.ObjectType == ObjectType.Player || Obj.ObjectType == ObjectType.Enemy)
            {
                if (Obj.IsDead == true && Obj.GetComponent<CharacterObject>().isRecalling == false && showRespawnTime == false)
                {
                    SlowSliderHp_Object.SetActive(false);
                    SliderHp_Object.SetActive(false);
                    RespawnTime_Object.SetActive(true);
                    StartCoroutine(RespawnTimeCountdown());
                    //Debug.Log("여길 두번?");
                }
                else if (Obj.IsDead == false)
                {
                    SlowSliderHp_Object.SetActive(true);
                    SliderHp_Object.SetActive(true);
                    RespawnTime_Object.SetActive(false);
                }
            }

            if (gameObject.name == "Player" || gameObject.name == "Monster")
            {
                if (mainCam != null)
                {
                    Vector2 screenPoint;
                    if (gameObject.tag == "Monster") screenPoint = mainCam.WorldToScreenPoint(new Vector3(Obj.transform.position.x, -0.4f, Obj.transform.position.z));
                    else screenPoint = mainCam.WorldToScreenPoint(Obj.transform.position);
                    Vector2 output;
                    RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent as RectTransform, screenPoint + Vector2.up * Offset, uiCam, out output);

                    if (Vector2.SqrMagnitude(originalOutput - output) < 500.0f)
                    {
                        rectTransform.anchoredPosition3D = originalOutput;
                    }
                    else
                    {
                        rectTransform.anchoredPosition3D = output;
                        originalOutput = output;
                    }

                    //Debug.Log(Vector2.SqrMagnitude(originalOutput - output));
                }
                else
                {
                    gameObject.SetActive(false);
                    //Debug.Log("HP 사라지는것 찾기2");

                }
                
            }
        }
        else
        {
            //Debug.Log("HP 사라지는것 찾기3");
            gameObject.SetActive(false);           
        }

    }
    IEnumerator RespawnTimeCountdown()
    {
        showRespawnTime = true;
        for (float i = Obj.RespawnTime; i > 0; i--)
        {
            RespawnTime_Text.text = i.ToString();
            yield return new WaitForSeconds(1.0f);
        }
        RespawnTime_Text.text = " ";
        yield return new WaitForSeconds(1.0f);
        showRespawnTime = false;
    }

    #region EventHandler

    private void OnEventNewBuff(DefaultObject obj, BuffController buff)
    {
        if (Obj != obj) return;
        foreach (var prefab in prefabBuffs)
        {
            if (prefab.buff == null)
            {
                prefab.Init(buff);
                break;
            }
        }
    }

    #endregion

}

﻿using DG.Tweening;
using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Secret
{
    public class PrefabItemNotice : MonoBehaviour
    {
        [SerializeField, GetComponentInChildrenName("TextState")] TextMeshProUGUI TextState;
        [SerializeField, GetComponentInChildrenName("ImageIcon")] Image ImageIcon;
        [SerializeField, GetComponentInChildrenName("TextName")] TextMeshProUGUI TextName;
        [SerializeField, GetComponentInChildrenName("ItemDesc")] TextMeshProUGUI ItemDesc;
        List<ItemInfo> itemInfos = new List<ItemInfo>();
        private float disappearTime = 1.0f;
        private float timer = 0f;
        private bool isForward = false;
        public DOTweenAnimation tween;

        public void OnRewind()
        {
            if (itemInfos.Count > 0)
            {
                TextState.SetText($"{itemInfos[0].noticeType}");
                ImageIcon.SetSprite(itemInfos[0].item.IconSprite);
                TextName.SetText(itemInfos[0].item.Name);
                ItemDesc.SetText(itemInfos[0].item.Desc);
                itemInfos.RemoveAt(0);
                tween.DORestart();
                isForward = true;
                timer = 0f;
            }
            else
                gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            tween.DORestart();
            isForward = true;
            timer = 0f;
        }

        private void Update()
        {
            if (isForward)
            {
                timer += Time.deltaTime;
                if (timer >= disappearTime)
                {
                    isForward = false;
                    tween.DOPlayBackwards();
                }
            }
        }

        public void DisplayPurchase(Item item)
        {
            if (gameObject.activeSelf)
                itemInfos.Add(new ItemInfo(item, NoticeType.Purchase));
            else
            {
                TextState.SetText($"{ NoticeType.Purchase}");
                ImageIcon.SetSprite(item.IconSprite);
                TextName.SetText(item.Name);
                ItemDesc.SetText(item.Desc);

                gameObject.SetActive(true);
                tween.DORestart();
                isForward = true;
                timer = 0f;
            }
        }


        public void DisplayMerge(Item item)
        {
            if (gameObject.activeSelf)
                itemInfos.Add(new ItemInfo(item, NoticeType.Merge));
            else
            {
                TextState.SetText($"{ NoticeType.Merge}");
                ImageIcon.SetSprite(item.IconSprite);
                TextName.SetText(item.Name);
                ItemDesc.SetText(item.Desc);

                gameObject.SetActive(true);
                tween.DORestart();
                isForward = true;
                timer = 0f;
            }
        }

        public void DisplayUse(Item item)
        {
            if (gameObject.activeSelf)
                itemInfos.Add(new ItemInfo(item, NoticeType.Use));
            else
            {
                TextState.SetText($"{NoticeType.Use}");
                ImageIcon.SetSprite(item.IconSprite);
                TextName.SetText(item.Name);
                ItemDesc.SetText(item.Desc);

                gameObject.SetActive(true);
                tween.DORestart();
                isForward = true;
                timer = 0f;
            }
        }

        public enum NoticeType
        {
            Purchase,
            Merge,
            Use,
        }

        struct ItemInfo
        {
            public NoticeType noticeType;
            public Item item;
            public ItemInfo(Item item, NoticeType type)
            {
                this.item = item;
                noticeType = type;
            }
        }
    }
}
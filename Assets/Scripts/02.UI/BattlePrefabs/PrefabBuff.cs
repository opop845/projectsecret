﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Secret
{
    public class PrefabBuff : MonoBehaviour
    {
        [SerializeField, GetComponentInChildrenName("ImageIcon")] Image ImageIcon;
        [SerializeField, GetComponentInChildrenName("TextTime")] TextMeshProUGUI TextTime;

        public BuffController buff { get; private set; }

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        public void Init(BuffController battleBuff)
        {
            buff = battleBuff;
            if (buff != null)
            {
                ImageIcon.SetSprite(battleBuff.sprite);
                transform.SetAsFirstSibling();
                TextTime.SetText((int)buff.DurationTime);
                gameObject.SetActive(true);

                buff.EventBuffUpdate += OnEventBuffUpdate;
                buff.EventBuffEnd += OnEventBuffEnd;
                buff.EventBuffChanged += OnEventBuffChanged;
            }
        }

        #region EventHandler

        private void OnEventBuffChanged()
        {
            TextTime.SetText((int)buff.DurationTime);
        }

        private void OnEventBuffEnd()
        {
            if(buff != null)
            {
                buff.EventBuffUpdate -= OnEventBuffUpdate;
                buff.EventBuffEnd -= OnEventBuffEnd;
                buff.EventBuffChanged -= OnEventBuffChanged;
                buff = null;
                gameObject.SetActive(false);
            }
        }

        public void OnEventBuffUpdate()
        {
            TextTime.SetText((int)buff.DurationTime);
        }

        #endregion
    }
}
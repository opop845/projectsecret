﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using I2.Loc;
using TMPro;

namespace Secret.UI
{
    public class PopupSettings : BaseUI<PopupSettings>
    {
        [SerializeField] TextMeshProUGUI textPlayerId;
        [SerializeField] TextMeshProUGUI textVersion;
        
        protected override void Initialize()
        {
            base.Initialize();
            textPlayerId.SetText(GameDataManager.Instance.User.PlayerId);
            textVersion.SetText($"version {Application.version}");
        }

        public void OnClickLanguage(string language)
        {
            if (LocalizationManager.HasLanguage(language) && LocalizationManager.CurrentLanguage != language)
            {
                LocalizationManager.CurrentLanguage = language;
            }
        }
    }
}
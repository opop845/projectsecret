﻿using DG.Tweening;
using Secret.Data;
using Secret.Server;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Secret.UI
{
    public class PrefabResultReward : MonoBehaviour
    {
        [SerializeField] Image ImageSkill;
        [SerializeField] Image ImageItem;
        [SerializeField] Image ImageGold;
        [SerializeField] Image SlotBG;
        [SerializeField] TextMeshProUGUI TextPrice;
        [SerializeField] TextMeshProUGUI TextCount;
        [SerializeField] GameObject SkillIndicator;

        public void Init(RewardChestBody body)
        {
            switch (body.RewardType)
            {
                case "SpellCard":
                    ImageGold.SetActive(false);
                    var userSpell = GameDataManager.Instance.User.GetUserSpell(body.Index);
                    if (userSpell == null) gameObject.SetActive(false);

                    if (userSpell.Spell.SpellType == SpellType.Item)
                    {
                        ImageItem.SetSprite(userSpell.Spell.Item.IconSprite);
                        ImageSkill.SetActive(false);
                        ImageItem.SetActive(true);
                        SkillIndicator.SetActive(false);
                    }
                    else
                    {
                        ImageSkill.SetSprite(userSpell.Spell.Skill.IconSprite);
                        ImageSkill.SetActive(true);
                        ImageItem.SetActive(false);
                        SkillIndicator.SetActive(true);
                    }

                    TextPrice.SetText(userSpell.Spell.Soul);

                    break;
                case "Gold":
                    TextPrice.transform.parent.SetActive(false);
                    ImageSkill.SetActive(false);
                    ImageItem.SetActive(false);
                    SkillIndicator.SetActive(false);
                    ImageGold.SetActive(true);
                    break;
            }
            TextCount.SetText(body.Value);
            SlotBG.SetActive(false);
            gameObject.SetActive(true);
        }
        private void OnDisable()
        {
            SlotBG.SetActive(true);
        }
    }
}
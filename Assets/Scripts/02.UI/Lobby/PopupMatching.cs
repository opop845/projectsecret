﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Secret
{
    public class PopupMatching : BaseUI<PopupMatching>
    {
        [SerializeField] TextMeshProUGUI textPlayerName;
        [SerializeField] TextMeshProUGUI TextPlayerScore;
        [SerializeField] TextMeshProUGUI TextPlayerBattlePower;
        [SerializeField] TextMeshProUGUI textEnemyName;
        [SerializeField] TextMeshProUGUI TextEnmeyScore;
        [SerializeField] TextMeshProUGUI TextEnmeyBattlePower;
        [SerializeField] Button BtnBattle;
        [SerializeField] PrefabMatchingSlot[] PlayerMatchingSlots;
        [SerializeField] PrefabMatchingSlot[] EnemyMatchingSlots;

        [SerializeField] GameObject[] playerModels;
        [SerializeField] GameObject[] enemyModels;
        private bool IsBattleReady = false;

        protected override void Awake()
        {
            base.Awake();
            GameEventManager.Instance.EventNewEnemyUser += OnEventNewEnemyUser;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            GameEventManager.Instance.EventNewEnemyUser -= OnEventNewEnemyUser;
        }

        public override void Open()
        {
            base.Open();
            if (GameDataManager.Instance.User.EnemyUser == null)
            {
                ServerManager.Instance.NewEnemyUser();
                Close();
            }
            else
            {
                IsBattleReady = false;
                BtnBattle.OnClick(OnClickBattle);
                textPlayerName.SetText(GameDataManager.Instance.User.NickName);
                TextPlayerScore.SetText(GameDataManager.Instance.User.Trophy);
                TextPlayerBattlePower.SetText(GameDataManager.Instance.User.currentCharacter.State.BattlePower);

                textEnemyName.SetText(GameDataManager.Instance.User.EnemyUser.EnemyName);
                TextEnmeyScore.SetText(GameDataManager.Instance.User.EnemyUser.BaseScore);
                TextEnmeyBattlePower.SetText(GameDataManager.Instance.User.EnemyUser.Character.State.BattlePower);

                foreach (var model in playerModels)
                    model.SetActive(false);

                foreach (var model in enemyModels)
                    model.SetActive(false);

                switch (GameDataManager.Instance.User.currentCharacter.Character.Index)
                {
                    case 100001:
                        playerModels[0].SetActive(true);
                        break;
                    case 100002:
                        playerModels[1].SetActive(true);
                        break;
                    case 100003:
                        playerModels[2].SetActive(true);
                        break;
                    case 100004:
                        playerModels[3].SetActive(true);
                        break;
                    case 100005:
                        playerModels[4].SetActive(true);
                        break;
                    case 100006:
                        playerModels[5].SetActive(true);
                        break;
                    case 100007:
                        playerModels[6].SetActive(true);
                        break;
                    case 100008:
                        playerModels[7].SetActive(true);
                        break;
                    case 100009:
                        playerModels[8].SetActive(true);
                        break;
                }

                switch (GameDataManager.Instance.User.EnemyUser.Character.Index)
                {
                    case 100001:
                        enemyModels[0].SetActive(true);
                        break;
                    case 100002:
                        enemyModels[1].SetActive(true);
                        break;
                    case 100003:
                        enemyModels[2].SetActive(true);
                        break;
                    case 100004:
                        enemyModels[3].SetActive(true);
                        break;
                    case 100005:
                        enemyModels[4].SetActive(true);
                        break;
                    case 100006:
                        enemyModels[5].SetActive(true);
                        break;
                    case 100007:
                        enemyModels[6].SetActive(true);
                        break;
                    case 100008:
                        enemyModels[7].SetActive(true);
                        break;
                    case 100009:
                        enemyModels[8].SetActive(true);
                        break;
                }

                StartCoroutine(SlotAnimation());
            }
        }

        IEnumerator SlotAnimation()
        {
            yield return new WaitForSeconds(0.2f);
            for (int i = 0; i < PlayerMatchingSlots.Length; i++)
            {
                var spell = GameDataManager.Instance.User.currentCharacter.Deck[i].Spell;
                PlayerMatchingSlots[i].Init(spell);
                spell = GameDataManager.Instance.User.EnemyUser.Spell[i].Spell;
                EnemyMatchingSlots[i].Init(spell);
                yield return new WaitForSeconds(0.0f);
            }
            IsBattleReady = true;
        }

        void OnClickBattle()
        {
            if (!IsBattleReady) return;
            ServerManager.Instance.BattleStart();
        }

        public void BattleStart()
        {
            // 인게임 진입연출 간략화, Sanghun
            //foreach (var model in playerModels)
            //    model.SetActive(false);
            //foreach (var model in enemyModels)
            //    model.SetActive(false);
            //GameEventManager.Instance.OnEventUIHide(true);
            ////canvas.enabled = false;
            //LobbyMainUI.Instance.LobbyCharacterAnimator.SetTrigger("LobbyToBattle");

            StartCoroutine(BattleStart());
            IEnumerator BattleStart()
            {
                GameDataManager.Instance.SetBattleAI();
                SceneLoadManager.Instance.LoadBattle();
                yield return null;
            }
        }

        public override void Close()
        {
            if (!canvas.enabled) return;
            StopAllCoroutines();
            base.Close();
        }

        #region Event

        public void OnClickNewEnemyUser()
        {
            ServerManager.Instance.NewEnemyUser();
        }

        #endregion

        #region EventHandler

        private void OnEventNewEnemyUser()
        {
            Open();
        }

        #endregion
    }
}
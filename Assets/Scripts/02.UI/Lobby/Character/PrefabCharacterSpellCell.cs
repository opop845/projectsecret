﻿using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Secret.UI
{
    public class PrefabCharacterSpellCell : MonoBehaviour
    {
        [SerializeField] Image imageSkill;
        [SerializeField] TextMeshProUGUI textSoul;

        public void Initialize(Spell spell)
        {
            imageSkill.SetSprite(spell.IconSprite);
            textSoul.SetText(spell.Soul);
        }

    }
}
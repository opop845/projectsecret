﻿using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Secret.UI
{
    public class PopupCharacterInfo : MonoBehaviour
    {
        private UserCharacter userCharacter;
        [SerializeField] List<PrefabCharacterSpellCell> spellCells;
        [SerializeField] TextMeshProUGUI textHP;
        [SerializeField] TextMeshProUGUI textAD;
        [SerializeField] TextMeshProUGUI textAS;
        [SerializeField] TextMeshProUGUI textMS;
        [SerializeField] TextMeshProUGUI textCC;
        [SerializeField] TextMeshProUGUI textCD;
        [SerializeField] TextMeshProUGUI textRR;

        LobbyCamController lobbyCamController; // Sanghun


        private void Awake()
        {
            lobbyCamController = GameObject.Find("LobbyCamController").GetComponent<LobbyCamController>();
        }


        public void Initialize(UserCharacter character)
        {
            userCharacter = character;
            for (int i = 0; i < userCharacter.CharacterSpell.Count; i++)
            {
                spellCells[i].Initialize(userCharacter.CharacterSpell[i]);
            }
            textHP.SetText(userCharacter.State.GetStatValue(AttrType.Hp));
            textAD.SetText(userCharacter.State.GetStatValue(AttrType.Attack));
            textAS.SetText(userCharacter.State.GetStatValue(AttrType.AttackSpeed));
            textMS.SetText(userCharacter.State.GetStatValue(AttrType.MoveSpeed));
            textCC.SetText(userCharacter.State.GetStatValue(AttrType.CriticalChance).ToString("p1"));
            textCD.SetText(userCharacter.State.GetStatValue(AttrType.CriticalDamage).ToString("p1"));
            textRR.SetText(userCharacter.State.GetStatValue(AttrType.HpRestorationRate).ToString("p1"));

            gameObject.SetActive(true);
        }

        public void Close()
        {
            if (lobbyCamController != null) lobbyCamController.SetLobbyCam("Character");
            if (gameObject.activeSelf) gameObject.SetActive(false);
        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Secret.Data;

namespace Secret.UI
{
    public class PrefabCharacter : EnhanceItem
    {
        [SerializeField] Image imageCharacter;
        [SerializeField] TextMeshProUGUI textLevel;
        [SerializeField] Image imageLock;
        [SerializeField] Image imageSelected;

        private UserCharacter userCharacter;

        private void OnDestroy()
        {
            GameEventManager.Instance.EventCharacterChanged -= OnEventCharacterChanged;
            GameEventManager.Instance.EventRewardTrophy -= OnEventRewardTrophy;
            GameEventManager.Instance.EventLobbyTabChanged -= OnEventLobbyTabChanged;
        }

        protected override void OnStart()
        {
            GameEventManager.Instance.EventCharacterChanged += OnEventCharacterChanged;
            GameEventManager.Instance.EventRewardTrophy += OnEventRewardTrophy;
            GameEventManager.Instance.EventLobbyTabChanged += OnEventLobbyTabChanged;
        }

        public void Init(UserCharacter character)
        {
            userCharacter = character;
            gameObject.SetActive(true);

            if (userCharacter.Level > 0) textLevel.SetText($"Lv.{userCharacter.Level}");
            else textLevel.SetText($"");              

            imageCharacter.SetSprite($"TabCharacterIcon_{userCharacter.CharacterIndex}");
            if (userCharacter == GameDataManager.Instance.User.currentCharacter)
            {
                imageSelected.SetActive(true);
            }
            else
                imageSelected.SetActive(false);
            imageLock.SetActive(userCharacter.IsLock);
        }

        public void OnClickCharacter()
        {
            OnClickEnhanceItem();
        }

        // Set the item "depth" 2d or 3d
        protected override void SetItemDepth(float depthCurveValue, int depthFactor, float itemCount)
        {
            int newDepth = (int)(depthCurveValue * itemCount);
            this.transform.SetSiblingIndex(newDepth);
        }

        public override void SetSelectState(bool isCenter)
        {
            if (imageCharacter.color == Color.gray && isCenter)
                GameEventManager.Instance.OnEventCharacterChanged(userCharacter);
            imageCharacter.color = isCenter ? Color.white : Color.gray;
         
        }

        #region EventHandler

        private void OnEventCharacterChanged(UserCharacter obj)
        {
            if (userCharacter == GameDataManager.Instance.User.currentCharacter)
                imageSelected.SetActive(true);
            else
                imageSelected.SetActive(false);
        }

        private void OnEventRewardTrophy()
        {
            textLevel.SetText(userCharacter.Level);
            if (userCharacter == GameDataManager.Instance.User.currentCharacter)
            {
                imageSelected.SetActive(true);
            }
            else
                imageSelected.SetActive(false);
            imageLock.SetActive(userCharacter.IsLock);
        }


        private void OnEventLobbyTabChanged(MainTabType type)
        {
            if (type != MainTabType.Character)
            {
                if (userCharacter == GameDataManager.Instance.User.currentCharacter)
                    OnClickEnhanceItem();
            }
        }

        #endregion
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Secret.Data;
using UnityEngine.UI;

namespace Secret.UI
{
    public class PopupCharacterLevelUp : MonoBehaviour
    {
        [SerializeField] GameObject abilityPointObject;
        [SerializeField] TextMeshProUGUI textLevel;
        [SerializeField] TextMeshProUGUI textNextLevel;
        [SerializeField] TextMeshProUGUI textGold;
        [SerializeField] TextMeshProUGUI textAbilityPoint;

        [SerializeField] GameObject spellObject;
        [SerializeField] TextMeshProUGUI textSoul;
        [SerializeField] Image imageSpell;

        private UserCharacter userCharacter;
        LobbyCamController lobbyCamController; // Sanghun

        private void Awake()
        {
            GameEventManager.Instance.EventCharacterExpertLevelUp += OnEventCharacterExpertLevelUp;
            lobbyCamController = GameObject.Find("LobbyCamController").GetComponent<LobbyCamController>();
        }

        private void OnDestroy()
        {
            GameEventManager.Instance.EventCharacterExpertLevelUp -= OnEventCharacterExpertLevelUp;
        }

        public void Initialize(UserCharacter character)
        {
            userCharacter = character;
            if (userCharacter != null)
            {
                textLevel.SetText(userCharacter.Level);
                if (userCharacter.NextLevel != null)
                {
                    textLevel.SetText(userCharacter.Level);
                    textNextLevel.SetText(userCharacter.NextLevel.Level);
                    textAbilityPoint.SetText($"x{userCharacter.NextLevel.RewardAbilityPoint}");
                    textGold.SetText(userCharacter.NextLevel.RequiredGold);
                    abilityPointObject.SetActive(true);

                    if (userCharacter.NextLevel.RewardSpell != null)
                    {
                        imageSpell.SetSprite(userCharacter.NextLevel.RewardSpell.IconSprite);
                        textSoul.SetText(userCharacter.NextLevel.RewardSpell.Soul);
                        spellObject.SetActive(true);
                    }
                    else
                    {
                        spellObject.SetActive(false);
                    }
                }
                else
                {
                    textLevel.SetText(userCharacter.Level);
                    textNextLevel.SetText("Max");
                    textAbilityPoint.SetText(0);
                    textGold.SetText(0);
                    abilityPointObject.SetActive(false);
                    spellObject.SetActive(false);
                }

                gameObject.SetActive(true);
            }
            else
                gameObject.SetActive(false);
        }

        public void Close()
        {
            if (lobbyCamController != null) lobbyCamController.SetLobbyCam("Character");
            if (gameObject.activeSelf) gameObject.SetActive(false);
        }

        #region Event

        public void OnClickLevelUpCharacter()
        {
            ServerManager.Instance.LevelUpCharacter(userCharacter);
        }

        #endregion

        #region EventHandler

        private void OnEventCharacterExpertLevelUp(UserCharacter character)
        {
            Initialize(character);
        }

        #endregion

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Secret.Data;
using System.Linq;

namespace Secret.UI
{
    public class PanelCharacter : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI textName;
        [SerializeField] TextMeshProUGUI textDesc;
        [SerializeField] TextMeshProUGUI textLevel;
        [SerializeField] TextMeshProUGUI textExp;
        [SerializeField] TextMeshProUGUI textBattlePower;
        //[SerializeField] Button btnSelect;
        //[SerializeField] Button btnLock;

        [SerializeField] ScrollRect characterScrollView;
        [SerializeField] PrefabCharacter prefabCharacter;

        [SerializeField] PopupCharacterLevelUp popupCharacterLevelUp;
        [SerializeField] PopupCharacterAbility popupCharacterAbility;
        [SerializeField] PopupCharacterInfo popupCharacterInfo;
        [SerializeField] PopupCharacterSkin popupCharacterSkin;
        [SerializeField] EnhanceScrollView scrollView;

        private UserCharacter userCharacter;
        private Canvas canvas;
        LobbyCamController lobbyCamController; // Sanghun

        private void Awake()
        {
            GameEventManager.Instance.EventCharacterChanged += OnEventCharacterChanged;
            GameEventManager.Instance.EventLobbyTabChanged += OnEventLobbyTabChanged;
            GameEventManager.Instance.EventCharacterExpertLevelUp += OnEventCharacterExpertLevelUp;
            GameEventManager.Instance.EventCharacterAbilityLevelUp += OnEventCharacterExpertLevelUp;
            canvas = GetComponent<Canvas>();
            canvas.enabled = false;
            lobbyCamController = GameObject.Find("LobbyCamController").GetComponent<LobbyCamController>();
        }


        private void OnDestroy()
        {
            GameEventManager.Instance.EventCharacterChanged -= OnEventCharacterChanged;
            GameEventManager.Instance.EventLobbyTabChanged -= OnEventLobbyTabChanged;
            GameEventManager.Instance.EventCharacterExpertLevelUp -= OnEventCharacterExpertLevelUp;
            GameEventManager.Instance.EventCharacterAbilityLevelUp -= OnEventCharacterExpertLevelUp;
        }
        public void Initialize()
        {
            int index = 0;
          var list=  GameDataManager.Instance.User.userCharactersDic.Values.OrderBy(x => x.Character.SortOrder).ToArray();

            foreach (var obj in list)
            {
                var prefab = Instantiate(prefabCharacter, scrollView.transform);
                prefab.Init(obj);
                scrollView.listEnhanceItems.Add(prefab);
                if (GameDataManager.Instance.User.currentCharacter == obj)
                    scrollView.startCenterIndex = index;
                index++;
            }

            var vector3 = scrollView.transform.localPosition;
            vector3.x = -(scrollView.listEnhanceItems.Count * scrollView.cellWidth) / 2f;
            scrollView.transform.localPosition = vector3;

            prefabCharacter.gameObject.SetActive(false);
            userCharacter = GameDataManager.Instance.User.currentCharacter;
            Display();
            gameObject.SetActive(true);
        }

        public void Display()
        {
            if (userCharacter != null)
            {
                textName.SetText(userCharacter.Character.Name);
                textDesc.SetText(userCharacter.Character.Desc);
                textLevel.SetText($"Lv.{userCharacter.Level}");
                if (userCharacter.NextLevel != null)
                    textExp.SetText($"{userCharacter.Exp}/{userCharacter.NextLevel.RequiredExp}");
                else
                    textExp.SetText($"{userCharacter.Exp}/0");
                textBattlePower.SetText(userCharacter.State.BattlePower.ToString("#,##0"));
                //if (userCharacter.IsLock)
                //{
                //    btnLock.SetActive(true);
                //    btnSelect.SetActive(false);
                //}
                //else
                //{
                //    btnLock.SetActive(false);
                //    btnSelect.SetActive(GameDataManager.Instance.User.currentCharacter != userCharacter);
                //}
            }
        }

        private void OnEventCharacterChanged(UserCharacter character)
        {
            userCharacter = character;
            Display();
        }

        #region Event

        public void OnClickCharacterSelected()
        {
            GameDataManager.Instance.User.currentCharacter = userCharacter;
        }

        public void OnClickCharacterLevelUpPopup()
        {
            popupCharacterLevelUp.Initialize(userCharacter);
            popupCharacterInfo.Close();
            popupCharacterAbility.Close();
            popupCharacterSkin.Close();
            lobbyCamController.SetLobbyCam("Character_Popup");
        }

        public void OnClickCharacterInfoPopup()
        {
            popupCharacterInfo.Initialize(userCharacter);
            popupCharacterLevelUp.Close();
            popupCharacterAbility.Close();
            popupCharacterSkin.Close();
            lobbyCamController.SetLobbyCam("Character_Popup");

        }

        public void OnClickCharacterAbilityPopup()
        {
            popupCharacterAbility.Initialize(userCharacter);
            popupCharacterInfo.Close();
            popupCharacterLevelUp.Close();
            popupCharacterSkin.Close();
            lobbyCamController.SetLobbyCam("Character_Popup");

        }

        public void OnClickCharacterSkinPopup()
        {
            popupCharacterSkin.Initialize(userCharacter);
            popupCharacterInfo.Close();
            popupCharacterLevelUp.Close();
            popupCharacterAbility.Close();
            lobbyCamController.SetLobbyCam("Character_Popup");
            //lobbyCamController.SetLobbyCam("Character");
        }

        public void OnClickExpUp()
        {
            ServerManager.Instance.ToolExpUp(userCharacter);
        }

        #endregion


        #region EventHandler

        private void OnEventLobbyTabChanged(MainTabType type)
        {
            if (type == MainTabType.Character)
            {
                lobbyCamController.SetLobbyCam("Character");
                canvas.enabled = true;
            }
            else
            {
                canvas.enabled = false;
                popupCharacterLevelUp.Close();
                popupCharacterAbility.Close();
                popupCharacterInfo.Close();
                popupCharacterSkin.Close();
                lobbyCamController.SetLobbyCam("Lobby");
            }
        }

        private void OnEventCharacterExpertLevelUp(UserCharacter character)
        {
            userCharacter = character;
            Display();
        }

        #endregion


    }
}
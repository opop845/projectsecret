﻿using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Secret.UI
{
    public class PopupCharacterAbility : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI[] textAbilitysLevel;
        [SerializeField] TextMeshProUGUI textAbilityName;
        [SerializeField] TextMeshProUGUI textAbilityDesc;
        [SerializeField] TextMeshProUGUI textAbilityRemainAbility;
        [SerializeField] TextMeshProUGUI textAbilityLevelUpAbility;
        [SerializeField] Button btnLevelUp; 

        private UserCharacter userCharacter;
        private int currentIndex;
        LobbyCamController lobbyCamController; // Sanghun


        private void Awake()
        {
            GameEventManager.Instance.EventCharacterAbilityLevelUp += OnEventCharacterAbilityLevelUp;
            lobbyCamController = GameObject.Find("LobbyCamController").GetComponent<LobbyCamController>();
            Debug.Log(lobbyCamController);
        }

        private void OnDestroy()
        {
            GameEventManager.Instance.EventCharacterAbilityLevelUp -= OnEventCharacterAbilityLevelUp;
        }

        public void Initialize(UserCharacter character)
        {
            userCharacter = character;
            currentIndex = 0;
            DisplayAbility();
            gameObject.SetActive(true);
        }

        public void Close()
        {
            if (lobbyCamController != null) lobbyCamController.SetLobbyCam("Character");
            if (gameObject.activeSelf) gameObject.SetActive(false);
        }

        public void DisplayAbility()
        {
            if(userCharacter != null)
            {
                for (int i = 0; i < userCharacter.CharacterAbilities.Count; i++)
                {
                    textAbilitysLevel[i].SetText($"Lv.{userCharacter.CharacterAbilities[i].Level}");
                }

                textAbilityRemainAbility.SetText(userCharacter.RemainAbilityPoint);
                textAbilityLevelUpAbility.SetText(userCharacter.CharacterAbilities[currentIndex].characterAbility.RequiredAbilityPoint);
                textAbilityName.SetText(userCharacter.CharacterAbilities[currentIndex].characterAbility.Name);
                textAbilityDesc.SetText(userCharacter.CharacterAbilities[currentIndex].characterAbility.Desc);
                btnLevelUp.interactable = userCharacter.RemainAbilityPoint >= userCharacter.CharacterAbilities[currentIndex].characterAbility.RequiredAbilityPoint;
            }
        }

        #region Event


        public void OnClickAbility(int index)
        {
            currentIndex = index;
            DisplayAbility();
        }

        public void OnClickLevelUpAbility()
        {
            if(userCharacter != null)
            {
                ServerManager.Instance.LevelUpAbility(userCharacter, userCharacter.CharacterAbilities[currentIndex]);  
            }
        }

        #endregion

        #region EventHandler

        void OnEventCharacterAbilityLevelUp(UserCharacter character)
        {
            userCharacter = character;
            DisplayAbility();
        }

        #endregion
    }
}
﻿using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.UI
{
    public class PopupCharacterSkin : MonoBehaviour
    {
        private UserCharacter userCharacter;
        LobbyCamController lobbyCamController; // Sanghun


        private void Awake()
        {
            lobbyCamController = GameObject.Find("LobbyCamController").GetComponent<LobbyCamController>();
        }

        public void Initialize(UserCharacter character)
        {
            userCharacter = character;
            gameObject.SetActive(true);
        }
        public void Close()
        {
            if (lobbyCamController != null) lobbyCamController.SetLobbyCam("Character");
            if (gameObject.activeSelf) gameObject.SetActive(false);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.UI
{
    public class PanelShop : MonoBehaviour
    {
        private Canvas canvas;
        private void Awake()
        {
            GameEventManager.Instance.EventLobbyTabChanged += OnEventLobbyTabChanged;
            canvas = GetComponent<Canvas>();
            canvas.enabled = false;
        }

        private void OnDestroy()
        {
            GameEventManager.Instance.EventLobbyTabChanged -= OnEventLobbyTabChanged;
        }

        public void Initialize()
        {
            gameObject.SetActive(true);
        }

        #region EventHandler

        private void OnEventLobbyTabChanged(MainTabType type)
        {
            if (type == MainTabType.Shop)
                canvas.enabled = true;
            else
                canvas.enabled = false;
        }

        #endregion
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Secret.Data;
using I2.Loc;

namespace Secret.UI
{
    public class PanelBattle : MonoSingleton<PanelBattle>
    {
        [SerializeField] TextMeshProUGUI textLevel;
        [SerializeField] TextMeshProUGUI textName;
        [SerializeField] TextMeshProUGUI textBattlePower;
        LobbyCamController lobbyCamController; // Sanghun

        private Canvas canvas;

        private void Awake()
        {
            canvas = GetComponent<Canvas>();
            GameEventManager.Instance.EventCharacterChanged += OnEventCharacterChanged;
            GameEventManager.Instance.EventLobbyTabChanged += OnEventLobbyTabChanged;
            LocalizationManager.OnLocalizeEvent += OnLocalizeEvent;
            lobbyCamController = GameObject.Find("LobbyCamController").GetComponent<LobbyCamController>();
        }



        private void OnDestroy()
        {
            GameEventManager.Instance.EventCharacterChanged -= OnEventCharacterChanged;
            GameEventManager.Instance.EventLobbyTabChanged -= OnEventLobbyTabChanged;
            LocalizationManager.OnLocalizeEvent -= OnLocalizeEvent;
        }

        public void Initialize()
        {
            var userCharacter = GameDataManager.Instance.User.currentCharacter;
            textLevel.SetText($"Lv.{userCharacter.Level}");
            textName.SetText(userCharacter.Character.Name);
            textBattlePower.SetText($"{userCharacter.State.BattlePower:#,##0}");
            gameObject.SetActive(true);
        }


        #region Event

        public void OnClickCharacterSelect(bool isRight)
        {
            if (isRight)
                GameDataManager.Instance.User.NextCharacter();
            else
                GameDataManager.Instance.User.PreCharacter();

        }

        public void OnClickBattle()
        {
            if (GameDataManager.Instance.User.currentCharacter.GetBattleCheck())
            {
                PopupMatching.Instance.Open();
            }
            else
            {
                GameEventManager.Instance.OnEventToast("덱을 셋팅하세요");
            }
        }

        public void OnClickMissionPopup()
        {
            PopupDailyQuest.Instance.Open();
        }

        public void OnClickRankingPopup()
        {
            PopupRanking.Instance.Open();
        }


        #endregion

        #region EventHandler

        void OnEventCharacterChanged(UserCharacter userCharacter)
        {
            textLevel.SetText($"Lv.{userCharacter.Level}");
            textName.SetText(userCharacter.Character.Name);
            textBattlePower.SetText($"{userCharacter.State.BattlePower:#,##0}");
        }

        private void OnEventLobbyTabChanged(MainTabType type)
        {
            if (type == MainTabType.Battle)
            {
                canvas.enabled = true;
                lobbyCamController.SetLobbyCam("Lobby");
            }
            else
                canvas.enabled = false;
        }

        private void OnLocalizeEvent()
        {
            if (GameDataManager.Instance.User != null)
            {
                var userCharacter = GameDataManager.Instance.User.currentCharacter;
                textLevel.SetText($"Lv.{userCharacter.Level}");
                textName.SetText(userCharacter.Character.Name);
                textBattlePower.SetText($"{userCharacter.State.BattlePower:#,##0}");
            }
        }


        #endregion
    }
}
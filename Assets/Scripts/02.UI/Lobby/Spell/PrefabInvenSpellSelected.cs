﻿using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using I2.Loc;

namespace Secret
{
    public class PrefabInvenSpellSelected : MonoBehaviour
    {
        [SerializeField] Image imageItem;
        [SerializeField] Image imageSkill;
        [SerializeField] Image imageSkillIndicator;
        [SerializeField] TextMeshProUGUI textSoul;
        [SerializeField] TextMeshProUGUI textLevel;

        [SerializeField] Button btnInfo;
        [SerializeField] Button btnDeckIn;
        [SerializeField] Button btnDeckOut;

        private UserSpell userSpell;
        private PrefabInvenSpell targetPrefab;
        private void OnEnable()
        {
            GameEventManager.Instance.EventDeckSpellChanged += OnEventDeckSpellChanged;
        }
        private void OnDisable()
        {
            GameEventManager.Instance.EventDeckSpellChanged -= OnEventDeckSpellChanged;
        }

        public void Initialize(PrefabInvenSpell prefabInvenSpell)
        {
            userSpell = prefabInvenSpell.userSpell;
            targetPrefab = prefabInvenSpell;
            if (userSpell == null) return;

            textSoul.SetText(userSpell.Spell.Soul);
            textLevel.SetText($"{LocalizationManager.GetTranslation("common_level")} {userSpell.Level}");

            if (userSpell.Spell.SpellType == SpellType.Item)
            {
                imageItem.SetActive(true);
                imageItem.SetSprite(userSpell.Spell.Item.IconSprite);
                imageSkill.SetActive(false);
                imageSkillIndicator.SetActive(false);
            }
            else
            {
                imageItem.SetActive(false);
                imageSkill.SetSprite(userSpell.Spell.Skill.IconSprite);
                imageSkill.SetActive(true);
                imageSkillIndicator.SetActive(true);
            }
            transform.SetParent(targetPrefab.transform.parent);
            transform.position = targetPrefab.transform.position;
            transform.SetAsLastSibling();
            transform.GetComponent<RectTransform>().pivot = targetPrefab.GetComponent<RectTransform>().pivot;
            if (GameDataManager.Instance.User.currentCharacter.Deck.ContainsValue(userSpell))
            {
                btnDeckIn.SetActive(false);
                btnDeckOut.SetActive(true);
            }
            else
            {
                btnDeckIn.SetActive(true);
                btnDeckOut.SetActive(false);
            }
            gameObject.SetReactive(true);
        }

        #region Event

        public void OnClickClose()
        {
            gameObject.SetActive(false);
        }

        public void OnClickSpellInfo()
        {
            if (userSpell != null)
                PopupSpellDetail.Instance.OpenSpellInfo(userSpell);
            OnClickClose();
        }

        public void OnClickSpellDeckIn()
        {
            GameDataManager.Instance.User.SetDeckInSpell(userSpell);
            OnClickClose();
        }

        public void OnClickSpellDeckOut()
        {
            GameDataManager.Instance.User.SetDeckOutSpell(userSpell);
            OnClickClose();
        }

        #endregion

        #region EventHandler

        void OnEventDeckSpellChanged()
        {
            if (userSpell == null) return;
            if (targetPrefab == null) return;
            transform.SetParent(targetPrefab.transform.parent);
            transform.position = targetPrefab.transform.position;
            transform.GetComponent<RectTransform>().pivot = targetPrefab.GetComponent<RectTransform>().pivot;
            transform.SetAsLastSibling();
            gameObject.SetReactive(true);
        }

        #endregion
    }
}
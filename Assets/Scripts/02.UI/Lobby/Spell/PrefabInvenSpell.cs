﻿using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using I2.Loc;

namespace Secret
{
    public class PrefabInvenSpell : MonoBehaviour
    {
        [SerializeField] GameObject bgObject;
        [SerializeField] Image imageItem;
        [SerializeField] Image imageSkill;
        [SerializeField] Image imageSkillIndicator;
        [SerializeField] TextMeshProUGUI textSoul;
        [SerializeField] TextMeshProUGUI textLevel;
        [SerializeField] TextMeshProUGUI textSpellCount;
        [SerializeField] PrefabInvenSpellSelected spellSelected;
        [SerializeField] Slider sliderCount;
        public UserSpell userSpell { get; private set; }

        private void Awake()
        {
            LocalizationManager.OnLocalizeEvent += OnLocalizeEvent;

        }

        private void OnDestroy()
        {
            LocalizationManager.OnLocalizeEvent -= OnLocalizeEvent;
        }

        public void Initialize(UserSpell target)
        {
            userSpell = target;
            if (userSpell == null)
                bgObject.SetActive(false);
            else
            {
                transform.name = userSpell.Index.ToString();
                bgObject.SetActive(true);
                textSoul.SetText(userSpell.Spell.Soul);
                textLevel.SetText($"{userSpell.Level}");
                var nextLevel = GameDataManager.Instance.GetSpellLevel(userSpell.Level + 1);
                if (nextLevel != null)
                {
                    sliderCount.maxValue = nextLevel.RequiredSpellCardAmount;
                    sliderCount.value = userSpell.Count;
                    textSpellCount.SetText($"{userSpell.Count}/{nextLevel.RequiredSpellCardAmount}");
                }
                else
                {
                    sliderCount.value = sliderCount.maxValue = userSpell.Count;
                    textSpellCount.SetText($"{userSpell.Count}/0");
                }

                if (userSpell.Spell.SpellType == SpellType.Item)
                {
                    imageItem.SetActive(true);
                    imageItem.SetSprite(userSpell.Spell.Item.IconSprite);
                    imageSkill.SetActive(false);
                    imageSkillIndicator.SetActive(false);
                }
                else
                {
                    imageItem.SetActive(false);
                    imageSkill.SetSprite(userSpell.Spell.Skill.IconSprite);
                    imageSkill.SetActive(true);
                    imageSkillIndicator.SetActive(true);
                }
            }
        }

        #region Event

        public void OnClickSpell()
        {
            spellSelected.Initialize(this);
        }

        #endregion

        #region EventHandler

        void OnLocalizeEvent()
        {
            Initialize(userSpell);
        }

        #endregion
    }
}
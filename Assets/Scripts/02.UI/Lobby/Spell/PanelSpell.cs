﻿using SuperScrollView;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Secret.Data;

namespace Secret.UI
{
    public class PanelSpell : MonoBehaviour
    {
        [SerializeField] LoopGridView gridViewInven;
        [SerializeField] PrefabInvenSpell prefabInvenSpell;
        [SerializeField] Transform deckListTransform;
        [SerializeField] TextMeshProUGUI textAverageSoul;

        [SerializeField] GameObject[] temps;

        List<PrefabInvenSpell> prefabInvenSpells = new List<PrefabInvenSpell>();
        private Canvas canvas;
        void Awake()
        {
            GameEventManager.Instance.EventDeckSpellChanged += OnDeckUpdate;
            GameEventManager.Instance.EventCharacterChanged += OnEventCharacterChanged;
            GameEventManager.Instance.EventCharacterExpertLevelUp += OnEventCharacterExpertLevelUp;
            GameEventManager.Instance.EventRewardTrophy += OnDeckUpdate;
            GameEventManager.Instance.EventLobbyTabChanged += OnEventLobbyTabChanged;
            canvas = GetComponent<Canvas>();
            canvas.enabled = false;
        }


        private void OnDestroy()
        {
            GameEventManager.Instance.EventDeckSpellChanged -= OnDeckUpdate;
            GameEventManager.Instance.EventCharacterChanged -= OnEventCharacterChanged;
            GameEventManager.Instance.EventCharacterExpertLevelUp -= OnEventCharacterExpertLevelUp;
            GameEventManager.Instance.EventRewardTrophy -= OnDeckUpdate;
            GameEventManager.Instance.EventLobbyTabChanged -= OnEventLobbyTabChanged;
        }

        #region EventHandler


        public void Initialize()
        {
            gridViewInven.InitGridView(GameDataManager.Instance.User.remainSpells.Count > 3 ? GameDataManager.Instance.User.remainSpells.Count : 4, OnGetSpellByRowColumn);

            for (int i = 0; i < 8; i++)
            {
                prefabInvenSpells.Add(Instantiate(prefabInvenSpell, deckListTransform));
            }
            for (int i = 0; i < GameDataManager.Instance.User.currentCharacter.Deck.Count; i++)
            {
                prefabInvenSpells[i].Initialize(GameDataManager.Instance.User.currentCharacter.Deck[i]);
            }
            textAverageSoul.SetText($"{GameDataManager.Instance.User.GetUserDeckSoulAverage():0.0}");
            prefabInvenSpell.gameObject.SetActive(false);
            gameObject.SetActive(true);
        }

        private void OnEventCharacterChanged(UserCharacter userCharacter)
        {
            OnDeckUpdate();
        }

        private void OnDeckUpdate()
        {
            for (int i = 0; i < GameDataManager.Instance.User.currentCharacter.Deck.Count; i++)
            {
                prefabInvenSpells[i].Initialize(GameDataManager.Instance.User.currentCharacter.Deck[i]);
            }

            if (gridViewInven.ItemTotalCount != GameDataManager.Instance.User.remainSpells.Count)
                gridViewInven.SetListItemCount(GameDataManager.Instance.User.remainSpells.Count > 3 ? GameDataManager.Instance.User.remainSpells.Count : 4, false);

            gridViewInven.RefreshAllShownItem();
            textAverageSoul.SetText($"{GameDataManager.Instance.User.GetUserDeckSoulAverage():0.0}");
        }

        private void OnEventCharacterExpertLevelUp(UserCharacter character)
        {
            OnDeckUpdate();
        }

        private void OnEventLobbyTabChanged(MainTabType type)
        {
            if (type == MainTabType.Spell)
            {
                canvas.enabled = true;
                foreach (var obj in temps)
                {
                    obj.SetActive(false);
                }
            }
            else
            {
                canvas.enabled = false;
                foreach (var obj in temps)
                {
                    obj.SetActive(true);
                }
            }
        }


        #endregion


        LoopGridViewItem OnGetSpellByRowColumn(LoopGridView gridView, int itemIndex, int row, int column)
        {
            if (itemIndex >= GameDataManager.Instance.User.remainSpells.Count)
                return null;
            LoopGridViewItem item = gridView.NewListViewItem("PrefabInvenSpell");

            var itemScript = item.GetComponent<PrefabInvenSpell>();//get your own component
            // IsInitHandlerCalled is false means this item is new created but not fetched from pool.
            if (item.IsInitHandlerCalled == false)
            {
                item.IsInitHandlerCalled = true;
            }
            //update the item’s content for showing, such as image,text.
            itemScript.Initialize(GameDataManager.Instance.User.remainSpells[itemIndex]);
            return item;
        }
    }
}
﻿using Secret.Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Secret.UI
{
    public class PrefabTrophySpell : MonoBehaviour
    {
        [SerializeField] GameObject bgObject;
        [SerializeField] Image imageItem;
        [SerializeField] Image imageSkill;
        [SerializeField] Image imageSkillIndicator;
        [SerializeField] TextMeshProUGUI textSoul;
        public Spell Spell { get; private set; }

        public void Initialize(Spell target)
        {
            Spell = target;
            if (Spell == null)
                gameObject.SetActive(false);
            else
            {
                bgObject.SetActive(true);
                textSoul.SetText(Spell.Soul);

                if (Spell.SpellType == SpellType.Item)
                {
                    imageItem.SetActive(true);
                    imageItem.SetSprite(Spell.Item.IconSprite);
                    imageSkill.SetActive(false);
                    imageSkillIndicator.SetActive(false);
                }
                else
                {
                    imageItem.SetActive(false);
                    imageSkill.SetSprite(Spell.Skill.IconSprite);
                    imageSkill.SetActive(true);
                    imageSkillIndicator.SetActive(true);
                }
                gameObject.SetActive(true);
            }
        }
    }
}
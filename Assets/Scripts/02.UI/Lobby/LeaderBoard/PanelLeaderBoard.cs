﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret.UI
{
    public class PanelLeaderBoard : MonoBehaviour
    {
        private Canvas canvas;
        public GameObject PetModel;

        private void Awake()
        {
            GameEventManager.Instance.EventLobbyTabChanged += OnEventLobbyTabChanged;
            canvas = GetComponent<Canvas>();
            canvas.enabled = false;
        }

        private void OnDestroy()
        {
            GameEventManager.Instance.EventLobbyTabChanged -= OnEventLobbyTabChanged;
        }

        public void Initialize()
        {
            gameObject.SetActive(true);
            PetModel.SetActive(false);
        }

        #region EventHandler

        private void OnEventLobbyTabChanged(MainTabType type)
        {
            if (type == MainTabType.LeaderBoard)
            {
                canvas.enabled = true;
                PetModel.SetActive(true);
            }
            else
            {
                canvas.enabled = false;
                PetModel.SetActive(false);
            }
        }

        #endregion
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Secret
{
    public class Toast : MonoSingleton<Toast>
    {
        [SerializeField] GameObject toastObject;
        [SerializeField] TextMeshProUGUI textValue;
        private void Awake()
        {
            GameEventManager.Instance.EventToast += OnEventToast;
        }

        private void OnDestroy()
        {
            GameEventManager.Instance.EventToast -= OnEventToast;
        }

        private void OnEventToast(string value)
        {
            textValue.SetText(value);
            toastObject.SetReactive(true);

            StopAllCoroutines();
            StartCoroutine(TimeOut());

            IEnumerator TimeOut()
            {
                yield return new WaitForSeconds(2f);
                toastObject.SetActive(false);
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Secret.Data;
using TMPro;

namespace Secret
{
    public class PrefabMatchingSlot : MonoBehaviour
    {
        [SerializeField, GetComponentInChildrenName("ItemBackground")] DOTweenAnimation ItemBackground;
        [SerializeField, GetComponentInChildrenName("ImageSkill")] Image ImageSkill;
        [SerializeField, GetComponentInChildrenName("ImageItem")] Image ImageItem;
        [SerializeField, GetComponentInChildrenName("SlotBG")] Image SlotBG;
        [SerializeField, GetComponentInChildrenName("TextPrice")] TextMeshProUGUI TextPrice;
        [SerializeField, GetComponentInChildrenName("SkillIndicator")] GameObject SkillIndicator; 

        public void Init(Spell spell)
        {
            if (spell.SpellType == SpellType.Item)
            {
                ImageItem.SetSprite(spell.Item.IconSprite);
                ImageSkill.SetActive(false);
                ImageItem.SetActive(true);
                SkillIndicator.SetActive(false);
            }
            else
            {
                ImageSkill.SetSprite(spell.Skill.IconSprite);
                ImageSkill.SetActive(true);
                ImageItem.SetActive(false);
                SkillIndicator.SetActive(true);
            }

            TextPrice.SetText(spell.Soul);
            SlotBG.SetActive(false);
            ItemBackground.SetActive(true);
        }
        private void OnDisable()
        {
            SlotBG.SetActive(true);
            ItemBackground.SetActive(false);
        }
    }
}
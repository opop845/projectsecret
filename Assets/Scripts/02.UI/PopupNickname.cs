﻿using Secret;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupNickname : BaseUI<PopupNickname>
{
    public TMP_InputField inputID;
    public Button ButtonConfirm;

    protected override void Initialize()
    {
        base.Initialize();
        ButtonConfirm.OnClick(OnClickConfirm);
    }

    public override void Close()
    {
        if (!string.IsNullOrEmpty(GameDataManager.Instance.User.NickName))
            base.Close();
    }

    #region Event

    void OnClickConfirm()
    {
        if (!string.IsNullOrEmpty(inputID.text))
            ServerManager.Instance.SetNickName(inputID.text);
    }

    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using I2.Loc;

namespace Secret
{
    public abstract class BaseUI<T> : BaseUI where T : BaseUI<T>
    {
        private static T _instance;
        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = UIManager.Instance.CreateInstance<T>();
                }
                return _instance;
            }
        }

        protected virtual void Awake()
        {
            LocalizationManager.OnLocalizeEvent += OnLocalizeEvent;
            _instance = (T)this;
            _instance.name = typeof(T).Name;
            _instance.canvas = GetComponent<Canvas>();

            if (isDim)
            {
                var obj = Instantiate(UIManager.Instance.DimObject, _instance.transform);
                obj.transform.SetAsFirstSibling();
            }
        }

        protected virtual void OnDestroy()
        {
            LocalizationManager.OnLocalizeEvent -= OnLocalizeEvent;
            _instance = null;
        }

        protected virtual void OnEnable() { }

        protected virtual void OnDisable() { }

        protected virtual void OnLocalizeEvent() { }
        protected virtual void CheckUI()
        {
        }
    }

    public abstract class BaseUI : MonoBehaviour
    {
        public Canvas canvas { get; set; }
        public Button ButtonClose;
        public bool isDim = false;
        public bool isProfileHide = false;
        public LocalizedString UIName { get; protected set; }
        private bool isInit = false;

        protected virtual void Initialize()
        {
            isInit = true;
            if (ButtonClose != null) ButtonClose.OnClick(Close);
        }

        public virtual void Open()
        {
            if (!isInit) Initialize();
            UIManager.Instance.PushUI(this);
            this.SetActive(true);
        }

        public virtual void Close()
        {
            UIManager.Instance.PopUI(this);
            this.SetActive(false);
        }

        public virtual void ForceClose()
        {
            UIManager.Instance.PopUI(this);
            this.SetActive(false);
        }
    }
}
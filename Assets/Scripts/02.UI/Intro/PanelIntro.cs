﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Secret
{
    public class PanelIntro : MonoBehaviour
    {
        private void Start()
        {
            SceneLoadManager.Instance.LoadIntro();
        }
    }
}
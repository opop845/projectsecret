﻿using Secret;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupState : BaseUI<PopupState>
{
    [SerializeField, GetComponentInChildrenName("Text")] TextMeshProUGUI Text;

    private PopupStateType currentType;
    public void SetActive(bool isOn, string text = "")
    {
        Text.SetText(text);
        gameObject.SetActive(isOn);
    }

    public void OpenState(PopupStateType type)
    {
        currentType = type;
        switch (currentType)
        {
            case PopupStateType.Login:
                Text.SetText("Login now...");
                ButtonClose.SetActive(false);
                break;
            case PopupStateType.Macting:
                Text.SetText("Battle Starts in Seconds...");
                ButtonClose.SetActive(true);
                break;
            default:
                return;
        }

        Open();
    }

    public override void Close()
    {
        switch (currentType)
        {
            case PopupStateType.Login:
                return;
            case PopupStateType.Macting:
                ServerManager.Instance.OnDisConnect();
                base.Close();
                break;
        }
    }

    public void ForceClose()
    {
        base.Close();
    }

}
public enum PopupStateType
{
    Login,
    Macting
}

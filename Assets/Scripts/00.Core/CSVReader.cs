﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DG.Tweening.Plugins;
using PlayFab.Json;

namespace Secret
{
    public class CSVReader
    {
        static string SPLIT_RE = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
        static string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";
        static char[] TRIM_CHARS = { '\"' };

        public static Dictionary<string, string> LocalDataRead()
        {
            var dic = new Dictionary<string, string>();

            var files = Resources.LoadAll<TextAsset>("LocalData");
            foreach (var file in files)
            {
                if (file.name == "GameRule")
                {
                    dic.Add(file.name, file.text);
                }
                else
                {
                    var list2 = new List<object>();
                    var lines = Regex.Split(file.text, LINE_SPLIT_RE);
                    var types = Regex.Split(lines[0], SPLIT_RE);
                    var header = Regex.Split(lines[1], SPLIT_RE);

                    for (var i = 2; i < lines.Length; i++)
                    {
                        var values = Regex.Split(lines[i], SPLIT_RE);
                        if (values.Length == 0 || values[0] == "") continue;
                        list2.Add(ParseData(types, header, values));
                    }
                    dic.Add(file.name, PlayFabSimpleJson.SerializeObject(list2));
                }
            }
            return dic;
        }

        public static Dictionary<string, object> ParseData(string[] types, string[] headers, string[] values)
        {
            var datas = new Dictionary<string, object>();
            for (int i = 0; i < values.Length; i++)
            {
                if(string.IsNullOrEmpty( values[i]))
                {
                    continue;
                }
                values[i] = values[i].Replace("\"", "");
                switch (types[i])
                {
                    case "int":
                        datas.Add(headers[i], int.Parse(values[i]));
                        break;
                    case "float":
                        datas.Add(headers[i], float.Parse(values[i]));
                        break;
                    case "string":
                        datas.Add(headers[i], values[i]);
                        break;
                    case "bool":
                        datas.Add(headers[i], bool.Parse(values[i]));
                        break;
                    case "array_int":
                        datas.Add(headers[i], Array.ConvertAll(values[i].Split(','), int.Parse));
                        break;
                    case "array_float":
                        datas.Add(headers[i], Array.ConvertAll(values[i].Split(','), float.Parse));
                        break;
                    case "array_string":
                        datas.Add(headers[i], values[i].Split(','));
                        break;
                    case "array_bool":
                        datas.Add(headers[i], Array.ConvertAll(values[i].Split(','), bool.Parse));
                        break;
                    default:
                        datas.Add(headers[i], values[i]);
                        break;
                }
            }
            return datas;
        }
    }
}
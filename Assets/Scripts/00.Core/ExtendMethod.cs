﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public static class ExtendMethod
{
    public static string SetParameter(this string target, string parameter,string value)
    {
        return target.Replace($"{{{parameter}}}", value);
    }
    public static bool IsNull(this Component component) => component != null ? true : false;
    public static void SetActive(this Component component, bool isOn)
    {
        if (component == null)
        {
            Debug.LogError("Component가 존재하지 않습니다.");
            return;
        }
        if (component.gameObject.activeSelf != isOn)
            component.gameObject.SetActive(isOn);
    }

    public static void SetReactive(this Component component, bool isOn)
    {
        if (component == null)
        {
            Debug.LogError("Component가 존재하지 않습니다.");
            return;
        }
        component.gameObject.SetActive(!isOn);
        component.gameObject.SetActive(isOn);
    }

    public static void SetReactive(this GameObject component, bool isOn)
    {
        if (component == null)
        {
            Debug.LogError("Component가 존재하지 않습니다.");
            return;
        }
        component.SetActive(!isOn);
        component.SetActive(isOn);
    }

    public static void OnClick(this Button btn, UnityAction action) => btn.onClick.AddListener(action);
    public static void OnValueChanged(this Toggle toggle, UnityAction<bool> action) => toggle.onValueChanged.AddListener(action);

    public static void SetSprite(this Image image, string name)
    {
        image.sprite = ResourceManager.Instance.LoadSprtie(name);
    }
    public static void SetSprite(this Image image, Sprite sprtie)
    {
        if (image != null && sprtie != null)
            image.sprite = sprtie;
    }

    public static void Swap<T>(T a, T b)
    {
        T temp = b;
        b = a;
        a = temp;
    }

    public static void Swap(ref int a, ref int b)
    {
        int temp = b;
        b = a;
        a = temp;
    }

    public static bool Swap<T>(this T[] objectArray, int x, int y)
    {
        // check for out of range
        if (objectArray.Length <= y || objectArray.Length <= x) return false;

        // swap index x and y
        T buffer = objectArray[x];
        objectArray[x] = objectArray[y];
        objectArray[y] = buffer;

        return true;
    }

    #region Array

    public static T RandomValue<T>(this IList<T> list)
    {
        if (list.Count > 0)
            return list[UnityEngine.Random.Range(0, list.Count)];
        else
            return default(T);
    }

    #endregion

    #region List

    public static void Shuffle<T>(this IList<T> list)
    {
        System.Random rng = new System.Random();
        int n = list.Count;
        while (n > 0)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static List<int> ParseIntListFromString(this string value, char split = ',')
    {
        var list = new List<int>();
        if (string.IsNullOrEmpty(value)) return list;
        string[] valueArray = value.Split(split);

        for (int i = 0; i < valueArray.Length; i++)
            list.Add(Convert.ToInt32(valueArray[i]));
        return list;
    }

    #endregion


    #region TMP

    public static void SetText(this TextMeshProUGUI text, int value)
    {
        if (text != null) { text.SetText(value.ToString()); }
    }

    public static void SetText(this TextMeshProUGUI text, long value)
    {
        if (text != null) { text.SetText(value.ToString()); }
    }

    public static void SetText(this TextMeshProUGUI text, float value)
    {
        if (text != null) { text.SetText(value.ToString()); }
    }


    #endregion

    #region Enum

    public static T ParseToEnum<T>(this string value)
    {
        return (T)Enum.Parse(typeof(T), value);
    }

    public static T[] ParseToEnums<T>(this string[] value)
    {
        T[] array = new T[value.Length];
        for (int i = 0; i < value.Length; i++)
            array[i] = value[i].ParseToEnum<T>();
        return array;
    }

    #endregion
}

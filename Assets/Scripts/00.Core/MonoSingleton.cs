﻿using UnityEngine;
using System.Collections;

public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance = null;
    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<T>() as T;

                if (_instance == null)
                {
                    var temp= new GameObject(typeof(T).Name);
                    _instance= (T)temp.AddComponent(typeof(T));
                }
            }

            return _instance;
        }
    }
}

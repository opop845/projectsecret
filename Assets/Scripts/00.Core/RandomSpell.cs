﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Secret.Data
{
    public class RandomSpell
    {
        private List<Spell> SpellList = new List<Spell>();
        public Spell[] ShopSpells = new Spell[3] { null, null, null };
        private int index = 0;

        public RandomSpell(List<UserSpell> spells)
        {
            for (int i = 0; i < spells.Count; i++)
            {
              var spell=  GameDataManager.Instance.GetSpell(spells[i].Index);
                for (int j = 0; j < spell.DrawWeight; j++)
                    SpellList.Add(spell);
            }

            SpellList.Shuffle();

            for (int i = 0; i < 3; i++)
                ShopSpells[i] = GetSpell();
        }

        //public RandomSpell(List<Spell> spells)
        //{
        //    SpellList = new List<Spell>(spells);
        //    SpellList.Shuffle();

        //    for (int i = 0; i < 3; i++)
        //        ShopSpells[i] = GetSpell();
        //}

        Spell GetSpell()
        {
            if (index >= SpellList.Count)
                index = 0;
            return SpellList[index++];
        }

        public void PurchaseItem(int index)
        {
            ShopSpells[index] = GetSpell();
        }

        public void Refresh()
        {
            for (int i = 0; i < 3; i++)
                ShopSpells[i] = GetSpell();
        }
    }
}
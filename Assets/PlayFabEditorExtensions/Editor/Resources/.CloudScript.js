//유저 데이터 키
var KEY_USER_BATTLE_INFO = "BattleInfo";
var KEY_USER_INFO = "UserInfo";
var KEY_USER_CHARACTER = "Character";
var KEY_USER_SPELL = "Spell";
var KEY_USER_ENEMYUSER = "EnemyUser";

//게임 데이터 키
var KEY_AI_USER = "AIUser";
var KEY_BUFF = "Buff";
var KEY_CHARACTER = "Character";
var KEY_CHARACTER_ABILITY = "CharacterAbility";
var KEY_CHARACTER_BATTLE_LEVEL = "CharacterBattleLevel";
var KEY_CHARACTER_EXPERT_LEVEL = "CharacterExpertLevel";
var KEY_GAMERULE = "GameRule";
var KEY_ITEM = "Item";
var KEY_MAP = "Map";
var KEY_MONSTER = "Monster";
var KEY_REWARD_CHEST = "RewardChest";
var KEY_SKILL = "Skill";
var KEY_SPELL = "Spell";
var KEY_SPELL_LEVEl = "SpellLevel";
var KEY_REWARD_TROPHY = "RewardTrophy";

function getRandomInt(min, max) { return Math.floor(Math.random() * (max - min + 1)) + min; }

function isEmpty(param) {
  return Object.keys(param).length === 0;
}

function GetTitleData(keys) {
  var titleData = server.GetTitleData({ Keys: keys })["Data"];
  var data = {};
  for (var key in titleData) {
    data[key] = JSON.parse(titleData[key]);
  }
  return data;
}

function NewCharacterData(index) {
  var data = {};
  data.Index = index;
  data.Exp = 0;
  data.Level = 1;
  data.Ability = [0, 0, 0, 0, 0];
  data.Deck = [0, 0, 0, 0, 0, 0, 0, 0];
  return data;
}

function NewSpell(index) {
  var data = {};
  data.Index = index;
  data.Level = 1;
  data.Count = 0;
  return data;
}

function GetUserCharacter(index) {
  var data = GetUserData(currentPlayerId, CHARACTER);
  for (var obj in data) {
    if (obj.Index === index)
      return obj;
  }
  return null;
}

function GetWeightedChoices(data) {
  let total = 1;
  for (let i = 0; i < data.length; ++i) {
    total += data[i].Weight;
  }

  const threshold = Math.floor(Math.random() * total);

  total = 0;
  for (let i = 0; i < data.length; ++i) {
    total += data[i].Weight;

    // If this value falls within the threshold, we're done!
    if (total >= threshold) {
      return data[i];
    }
  }
}

function GetUserData(playfabId, keys) {
  var userData = server.GetUserData({ PlayFabId: playfabId, Keys: keys })["Data"];
  var data = {};
  for (var key in userData) {
    data[key] = JSON.parse(userData[key].Value);
  }
  return data;
}

function GetUserVirtualCurrency() {
  var getUserInventory = server.GetUserInventory({ PlayFabId: currentPlayerId });
  return getUserInventory.VirtualCurrency;
}

handlers.NewEnemyMatching = function () {
  var trophy = server.GetPlayerStatistics({ PlayFabId: currentPlayerId, StatisticNames: ["Trophy"] }).Statistics[0].Value;
  var userData = GetUserData(currentPlayerId, KEY_USER_ENEMYUSER);
  var aiData;
  if (isEmpty(userData))
    aiData = GetTitleData(KEY_AI_USER)[KEY_AI_USER].filter(x => x.BaseScore <= trophy + 50 && x.BaseScore >= trophy - 50);
  else
    aiData = GetTitleData(KEY_AI_USER)[KEY_AI_USER].filter(x => x.EnemyIndex != userData[KEY_USER_ENEMYUSER].EnemyIndex && x.BaseScore <= trophy + 50 && x.BaseScore >= trophy - 50);
  var randomValue = Math.floor(Math.random() * aiData.length);
  var enemyData = {};
  enemyData.IsLose = false;
  enemyData.EnemyType = "AI";
  enemyData.BaseScore = aiData[randomValue].BaseScore;
  enemyData.EnemyIndex = aiData[randomValue].Index;
  enemyData.CharacterIndex = aiData[randomValue].CharacterIndex;
  enemyData.EnemyName = "";
  enemyData.Level = aiData[randomValue].Level;
  enemyData.Ability = aiData[randomValue].Ability;
  enemyData.Spell = aiData[randomValue].Spell;
  enemyData.SpellLevel = aiData[randomValue].SpellLevel;
  var data = {};
  data[KEY_USER_ENEMYUSER] = JSON.stringify(enemyData);
  server.UpdateUserData({ PlayFabId: currentPlayerId, Data: data });
  return enemyData;
}

//신규유저 데이터
handlers.NewUser = function () {
  var data = {};
  var gamerule = GetTitleData("GameRule")["GameRule"];
  var battleInfo = {};
  battleInfo.Character = gamerule.DefaultCharacter;
  battleInfo.Level = 1;
  battleInfo.Deck = gamerule.DefaultSpell;
  battleInfo.DeckLevel = [1, 1, 1, 1, 1, 1, 1, 1];
  data.BattleInfo = JSON.stringify(battleInfo);

  var userInfo = {};
  userInfo.Character = gamerule.DefaultCharacter;
  userInfo.HighTrophy = 0;
  userInfo.TrophyRewardIndex = 1;
  data.UserInfo = JSON.stringify(userInfo);
  var character = [];
  var userCharacter = NewCharacterData(gamerule.DefaultCharacter);
  userCharacter.Deck = gamerule.DefaultDeck;
  character.push(userCharacter);
  data.Character = JSON.stringify(character);

  var spells = [];
  for (var i = 0; i < gamerule.DefaultSpell.length; i++) {
    spells.push(NewSpell(gamerule.DefaultSpell[i]));
  }
  data.Spell = JSON.stringify(spells);

  server.UpdateUserData({ PlayFabId: currentPlayerId, Data: data });
  server.UpdatePlayerStatistics({ PlayFabId: currentPlayerId, Statistics: [{ "StatisticName": "Trophy", "Value": 0 }] });
}

//캐릭터 레벨업
handlers.LevelUpCharacter = function (args) {
  try {
    var userData = GetUserData(currentPlayerId, [KEY_USER_CHARACTER, KEY_USER_SPELL]);
    var userCharacter = userData[KEY_USER_CHARACTER].find(x => x.Index == args.CharacterIndex);
    if (userCharacter == null) throw "Not find userCharacter";

    var nextLevel = GetTitleData(KEY_CHARACTER_EXPERT_LEVEL)[KEY_CHARACTER_EXPERT_LEVEL].find(x => x.CharacterIndex == userCharacter.Index && x.Level == (userCharacter.Level + 1));
    if (nextLevel == null) throw "Max Level";

    if (userCharacter.Exp < nextLevel.RequiredExp)
      throw "Not enough experience";
    var gold = Number(GetUserVirtualCurrency()["GG"]);
    if (gold < nextLevel.RequiredGold)
      throw "Not enough gold";

    server.SubtractUserVirtualCurrency({ PlayFabId: currentPlayerId, VirtualCurrency: "GG", Amount: nextLevel.RequiredGold });
    var data = {};
    var body = {};

    if (nextLevel.hasOwnProperty("RewardSpell")) {
      if (!userData[KEY_USER_SPELL].some(x => x.Index == nextLevel.RewardSpell)) {
        var spell = NewSpell(nextLevel.RewardSpell);
        body.UserSpell = spell;
        userData[KEY_USER_SPELL].push(spell);
        data[KEY_USER_SPELL] = JSON.stringify(userData[KEY_USER_SPELL]);
      }
    }
    userCharacter.Level++;
    userCharacter.Exp = 0;
    data[KEY_USER_CHARACTER] = JSON.stringify(userData[KEY_USER_CHARACTER]);
    server.UpdateUserData({ PlayFabId: currentPlayerId, Data: data });
    body.UserCharacter = userCharacter;
    body.GG = gold - nextLevel.RequiredGold;
    return body;

  } catch (ex) {
    log.error(ex);
  }

}

//스펠 레벨업
handlers.LevelUpSpell = function (args, context) {
  try {
    var userSpells = GetUserData(currentPlayerId, "Spell");
    var userSpell = userSpells["Spell"].find(x => x.Index == args.SpellIndex);
    if (userSpell == null) throw "Not find userSpell";

    var nextLevel = GetTitleData("SpellLevel")["SpellLevel"].find(x => x.Index == userSpell.Level + 1);
    if (nextLevel == null) throw "Max Level";
    if (nextLevel.RequiredSpellCardAmount > userSpell.Count) throw "Not enough cards";

    var gold = Number(GetUserVirtualCurrency()["GG"]);
    if (nextLevel.RequiredGold > gold) throw "Not enough gold.";

    server.SubtractUserVirtualCurrency({ PlayFabId: currentPlayerId, VirtualCurrency: "GG", Amount: nextLevel.RequiredGold });
    userSpell.Count -= nextLevel.RequiredSpellCardAmount;
    userSpell.Level++;
    var data = {};
    data.Spell = JSON.stringify(userSpells["Spell"]);
    server.UpdateUserData({ PlayFabId: currentPlayerId, Data: data });
    var body = {};
    body.UserSpell = userSpell;
    body.GG = gold - nextLevel.RequiredGold;
    return body;

  } catch (ex) {
    log.error(ex);
    // server.WriteTitleEvent({
    //   EventName: 'LevelUpSpell',
    //   Body: ex
    // });
  }
}

//어빌리티 레벨업
handlers.LevelUpAbility = function (args) {
  try {
    var userData = GetUserData(currentPlayerId, KEY_USER_CHARACTER);
    var titleData = GetTitleData([KEY_CHARACTER_ABILITY, KEY_CHARACTER_EXPERT_LEVEL]);

    var userCharacter = userData[KEY_USER_CHARACTER].find(x => x.Index == args.CharacterIndex);
    if (userCharacter == null) throw "Not find userCharacter";

    var upgradeAbility = titleData[KEY_CHARACTER_ABILITY].find(x => x.CharacterIndex == args.CharacterIndex && x.AbilityIndex == args.AbilityIndex);
    var index = 1;
    var usedAbilityPoint = 0;
    for (var value in userCharacter.Ability) {
      var ability = titleData[KEY_CHARACTER_ABILITY].find(x => x.CharacterIndex == args.CharacterIndex && x.AbilityIndex == index);
      usedAbilityPoint += ability.RequiredAbilityPoint * userCharacter.Ability[value];
      index++;
    }

    var abilityPoint = 0;
    var expertLevels = titleData[KEY_CHARACTER_EXPERT_LEVEL].filter(x => x.CharacterIndex == args.CharacterIndex && x.Level <= userCharacter.Level);
    for (var i in expertLevels) {
      abilityPoint += expertLevels[i].RewardAbilityPoint;
    }
    var remainAbilityPoint = abilityPoint - usedAbilityPoint;
    if (remainAbilityPoint < upgradeAbility.RequiredAbilityPoint) throw "Not enough Ability Points.";
    userCharacter.Ability[args.AbilityIndex - 1]++;
    var data = {};
    data[KEY_USER_CHARACTER] = JSON.stringify(userData[KEY_USER_CHARACTER]);
    server.UpdateUserData({ PlayFabId: currentPlayerId, Data: data });
    var body = {};
    body.CharacterIndex = args.CharacterIndex;
    body.AbilityIndex = args.AbilityIndex;
    body.AbilityLevel = userCharacter.Ability[args.AbilityIndex - 1];
    return body;

  } catch (ex) {
    log.error(ex);
    // server.WriteTitleEvent({
    //   EventName: 'LevelUpSpell',
    //   Body: ex
    // });
  }
}

//트로피 보상
handlers.RewardTrophy = function (args) {
  try {
    var body = {};
    var data = {};
    var titleData = GetTitleData([KEY_REWARD_TROPHY, KEY_SPELL]);
    var userData = GetUserData(currentPlayerId, [KEY_USER_INFO, KEY_USER_SPELL, KEY_USER_CHARACTER]);

    if (args.RewardIndex != userData[KEY_USER_INFO].TrophyRewardIndex + 1)
      throw "You can't get this reward."

    var rewardData = titleData[KEY_REWARD_TROPHY].find(x => x.Index == args.RewardIndex);
    if (rewardData == null)
      throw "There is no reward available.";

    if (rewardData.HighTrophy > userData[KEY_USER_INFO].HighTrophy)
      throw "Not enough trophies.";
    userData[KEY_USER_INFO].TrophyRewardIndex++;

    body.TrophyRewardIndex = userData[KEY_USER_INFO].TrophyRewardIndex;
    data[KEY_USER_INFO] = JSON.stringify(userData[KEY_USER_INFO]);

    switch (rewardData.RewardType) {
      case "Gold":
        body.GG = server.AddUserVirtualCurrency({ PlayFabId: currentPlayerId, VirtualCurrency: "GG", Amount: rewardData.RewardParameter[0] }).Balance;
        break;
      case "Character":
        var character = userData[KEY_USER_CHARACTER].find(x => x.Index == rewardData.RewardParameter[0]);
        if (character == null) character = NewCharacterData(rewardData.RewardParameter[0]);

        userData[KEY_USER_CHARACTER].push(character);
        body[KEY_USER_CHARACTER] = character;
        data[KEY_USER_CHARACTER] = JSON.stringify(userData[KEY_USER_CHARACTER]);
        break;
      case "Spell":
        body[KEY_USER_SPELL] = [];
        rewardData.RewardParameter.forEach(value => {
          var spell = userData[KEY_USER_SPELL].find(x => x.Index == value);
          if (spell == null) spell = NewSpell(value);

          body[KEY_USER_SPELL].push(spell);
          userData[KEY_USER_SPELL].push(spell);
        });

        data[KEY_USER_SPELL] = JSON.stringify(userData[KEY_USER_SPELL]);
        break;
    }

    server.UpdateUserData({ PlayFabId: currentPlayerId, Data: data });
    return body;
  } catch (ex) {
    log.error(ex);
    // server.WriteTitleEvent({
    //   EventName: 'LevelUpSpell',
    //   Body: ex
    // });
  }
}


//덱 저장
handlers.SaveDeck = function (args) {
  var data = {};
  var userData = GetUserData(currentPlayerId, [KEY_USER_CHARACTER, KEY_USER_INFO]);
  userData[KEY_USER_INFO].Character = args.Character;
  data.UserInfo = JSON.stringify(userData[KEY_USER_INFO]);
  args.Spell.forEach(spell => userData[KEY_USER_CHARACTER].find(x => x.Index == spell.Index).Deck = spell.Deck);
  data.Character = JSON.stringify(userData[KEY_USER_CHARACTER]);
  server.UpdateUserData({ PlayFabId: currentPlayerId, Data: data });
}

//배틀 스타트
handlers.BattleStart = function (args) {
  var data = {};
  var userData = GetUserData(currentPlayerId, [KEY_USER_CHARACTER, KEY_USER_INFO, KEY_USER_ENEMYUSER]);
  if (args.hasOwnProperty(KEY_USER_CHARACTER)) {
    userData[KEY_USER_INFO].Character = args.Character;
    data[KEY_USER_INFO] = JSON.stringify(userData[KEY_USER_INFO]);
    args.Spell.forEach(spell => userData[KEY_USER_CHARACTER].find(x => x.Index == spell.Index).Deck = spell.Deck);
    data[KEY_USER_CHARACTER] = JSON.stringify(userData[KEY_USER_CHARACTER]);
  }

  //마지막 배틀정보 저장
  var battleInfo = {};
  battleInfo.CharacterIndex = args.BattleInfo.CharacterIndex;
  battleInfo.Level = args.BattleInfo.Level;
  battleInfo.Spell = args.BattleInfo.Spell;
  battleInfo.SpellLevel = args.BattleInfo.SpellLevel;
  battleInfo.Ability = args.BattleInfo.Ability;

  userData[KEY_USER_ENEMYUSER].IsLose = true;
  data[KEY_USER_ENEMYUSER] = JSON.stringify(userData[KEY_USER_ENEMYUSER]);
  data[KEY_USER_BATTLE_INFO] = JSON.stringify(battleInfo);

  server.UpdateUserData({ PlayFabId: currentPlayerId, Data: data });
}

handlers.TempExpUp = function (args) {
  var userData = GetUserData(currentPlayerId, KEY_USER_CHARACTER);
  var character = userData[KEY_USER_CHARACTER].find(x => x.Index == args.CharacterIndex);
  character.Exp += 5;
  var data = {};
  data[KEY_USER_CHARACTER] = JSON.stringify(userData[KEY_USER_CHARACTER]);
  server.UpdateUserData({ PlayFabId: currentPlayerId, Data: data });
  return character;
}

handlers.BattleResult = function (args) {
  try {
    var userData = GetUserData(currentPlayerId, [KEY_USER_SPELL, KEY_USER_ENEMYUSER, KEY_USER_INFO,KEY_USER_CHARACTER]);
    if (userData[KEY_USER_ENEMYUSER] == null) throw "Not EnemyUser";
    var trophy = server.GetPlayerStatistics({ PlayFabId: currentPlayerId, StatisticNames: ["Trophy"] }).Statistics[0].Value;
    var body = {};
    var data = {};
    body.HighTrophy = userData[KEY_USER_INFO].HighTrophy;
    body.PreTrophy = trophy;
    if (String(args.IsWin) == "true") {
      trophy += 50;
      body.Trophy = trophy;
      if (userData[KEY_USER_INFO].HighTrophy < trophy) {
        userData[KEY_USER_INFO].HighTrophy = trophy;
        data[KEY_USER_INFO] = JSON.stringify(userData[KEY_USER_INFO]);
        body.HighTrophy = trophy;
      }

      var rewardChest = GetTitleData(KEY_REWARD_CHEST)[KEY_REWARD_CHEST];
      var userSpell = userData[KEY_USER_SPELL];
      body.Reward = [];
      for (var i = 0; i < 3; i++) {
        var reward = GetWeightedChoices(rewardChest);
        var rewardValue = {};
        rewardValue.RewardType = reward.RewardType;
        rewardValue.Value = reward.RewardAmount;

        switch (reward.RewardType) {
          case "SpellCard":
            var spell = userSpell[getRandomInt(0, userSpell.length - 1)];
            spell.Count += reward.RewardAmount;
            rewardValue.Index = spell.Index;
            break;
        }

        body.Reward.push(rewardValue);
      }

      var userCharacter = userData[KEY_USER_CHARACTER].find(x=>x.Index == userData[KEY_USER_INFO].Character);
      userCharacter.Exp++;
      body.Exp = userCharacter.Exp;
      body.CharacterIndex = userData[KEY_USER_INFO].Character;
      body.IsWin = "True";
      data[KEY_USER_CHARACTER] =JSON.stringify(userData[KEY_USER_CHARACTER]);
      data[KEY_USER_SPELL] = JSON.stringify(userSpell);
      server.UpdateUserData({ PlayFabId: currentPlayerId, Data: data, KeysToRemove: [KEY_USER_ENEMYUSER] });

      var GG = 0;
      for (var i = 0; i < body.Reward.length; i++) {
        if (body.Reward[i].RewardType == "Gold")
          GG += body.Reward[i].Value;
      }
      body.GG = server.AddUserVirtualCurrency({ PlayFabId: currentPlayerId, VirtualCurrency: "GG", Amount: GG }).Balance;
    } else {
      trophy -= 50;
      if (trophy < 0) trophy = 0;
      body.Trophy = trophy;
      body.IsWin = "False";
      server.UpdateUserData({ PlayFabId: currentPlayerId, KeysToRemove: [KEY_USER_ENEMYUSER] });
    }

    server.UpdatePlayerStatistics({ PlayFabId: currentPlayerId, Statistics: [{ "StatisticName": "Trophy", "Value": trophy }] });
    return body;

  } catch (ex) {
    log.error(ex);
  }
}

handlers.ForceQuitLose = function (args) {
  try {
    var enemyuser = GetUserData(currentPlayerId, KEY_USER_ENEMYUSER)[KEY_USER_ENEMYUSER];
    if (enemyuser.IsLose) {
      var trophy = server.GetPlayerStatistics({ PlayFabId: currentPlayerId, StatisticNames: ["Trophy"] }).Statistics[0].Value;
      trophy -= 50;
      if (trophy < 0) trophy = 0;
      var body = {};
      body.Trophy = trophy;
      server.UpdatePlayerStatistics({ PlayFabId: currentPlayerId, Statistics: [{ "StatisticName": "Trophy", "Value": trophy }] });
      body.EnemyUser = server.ExecuteCloudScript({ FunctionName: "NewEnemyMatching", PlayFabId: currentPlayerId }).FunctionResult;
      return body;
    } else
      throw "DataError";
  }
  catch (ex) {
    log.error(ex);
  }
}
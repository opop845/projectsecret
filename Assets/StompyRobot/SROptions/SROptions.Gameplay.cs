﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SRDebugger;
using System.ComponentModel;
using Secret.Data;
using Secret;

public partial class SROptions
{
	[Category("Character")]

	public void CharacterExpUp()
    {
		if(GameDataManager.Instance.User != null)
			ServerManager.Instance.ToolExpUp(GameDataManager.Instance.User.currentCharacter);
	}

	[Category("Character")]
	public void HighTrophyUp()
    {
		ServerManager.Instance.ToolHighTrophyUp();
	}
}
